﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace VMS_SNMP
{
    public partial class SnmpSign
    {
        private Dictionary<string, Dictionary<string, List<string>>> _templates;

        private void LoadTemplates()
        {
            // Build a map of all templates for this sign type
            _templates = new Dictionary<string, Dictionary<string, List<string>>>();
            using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
            {
                sqlConnection.Open();

                SqlCommand command = new SqlCommand(String.Format("SELECT * FROM {0} WHERE {1} = '{2}' ORDER BY {3}, {4}, {5}",
                    /* 0 */ SignTemplateRecord.TableName,
                    /* 1 */ SignTemplateRecord.ColumnSignType,
                    /* 2 */ _type,
                    /* 3 */ SignTemplateRecord.ColumnType,
                    /* 4 */ SignTemplateRecord.ColumnSubType,
                    /* 5 */ SignTemplateRecord.ColumnSequence),
                sqlConnection);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    SignTemplateRecord template = new SignTemplateRecord(reader);
                    Dictionary<string, List<string>> templates;
                    List<string> templateList;
                    if (!_templates.TryGetValue(template.Type, out templates))
                    {
                        // Create new map entry
                        templates = new Dictionary<string, List<string>>();
                        _templates.Add(template.Type, templates);
                    }
                    if (!templates.TryGetValue(template.SubType, out templateList))
                    {
                        templateList = new List<string>();
                        templates.Add(template.SubType, templateList);
                    }
                    templates[template.SubType].Add(template.Template);
                }
                reader.Close();
            }
        }

        private string GetTemplate(string type, string subType)
        {
            // Look up type
            Dictionary<string, List<string>> templates;
            if (_templates.TryGetValue(type, out templates))
            {
                // Look up subtype
                List<string> templateList;
                if (templates.TryGetValue(subType, out templateList)
                    && templateList != null)
                {
                    string template = "";
                    foreach (string templateItem in templateList)
                    {
                        template += templateItem;
                    }

                    return template;
                }
            }

            // Only log errors for non log types
            foreach (string logType in SignTemplateRecord.TypeLogs)
            {
                if (type == logType) return null;
            }

            LogError(String.Format("Missing SIGN {0}:{1} template!", type, subType));

            return null;
        }

        private List<string> GetTemplates(string type, string subType)
        {
            // Look up type
            Dictionary<string, List<string>> templates;
            if (_templates.TryGetValue(type, out templates))
            {
                // Look up subtype
                List<string> templateList;
                if (templates.TryGetValue(subType, out templateList)
                    && templateList != null)
                {
                    return templateList;
                }
            }

            // Only log errors for non log types
            foreach (string logType in SignTemplateRecord.TypeLogs)
            {
                if (type == logType) return null;
            }

            LogError(String.Format("Missing SIGN {0}:{1} template!", type, subType));

            return null;
        }

        private Dictionary<string, string> GetTemplates(string type)
        {
            // Look up type
            Dictionary<string, List<string>> templates;
            if (_templates.TryGetValue(type, out templates)
                && templates != null)
            {
                Dictionary<string, string> templateItems = new Dictionary<string, string>();
                foreach (KeyValuePair<string, List<string>> item in templates)
                {
                    string template = "";
                    foreach (string templateItem in item.Value)
                    {
                        template += templateItem;
                    }
                    templateItems.Add(item.Key, template);
                }

                return templateItems;
            }

            LogError(String.Format("Missing SIGN {0} templates!", type));

            return null;
        }
    }
}