﻿using System;
using System.Xml;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Timers;
using System.Threading;
using ServiceBrokerListener.Domain;

namespace VMS_SNMP
{
    public partial class SnmpSign : SignRecord
    {
        // Constants
        public const string StatusMessage = "SET";
        public const string StatusFault = "FAULT";
        public const string StatusComms = "COMMS";
        public const string StatusPower = "POWER";
        public const string StatusTemperature = "TEMPERATURE";
        public const string StatusLuminance = "LUMINANCE";

        public const string ElementRoot = "snmp";

        public const string ParameterUnit = "unit";
        public const string ParameterWidth = "width";
        public const string ParameterHeight = "height";
        public const string ParameterColours = "colours";
        public const string ParameterLedFaultMode = "LED_FAULT_MODE";
        public const string ProcedureClusterBusy = "SignClusterIsBusy";

        public const string ProcessDimBrightValue = "dbo.lightingTable";

        // Defaults
        public const int DefaultPollInterval = 30;
        public const int DefaultTimeoutMargin = 5;
        public const int DefaultWidth = 64;
        public const int DefaultHeight = 64;
        public const int DefaultColours = 3;
        public const int DefaultDayLight = 0;
        public const int DefaultFaultMode = 0;
        // Members
        private int _width, _height, _colours;
        private Dictionary<string, int> _parameters;

        private int _pollTimerInterval;

        private int _luminanceDayLightLevel;
        private int _LedFaultMode;
        private SnmpRequestManager _snmpRequest;

        private SqlDependencyEx _actionDependency = null;

        private System.Timers.Timer _actionTimer, _watchdogTimer, _pollTimer, _luminanceTimer,_ledFaultModeTimer;
        private ElapsedEventHandler _actionTimerHandler, _watchdogTimerHandler, _pollTimerHandler,_luminanceTimerHandler,_ledFaultModeTimerHandler;

        private Semaphore _actionProcessing;
        private bool _actionWaiting, _pollNeeded;

        private SignActionRecord _currentAction;

        private delegate void ProcessActionDelegate(SignActionRecord action);
        private Dictionary<string, ProcessActionDelegate> _actions;
        private Dictionary<string, int> _actionDurations;

        private Dictionary<string, string> _statuses;

        // Constructors
        public SnmpSign(IDataRecord record)
            : this(IDataRecordConvert.ToInt(record, ColumnId),
            IDataRecordConvert.ToString(record, ColumnType),
            IDataRecordConvert.ToInt(record, ColumnPlacementId),
            IDataRecordConvert.ToString(record, ColumnHaAddress),
            IDataRecordConvert.ToString(record, ColumnNetAddress),
            IDataRecordConvert.ToInt(record, ColumnUnit),
            IDataRecordConvert.ToString(record, ColumnInfo),
            IDataRecordConvert.ToBoolean(record, ColumnActive))
        { }

        public SnmpSign(int id, string type, int placementId, string haAddress, string netAddress, int unit, string info, bool active)
            : base(id, type, placementId, haAddress, netAddress, unit, info, active)
        {
            // Populate actions
            _actions = new Dictionary<string, ProcessActionDelegate>()
            {
                { SignActionRecord.ActionSet, ProcessSet },
                { SignActionRecord.ActionValidate, ProcessValidate },
                { SignActionRecord.ActionBlank, ProcessBlank },
                { SignActionRecord.ActionPoll, ProcessPoll },
                { SignActionRecord.ActionGetBitmap, ProcessGetBitmap },
                { SignActionRecord.ActionGetFaultBitmap, ProcessGetBitmap },
                { SignActionRecord.ActionGetValidationBitmap, ProcessGetBitmap },
                { SignActionRecord.ActionSetLuminance, ProcessSetLuminance },
                { SignActionRecord.ActionClearLuminance, ProcessClearLuminance },
                { SignActionRecord.ActionGetSnmp, ProcessGetSnmp },
                { SignActionRecord.ActionSetSnmp, ProcessSetSnmp },
                { SignActionRecord.ActionSetLuminaceToOverride ,ProcessLuminaceOverride},
                {SignActionRecord.ActionLedFaultReportingMode,ProcessLedFaultMode}
     // Add led fault info here
            };

            LogInfo("SnmpSign IN ");
            // Create event handlers
            _actionTimerHandler = new ElapsedEventHandler(timer_OnElapsedEventHandler);

            _watchdogTimerHandler = new ElapsedEventHandler(timer_OnElapsedEventHandler);
            _pollTimerHandler = new ElapsedEventHandler(timer_OnElapsedEventHandler);
            _luminanceTimerHandler = new ElapsedEventHandler(timer_OnElapsedEventHandler);
            _ledFaultModeTimerHandler = new ElapsedEventHandler(timer_OnElapsedEventHandler);

            _actionDurations = new Dictionary<string, int>();

            _statuses = new Dictionary<string, string>();
        }

        ~SnmpSign()
        {
            Stop();
        }

        // Initialisation
        public bool Initialise(SnmpSignManager manager)
        {
            // Sign is not active, can't initialise
            if (!_active) return false;

            try
            {
                // Get SNMP request object for sign from manager
                LogInfo("GetSnmpRequestHandler:" + _netAddress);

                _snmpRequest = manager.GetSnmpRequestHandler(_netAddress);

                // No route to sign?
                if (_snmpRequest == null) return false;

               
                // Load configuration
                LoadConfiguration();

                // Load XML templates
                LoadTemplates();

                // Load SNMP value translation
                LoadTranslations();

                // Set translation delegate
                _snmpRequest.TranslateDelegate = TranslateValue;

                // Create action semaphore
                _actionProcessing = new Semaphore(0, 1);

                return true;
            }
            catch (Exception e)
            {
                LogError(e.ToString());
            }

            return false;
        }

        private void LoadConfiguration()
        {
            // Get configuration values
            _pollTimerInterval = ConfigurationRecord.GetInt(ConfigurationRecord.TypeSign, _type, 
                ConfigurationRecord.ValuePollInterval, DefaultPollInterval);

            _width = ConfigurationRecord.GetInt(ConfigurationRecord.TypeSign, _type, 
                ConfigurationRecord.ValueWidth, DefaultWidth);

            _height = ConfigurationRecord.GetInt(ConfigurationRecord.TypeSign, _type, 
                ConfigurationRecord.ValueHeight, DefaultHeight);

            _colours = ConfigurationRecord.GetInt(ConfigurationRecord.TypeSign, _type, 
                ConfigurationRecord.ValueColours, DefaultColours);

            _snmpRequest.SnmpVersion = ConfigurationRecord.GetInt(ConfigurationRecord.TypeSign, _type,
                ConfigurationRecord.ValueSnmpVersion, _snmpRequest.SnmpVersion);

            _snmpRequest.SnmpCommunity = ConfigurationRecord.GetString(ConfigurationRecord.TypeSign, _type,
                ConfigurationRecord.ValueSnmpCommunity, _snmpRequest.SnmpCommunity);

            _snmpRequest.Timeout = ConfigurationRecord.GetInt(ConfigurationRecord.TypeSign, _type,
                ConfigurationRecord.ValueSnmpTimeout, _snmpRequest.Timeout);

            _luminanceDayLightLevel = ConfigurationRecord.GetInt(ConfigurationRecord.TypeSign, _type,
                    ConfigurationRecord.ValueLuminanceDayLight, DefaultDayLight);

            // Sets up the SignAction mode 0 or 1 from the database
            _LedFaultMode = ConfigurationRecord.GetInt(ConfigurationRecord.TypeSign, _type, ConfigurationRecord.ValueLedFaultActionMode, DefaultFaultMode);

            // Get action durations for each supported action type
            _actionDurations.Clear();
            foreach (KeyValuePair<string, ProcessActionDelegate> item in _actions)
            {
                int duration = ConfigurationRecord.GetInt(ConfigurationRecord.TypeSign, 
                    _type, String.Format("DURATION_{0}", item.Key), 0);
                if (duration > 0) _actionDurations[item.Key] = duration;
            }

            // Set parameter values
            _parameters = new Dictionary<string, int>();
            _parameters.Add(ParameterUnit, _unit);
            _parameters.Add(ParameterWidth, _width);
            _parameters.Add(ParameterHeight, _height);
            _parameters.Add(ParameterColours, _colours);
            _parameters.Add(ParameterLedFaultMode, _LedFaultMode);
        }

        public void Start()
        {
            // Flag that sign is active
            _active = true;

            // Flag that action check is needed
            _actionWaiting = true;

            // Process poll in a thread from the pool
            ThreadPool.QueueUserWorkItem(ProcessPollItem, this);

            // Start status polling timer
            timer_StartPollTimer();

            timer_StartIuminanceTimer(5000);

            timer_LedFaultModeStartTimer(10000);
        }

        public void Stop()
        {
            // Flag that sign is inactive
            _active = false;

            // Stop timers
            timer_StopPollTimer();
            timer_StopActionTimer();
            timer_StopWatchdogTimer();
            timer_StopluminanceTimer();
            // Busy processing action?
            if (!_actionProcessing.WaitOne(0))
            {
                _actionProcessing.WaitOne();
            }

            // Dispose of SqlDependencyEx objects
            if (_actionDependency != null)
            {
                _actionDependency.Stop();
                _actionDependency.TableChanged -= sql_OnActionChangeHandler;
                _actionDependency = null;
            }
        }

        private void TryProcessNextAction()
        {
            // Sign still active?
            if (_active)
            {
                // Not busy processing action?
                if (_actionProcessing.WaitOne(0))
                {
                    // Process next action in a thread from the pool
                    ThreadPool.QueueUserWorkItem(ProcessNextActionItem, this);
                }
                else
                {
                    _actionWaiting = true;
                }
            }
        }

        static private void ProcessNextActionItem(Object stateInfo)
        {
            // Get context from state info
            SnmpSign context = (SnmpSign)stateInfo;
            if (context != null)
            {
                // Call in context
                context.ProcessNextAction();
            }
        }

        private void TryProcessPoll()
        {
            // Sign still active?
            if (_active)
            {
                // Not busy processing action?
                if (_actionProcessing.WaitOne(0))
                {
                    // Process poll in a thread from the pool
                    ThreadPool.QueueUserWorkItem(ProcessPollItem, this);
                }
                else
                {
                    _pollNeeded = true;
                }
            }
        }

        private static void ProcessPollItem(Object stateInfo)
        {
            // Get context from state info
            SnmpSign context = (SnmpSign)stateInfo;
            if (context != null)
            {
                // Call in context
                context.ProcessPoll(null);

                // No action was delayed?
                if (!context._actionWaiting)
                {
                    // Signal that next action can be processed
                    context._actionProcessing.Release();
                }
                else
                {
                    // Process next action
                    context.ProcessNextAction();
                }
            }
        }

        private static void ProcessPingItem(Object stateInfo)
        {
            // Get context from state info
            SnmpSign context = (SnmpSign)stateInfo;
            if (context != null)
            {
                // Call in context
                context.ProcessPing();
            }
        }

        private void ProcessNextAction()
        {
            // Sign still active?
            if (_active)
            {
                bool releaseAction = true;
                try
                {
                    using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                    {
                        sqlConnection.Open();

                        // Set up SignAction query that will get next action for this sign
                        string sqlString = String.Format("SELECT {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}"
                            + " FROM {0} WHERE {2} = {9} AND {4} = '{10}' ORDER BY {6} ASC",
                            /* 0 */ SignActionRecord.TableName,
                            /* 1 */ SignActionRecord.ColumnId,
                            /* 2 */ SignActionRecord.ColumnSignId,
                            /* 3 */ SignActionRecord.ColumnAction,
                            /* 4 */ SignActionRecord.ColumnStatus,
                            /* 5 */ SignActionRecord.ColumnInfo,
                            /* 6 */ SignActionRecord.ColumnStartTime,
                            /* 7 */ SignActionRecord.ColumnLastUpdated,
                            /* 8 */ SignActionRecord.ColumnUserId,
                            /* 9 */ _id,
                            /* 10 */ SignActionRecord.StatusPending);
                        SqlCommand command = new SqlCommand(sqlString, sqlConnection);

                        // While there are actions due
                        bool waitForNext = false;
                        while (!waitForNext)
                        {
                            // Restart the query each time as the table may have changed since the last action was completed
                            SqlDataReader reader = command.ExecuteReader();
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    // Check if action is due to start
                                    DateTime actionStart = IDataRecordConvert.ToDateTime(reader, SignActionRecord.ColumnStartTime);
                                    double interval = (actionStart.Ticks - DateTime.Now.Ticks) / 10000;
                                    if (interval <= 0)
                                    {
                                        // Process action 
                                        ProcessAction(new SignActionRecord(reader));
                                        break;
                                    }
                                    else
                                    {
                                        // Signal that next action can be processed
                                        _actionProcessing.Release();
                                        releaseAction = false;

                                        // Start a timer that will fire when next action is ready
                                        timer_StartActionTimer(interval);

                                        // Done for now, wait for timer for next action
                                        waitForNext = true;
                                        break;
                                    }
                                }
                            }
                            // No more due actions
                            else waitForNext = true;

                            reader.Close();
                        }
                    }
                }
                catch (Exception e)
                {
                    LogError(e.ToString());
                }

                // Was poll attempt made while processing action?
                if (_pollNeeded)
                {
                    ProcessPoll(null);
                    _pollNeeded = false;
                }

                if (releaseAction)
                {
                    // Signal that next action can be processed
                    _actionProcessing.Release();
                }

                // Set up notification to wait for next action
                WaitForAction();
            }
        }

        private int GetActionTimeout(SignActionRecord action)
        {
            // Any fixed duration for action?
            int duration = 0;
            if (_actionDurations.ContainsKey(action.Action))
            {
                duration = _actionDurations[action.Action];
            }

            return duration + _snmpRequest.Timeout + DefaultTimeoutMargin;
        }

        private void ProcessAction(SignActionRecord action)
        {
            LogInfo("Process Action = " + action.Action);
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                {
                    sqlConnection.Open();

                    // Use action handler if there is one
                    if (_actions.ContainsKey(action.Action)
                        && _actions[action.Action] != null)
                    {
                        _currentAction = action;

                        // Set action status to busy
                        action.Status = SignActionRecord.StatusBusy;
                        action.Update();

                        // Log action
                        LogAction(action);

                        // Start a timer that will fire when the action times out
                        timer_StartWatchdogTimer(GetActionTimeout(action) * 1000);

                        // Process action
                        _actions[action.Action](action);

                        LogInfo(String.Format("{0} ({1}): {2}", action.Action, 
                            action.Id, action.Status));
                    }
                    else
                    {
                        // Invalid action
                        action.Status = SignActionRecord.StatusFailure;
                        action.Update();
                    }
                }
            }
            catch (Exception e)
            {
                LogError(e.ToString());
            }

            // Clear current action
            _currentAction = null;

            // Stop time out timer
            timer_StopWatchdogTimer();
        }

        private void WaitForAction()
        {
            // Sign still active?
            if (_active)
            {
                try
                {
                    // Didn't start change notification yet?
                    if (_actionDependency == null)
                    {
                        // Start change notification for SignAction table
                        _actionDependency = new SqlDependencyEx(SnmpSignManager.SqlConnectionString, SnmpSignManager.DbName, SignActionRecord.TableName,
                            SnmpSignManager.DbUser, SnmpSignManager.DbSchema, SqlDependencyEx.NotificationType.All,
                            false, SnmpSignManager.DbNextIdentity);

                        _actionDependency.TableChanged += sql_OnActionChangeHandler;
                        _actionDependency.Start();
                    }
                }
                catch (Exception e)
                {
                    LogError(e.ToString());
                }
            }
        }

        private bool IsClusterBusy()
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                {
                    sqlConnection.Open();

                    // Is sign's cluster busy?
                    string sqlString = String.Format("EXEC {0} {1}", ProcedureClusterBusy, _id);
                    SqlCommand command = new SqlCommand(sqlString, sqlConnection);
                    System.Object result = command.ExecuteScalar();
                    if (result != null && Convert.ToInt32(result) != 0)
                    {
                        return true;
                    }
                }
            }
            catch (Exception e)
            {
                LogError(e.ToString());
            }

            return false;
        }

        private bool ProcessPing()
        {
            // Get comms check template
            string template = GetTemplate(SignTemplateRecord.TypeTemplate, StatusComms);

           
            if (template != null)
            {
                for (int retry = 0; retry < _numberRetries; retry++)
                {
                    try
                    {
                       
                   
                        // Get SNMP values
                        if (_snmpRequest.GetValues(ref template, _parameters, _externalCommsFault))
                        {
                            // Set status
                            SetStatus(StatusComms, template);
                            _externalCommsFault = false;
                            _numberRetries = DEFAULT_RETRIES;
                            return true;
                        }
                    }
                    catch (Exception e)
                    {
                        
                        LogError(e.ToString());
                    }

                    // Fail if sign has become inactive
                    if (!_active) return false;
                }
            }

            // Couldn't read comms check values so use format template
            template = GetTemplate(SignTemplateRecord.TypeFormat, StatusComms);
            if (template != null)
            {
                // Not being able to read comms check SNMP values constitutes failure (either wrong hardware or not reachable)
                template = String.Format(template, "1");
                SetStatus(StatusComms, template);
                _externalCommsFault = true;
                _numberRetries = 1;
            }

            return false;
        }

        private bool ProcessSet(string xml)
        {
            if (_externalCommsFault) ProcessPing();

            if (_externalCommsFault) return false;

            // Create XML document (embed info xml in dummy root)
            XmlDocument info = new XmlDocument();
            info.LoadXml(String.Format("<{0}>{1}</{0}>", ElementRoot, xml));

            // Get message node
            XmlNodeList elements = info.DocumentElement.GetElementsByTagName(SignActionRecord.ElementMessage);
            if (elements.Count > 0)
            {
                List<string> templates = GetTemplates(SignTemplateRecord.TypeAction, SignActionRecord.ActionSet);
                if (templates != null)
                {
                    // Any attributes?
                    Dictionary<string, string> attributes = null;
                    elements = elements[0].ChildNodes;
                    if (elements.Count > 0)
                    {
                        // Build a dictionary of the attributes & values
                        attributes = new Dictionary<string, string>();
                        foreach (XmlNode node in elements)
                        {
                            attributes[node.Name] = node.InnerText;
                        }
                    }

                    bool success = false;
                    for (int i = 0; i < templates.Count; i++)
                    {
                        try
                        {
                            // Set SNMP values
                            string template = templates[i];
                            success = _snmpRequest.SetValues(ref template, _parameters, attributes, _externalCommsFault);
                        }
                        catch (Exception e)
                        {
                            LogError(e.ToString());
                        }

                        // Fail if sign has become inactive
                        if (!_active) return false;

                        // Error?
                        if (!success) break;
                    }

                    // Update set status
                    UpdateStatus(SnmpSign.StatusMessage);

                    return success;
                }
            }

            return false;
        }

        private void ProcessSet(SignActionRecord action)
        {
            LogInfo(String.Format("{0} ({1}): {2}", action.Action, action.Id, action.Info));

            // Set requested
            action.Status = ProcessSet(action.Info) ? SignActionRecord.StatusSuccess : SignActionRecord.StatusFailure;

            // Log action 
            LogAction(action);

            // Update action record
            action.Update();
            ProcessClearLuminance(action);
            // Do a comms check if action failed and sign still active
            if (action.Status != SignActionRecord.StatusSuccess && _active && !_externalCommsFault)
            {
                ProcessPing();
            }
        }

        private bool ProcessValidate(string xml)
        {
            LogInfo("In ProcessValidate");
            // Create XML document (embed info xml in dummy root)
            XmlDocument info = new XmlDocument();
            info.LoadXml(String.Format("<{0}>{1}</{0}>", ElementRoot, xml));

            // Get <message> node
            XmlNodeList elements = info.DocumentElement.GetElementsByTagName(SignActionRecord.ElementMessage);
           

            if (elements.Count > 0)
            {
                List<string> templates = GetTemplates(SignTemplateRecord.TypeAction, SignActionRecord.ActionValidate);
                if (templates != null)
                {
                   
                    // Any attributes?
                    Dictionary<string, string> attributes = null;
                    elements = elements[0].ChildNodes;
                    if (elements.Count > 0)
                    {
                        // Build a dictionary of the attributes & values
                        attributes = new Dictionary<string, string>();
                        foreach (XmlNode node in elements)
                        {
                            attributes[node.Name] = node.InnerText;
                        }
                    }


                    bool success = false;
                    for (int i = 0; i < templates.Count; i++)
                    {
                        try
                        {
                            // Set SNMP values
                            string template = templates[i];
                         
                            success = _snmpRequest.SetValues(ref template, _parameters, attributes,0,SnmpRequestManager.ValidateTimeOut, _externalCommsFault);
                        }
                        catch (Exception e)
                        {
                            LogError(e.ToString());
                        }

                        // Fail if sign has become inactive
                        if (!_active) return false;

                        // Error?
                        if (!success) break;
                    }

                    return success;
                }
            }

            return false;
        }

        private void ProcessValidate(SignActionRecord action)
        {
            LogInfo(String.Format("{0} ({1}): {2}", action.Action, action.Id, action.Info));

            if (_externalCommsFault)
            {
                action.Status = SignActionRecord.StatusFailure;
            }
            else
            {
                // Validate requested
                action.Status = ProcessValidate(action.Info) ? SignActionRecord.StatusSuccess : SignActionRecord.StatusFailure;
            }
                //   ProcessClearLuminance(action);
            // Log action 
            LogAction(action);

            // Update action record
            action.Update();

            // Do a fault check if action failed and sign still active
            if (action.Status != SignActionRecord.StatusSuccess && _active && !_externalCommsFault)
            {
                ProcessPoll(null);
            }
        }

        private void ProcessBlank(SignActionRecord action)
        {
            LogInfo(String.Format("{0} ({1}): {2}", action.Action, action.Id, action.Info));

            List<string> templates = GetTemplates(SignTemplateRecord.TypeAction, SignActionRecord.ActionBlank);
            if ((templates != null) && (!_externalCommsFault))
            {
                for (int i = 0; i < templates.Count; i++)
                {
                    bool success = false;
                    try
                    {
                        // Set SNMP values
                        string template = templates[i];
                        success = _snmpRequest.SetValues(ref template, _parameters, _externalCommsFault);
                    }
                    catch (Exception e)
                    {
                        LogError(e.ToString());
                    }

                    // Fail if sign has become inactive
                    if (!_active) success = false;

                    action.Status = success ? SignActionRecord.StatusSuccess : SignActionRecord.StatusFailure;

                    // Error?
                    if (!success) break;
                }
            }
            else
            {
                action.Status = SignActionRecord.StatusFailure;
            }

            // Update set status (if sign still active)
            if (_active) UpdateStatus(SnmpSign.StatusMessage);

            // Log action 
            LogAction(action);

            // Update action record
            action.Update();

            // Do a comms check if action failed and sign still active
            if (action.Status != SignActionRecord.StatusSuccess && _active && !_externalCommsFault)
            {
                ProcessPing();
            }
        }

        private void ProcessPoll(SignActionRecord action)
        {
            if (_externalCommsFault && action != null)
            {
                // Update action record
                action.Status = SignActionRecord.StatusFailure;
                action.Update();


                // Sign still active?
                if (_active)
                {
                    // Restart poll timer
                    timer_StartPollTimer();
                }
                return;
            }

            // Only process poll if it was requested through SignAction, or the cluster is not busy processing an action
            if (action != null || !IsClusterBusy())
            {
                if (action == null) LogInfo(String.Format("{0}*", SignActionRecord.ActionPoll));
                else LogInfo(String.Format("{0} ({1})", action.Action, action.Id));

                bool error = true;

                // Check connectivity
                if (ProcessPing())
                {
                    // Get status templates
                    Dictionary<string, string> templates = new Dictionary<string, string>(GetTemplates(SignTemplateRecord.TypeStatus));
                    string[] xmlItems = new string[templates.Count];
                    int index = 0;

                    // Build array of xml request items
                    foreach (KeyValuePair<string, string> item in templates)
                    {
                        xmlItems[index++] = item.Value;
                    }

                    error = false;
                    try
                    {
                        // Get SNMP values
                        if (_snmpRequest.GetValues(ref xmlItems, _parameters, _externalCommsFault))
                        {
                            index = 0;
                            foreach (KeyValuePair<string, string> item in templates)
                            {
                                // Set status
                                SetStatus(item.Key, xmlItems[index++]);
                            }
                        }
                        else error = true;
                    }
                    catch (Exception e)
                    {
                        LogError(e.ToString());
                        error = true;
                    }
                }

                // Poll was requested through SignAction?
                if (action != null)
                {
                    // Update action record
                    action.Status = !error ? SignActionRecord.StatusSuccess : SignActionRecord.StatusFailure;
                    action.Update();
                }
            }

            // Sign still active?
            if (_active)
            {
                // Restart poll timer
                timer_StartPollTimer();
            }
        }

        private void ProcessGetBitmap(SignActionRecord action)
        {
            LogInfo(String.Format("{0} ({1}): {2}", action.Action, action.Id, action.Info));

            bool success = false;
            string info = GetTemplate(SignTemplateRecord.TypeAction, action.Action);
            if ((info != null) && (!_externalCommsFault))
            {
                try
                {
                    // Get SNMP values
                    success = _snmpRequest.GetValues(ref info, _parameters, SnmpRequestManager.PacketSize / _width, _externalCommsFault);
                    if (success) action.Info = info;
                }
                catch (Exception e)
                {
                    LogError(e.ToString());
                }
            }

            // Update action record
            action.Status = success ? SignActionRecord.StatusSuccess : SignActionRecord.StatusFailure;
            action.Update();
        }

        private bool ProcessSetLuminance(string xml)
        {
            // Create XML document (embed info xml in dummy root)
            XmlDocument info = new XmlDocument();
            info.LoadXml(String.Format("<{0}>{1}</{0}>", ElementRoot, xml));

            // Get <luminance> node
            XmlNodeList elements = info.DocumentElement.GetElementsByTagName(SignActionRecord.ElementLuminance);
            if (elements.Count > 0)
            {
                List<string> templates = GetTemplates(SignTemplateRecord.TypeAction, SignActionRecord.ActionSetLuminance);
                if (templates != null)
                {
                    // Any attributes?
                    Dictionary<string, string> attributes = null;
                    elements = elements[0].ChildNodes;
                    if (elements.Count > 0)
                    {
                        // Build a dictionary of the attributes & values
                        attributes = new Dictionary<string, string>();
                        foreach (XmlNode node in elements) 
                        {
                            attributes[node.Name] = node.InnerText;
                        }
                    }

                    bool success = false;
                    for (int i = 0; i < templates.Count; i++)
                    {
                        try
                        {
                            // Set SNMP values
                            string template = templates[i];
                            success = _snmpRequest.SetValues(ref template, _parameters, attributes, _externalCommsFault);
                        }
                        catch (Exception e)
                        {
                            LogError(e.ToString());
                        }

                        // Fail if sign has become inactive
                        if (!_active) return false;

                        // Error?
                        if (!success) break;
                    }

                    // Update luminance status
                    UpdateStatus(SnmpSign.StatusLuminance);

                    return success;
                }
            }

            return false;
        }

        private void ProcessLedFaultMode(SignActionRecord action)
        {
            LogInfo("ProcessLedMode");

            LogInfo(String.Format("{0} ({1}): {2}", action.Action, action.Id, action.Info));

            if (_externalCommsFault)
            {
                action.Status = SignActionRecord.StatusFailure;
            }
            else
            {
                action.Status = ProcessLedFaultMode(action.Info) ? SignActionRecord.StatusSuccess : SignActionRecord.StatusFailure;
            }
            // Log action 
            LogAction(action);

            // Update action record
            action.Update();

            // Do a comms check if action failed and sign still active
            if (action.Status != SignActionRecord.StatusSuccess && _active && !_externalCommsFault)
            {
                ProcessPing();
            }
   
        }

            bool  ProcessLedFaultMode(string xml)
            {

           
            XmlDocument info = new XmlDocument();
            info.LoadXml(String.Format("<{0}>{1}</{0}>", ElementRoot, xml));

            // Get <luminance> node
            XmlNodeList elements = info.DocumentElement.GetElementsByTagName(SignActionRecord.LedMode);
            if (elements.Count > 0)
            {
                List<string> templates = GetTemplates(SignTemplateRecord.TypeAction, SignActionRecord.ActionLedFaultReportingMode);
                if (templates != null)
                {
                    // Any attributes?
                    Dictionary<string, string> attributes = null;
                    elements = elements[0].ChildNodes;
                    if (elements.Count > 0)
                    {
                        // Build a dictionary of the attributes & values
                        attributes = new Dictionary<string, string>();
                        foreach (XmlNode node in elements)
                        {
                            attributes[node.Name] = node.InnerText;
                        }
                    }

                    bool success = false;
                    for (int i = 0; i < templates.Count; i++)
                    {
                        try
                        {
                            // Set SNMP values
                            string template = templates[i];
                            success = _snmpRequest.SetValues(ref template, _parameters, attributes, _externalCommsFault);
                          
                            LogInfo("LedfaultResult:" + success);
                        }
                        catch (Exception e)
                        {
                            LogError(e.ToString());
                        }

                        // Fail if sign has become inactive
                        if (!_active) return false;

                        // Error?
                        if (!success) break;
                    }

                    // Update luminance status
                    UpdateStatus(SnmpSign.StatusLuminance);

                    return success;
                }
            }

            return false;
        }

            private void ProcessSetLuminance(SignActionRecord action)
            {
                LogInfo(String.Format("{0} ({1}): {2}", action.Action, action.Id, action.Info));


            if (_externalCommsFault)
            {
                action.Status = SignActionRecord.StatusFailure;
            }
            else
            // Set luminance requested
            {
                action.Status = ProcessSetLuminance(action.Info) ? SignActionRecord.StatusSuccess : SignActionRecord.StatusFailure;
            }
            // Log action 
            LogAction(action);

            // Update action record
            action.Update();

            // Do a comms check if action failed and sign still active
            if (action.Status != SignActionRecord.StatusSuccess && _active && !_externalCommsFault)
            {
                ProcessPing();
            }
        }

        private void SetLedMode()
        {
             using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
            {
                sqlConnection.Open();

                // Call the DimBright Procedures

                SqlCommand clusterCommand = new SqlCommand("Select id,active FROM Cluster", sqlConnection);

                LogInfo("Going Into ClusterReader")
;
                using (SqlDataReader clusterReader = clusterCommand.ExecuteReader())
                {
                    LogInfo("Reading Cluster member for Id:" + Id);

                    while (clusterReader.Read())
                    {
                        bool cbool = Convert.ToBoolean(clusterReader["active"].ToString());

                        LogInfo("Sign Not active " + cbool);

                        if (cbool == true)
                        {
                            ClusterActionRecord.InsertClusterActionRecord("LEDFAULTREPORTINGMODE", Convert.ToInt16(clusterReader["id"]), "<ledfaultmode><value>" + _LedFaultMode + "</value ></ledfaultmode>");
                        }

                    }
                }
            }

        }

        private void ProcessDimBrightLuminance()
        {

                try
            {
                LogInfo("DimBright Procedures");
                using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                {
                    sqlConnection.Open();

                    // Call the DimBright Procedures

                    string sqlString = String.Format("EXEC {0} ", ProcessDimBrightValue);
                    SqlCommand command = new SqlCommand(sqlString, sqlConnection);

                    System.Object result = command.ExecuteScalar();

                    LogInfo("Luminance level : " + result);

                    SqlCommand clusterCommand = new SqlCommand("Select id,active FROM Cluster",sqlConnection);

                    LogInfo("Going Into ClusterReader")
;
                    using (SqlDataReader clusterReader = clusterCommand.ExecuteReader())
                    {
                        LogInfo("Reading Cluster member for Id:" + Id);


                        while (clusterReader.Read())
                        {
                            bool cbool =Convert.ToBoolean( clusterReader["active"].ToString());

                            LogInfo("Sign Not active " + cbool);
                          
                            if (cbool == true)
                            {
               
                                TimeSpan t = new TimeSpan(0, 1, 0);

                                luminanceTime = DateTime.Now.Add(t);

                                ClusterActionRecord.InsertClusterActionRecord("SETLUMINANCE",Convert.ToInt16(clusterReader["id"]), "<luminance><level>" + result + "</level ></luminance>");
                            }

                         }
                    }     
                }

                }

                catch (Exception e)
                {
                    LogError(e.ToString());
                }
        }

        private void ProcessLuminaceOverride(SignActionRecord action)
        {
            LogInfo(String.Format("{0} ({1}): {2}", action.Action, action.Id, action.Info));

            List<string> templates = GetTemplates(SignTemplateRecord.TypeAction, SignActionRecord.ActionSetLuminaceToOverride);
            if (templates != null)
            {
                bool success = false;
                for (int i = 0; i < templates.Count; i++)
                {
                    try
                    {
                        // Set SNMP values
                        string template = templates[i];
                        success = _snmpRequest.SetValues(ref template, _parameters, _externalCommsFault);
                    }
                    catch (Exception e)
                    {
                        LogError(e.ToString());
                    }

                    // Fail if sign has become inactive
                    if (!_active) success = false;

                    // Error?
                    if (!success) break;
                }

                action.Status = success ? SignActionRecord.StatusSuccess : SignActionRecord.StatusFailure;
            }
            else
            {
                action.Status = SignActionRecord.StatusFailure;
            }

            // Update luminance status (if sign still active)
            if (_active) UpdateStatus(SnmpSign.StatusLuminance);

            // Log action 
            LogAction(action);

            // Update action record
            action.Update();

            // Do a comms check if action failed and sign still active
            if (action.Status != SignActionRecord.StatusSuccess && _active)
            {
                ProcessPing();
            }
        }

        private void ProcessClearLuminance(SignActionRecord action)
        {
            LogInfo(String.Format("{0} ({1}): {2}", action.Action, action.Id, action.Info));

            List<string> templates = GetTemplates(SignTemplateRecord.TypeAction, SignActionRecord.ActionClearLuminance);
            if (templates != null)
            {
                bool success = false;
                for (int i = 0; i < templates.Count; i++)
                {
                    try
                    {
                        // Set SNMP values
                        string template = templates[i];
                        success = _snmpRequest.SetValues(ref template, _parameters, _externalCommsFault);
                    }
                    catch (Exception e)
                    {
                        LogError(e.ToString());
                    }

                    // Fail if sign has become inactive
                    if (!_active) success = false;

                    // Error?
                    if (!success) break;
                }

                action.Status = success ? SignActionRecord.StatusSuccess : SignActionRecord.StatusFailure;
            }
            else
            {
                action.Status = SignActionRecord.StatusFailure;
            }

            // Update luminance status (if sign still active)
            if (_active) UpdateStatus(SnmpSign.StatusLuminance);

            // Log action 
            LogAction(action);

            // Update action record
            action.Update();

            // Do a comms check if action failed and sign still active
            if (action.Status != SignActionRecord.StatusSuccess && _active)
            {
                ProcessPing();
            }
        }


        
        private void ProcessGetSnmp(SignActionRecord action)
        {
            LogInfo(String.Format("{0} ({1}): {2}", action.Action, action.Id, action.Info));

            bool success = false;
            try
            {
                // Get SNMP values
                string info = action.Info;
                success = _snmpRequest.GetValues(ref info, _parameters, _externalCommsFault);
                if (success) action.Info = info;
            }
            catch (Exception e)
            {
                LogError(e.ToString());
            }

            // Update action record
            action.Status = success ? SignActionRecord.StatusSuccess : SignActionRecord.StatusFailure;
            action.Update();
        }

        private void ProcessSetSnmp(SignActionRecord action)
        {
            LogInfo(String.Format("{0} ({1}): {2}", action.Action, action.Id, action.Info));

            bool success = false;
            try
            {
                // Set SNMP values
                string info = action.Info;
                success = _snmpRequest.SetValues(ref info, _parameters, _externalCommsFault);
                if (success) action.Info = info;
            }
            catch (Exception e)
            {
                LogError(e.ToString());
            }

            // Update action record
            action.Status = success ? SignActionRecord.StatusSuccess : SignActionRecord.StatusFailure;
            action.Update();
        }

        private void sql_OnActionChangeHandler(object sender, SqlDependencyEx.TableChangedEventArgs e)
        {
            // Try to process next action
            TryProcessNextAction();
        }

        // Timers
        private void timer_StartPollTimer()
        {
            _pollTimer = new System.Timers.Timer();
            _pollTimer.Elapsed += _pollTimerHandler;
            _pollTimer.Interval = _pollTimerInterval * 1000;
            _pollTimer.Enabled = true;
            _pollTimer.AutoReset = false;
        }

        private void timer_StopPollTimer()
        {
            if (_pollTimer != null)
            {
                _pollTimer.Elapsed -= _pollTimerHandler;
                _pollTimer.Stop();
                _pollTimer = null;
            }
        }

        private void timer_StartActionTimer(double interval)
        {
            _actionTimer = new System.Timers.Timer();
            _actionTimer.Elapsed += _actionTimerHandler;
            _actionTimer.Interval = interval;
            _actionTimer.Enabled = true;
            _actionTimer.AutoReset = false;
        }

        private void timer_StopActionTimer()
        {
            if (_actionTimer != null)
            {
                _actionTimer.Elapsed -= _actionTimerHandler;
                _actionTimer.Stop();
                _actionTimer = null;
            }
        }

        private void timer_StartIuminanceTimer(double interval)
        {
            _luminanceTimer = new System.Timers.Timer();
            _luminanceTimer.Elapsed += _luminanceTimerHandler;
            _luminanceTimer.Interval = interval;
            _luminanceTimer.Enabled = true;
            _luminanceTimer.AutoReset = false;
        }
        
        private void timer_StopluminanceTimer()
        {
            if(_luminanceTimer !=null)
            {
                _luminanceTimer.Elapsed -= _luminanceTimerHandler;
                _luminanceTimer.Stop();
                _luminanceTimer = null;
            }
        }

        private void timer_StartWatchdogTimer(double interval)
        {
            _watchdogTimer = new System.Timers.Timer();
            _watchdogTimer.Elapsed += _watchdogTimerHandler;
            _watchdogTimer.Interval = interval;
            _watchdogTimer.Enabled = true;
            _watchdogTimer.AutoReset = false;
        }

        private void timer_StopWatchdogTimer()
        {
            if (_watchdogTimer != null)
            {
                _watchdogTimer.Elapsed -= _watchdogTimerHandler;
                _watchdogTimer.Stop();
                _watchdogTimer = null;
            }
        }

        private void timer_LedFaultModeStartTimer(double interval)
        {
            _ledFaultModeTimer = new System.Timers.Timer();
            _ledFaultModeTimer.Elapsed += _ledFaultModeTimer_Elapsed;
            _ledFaultModeTimer.Interval = interval;
            _ledFaultModeTimer.Enabled = true;
            _ledFaultModeTimer.AutoReset = false;
          
        }

        private void timer_LedFaultModeStopTimer()
        {
            if(_ledFaultModeTimer !=null)
            {
                _ledFaultModeTimer.Elapsed -= _ledFaultModeTimerHandler;
            }
        }
        private void _ledFaultModeTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (sender == _ledFaultModeTimer)
            {

            //    ProcessLedFaultMode(Convert.ToString(_LedFaultMode));

           //     timer_LedFaultModeStartTimer(120000);
            }
        }

        static DateTime luminanceTime = DateTime.Now;

        private void timer_OnElapsedEventHandler(object source, ElapsedEventArgs e)
        {
            // Next action is due?
            if (source == _actionTimer)
            {
                // Release timer
                _actionTimer = null;

                // Try to process next action
                TryProcessNextAction();
            }
            // Action has timed out?
            else if (source == _watchdogTimer)
            {
                // Release timer
                _watchdogTimer = null;

                // Have an action in progress?
                if (_currentAction != null)
                {
                    // Set its status to timed out
                    _currentAction.Status = SignActionRecord.StatusTimeout;
                    _currentAction.Update();
                    _currentAction = null;

                    // Try to process next action
                    TryProcessNextAction();
                }
            }
            // Poll is due?
            else if (source == _pollTimer)
            {
                // Try to process poll
                TryProcessPoll();
            }
            else if(source ==_luminanceTimer)
            {
                if(DateTime.Now.Ticks > luminanceTime.Ticks)
                {
                    ProcessDimBrightLuminance();
                    SetLedMode();
                }
             
                timer_StartIuminanceTimer(600000); //10 minutes
            }
           
            
        }

        private void UpdateStatus(string type)
        {
            // Get message status
            string template = GetTemplate(SignTemplateRecord.TypeStatus, type);
            if (template != null)
            {
                try
                {
                    // Get SNMP values
                    if (_snmpRequest.GetValues(ref template, _parameters, _externalCommsFault))
                    {
                        // Set status
                        SetStatus(type, template);
                    }
                }
                catch (Exception e)
                {
                    LogError(e.ToString());
                }
            }
        }

        private bool UpdateStatus(string type, string info)
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                {
                    sqlConnection.Open();

                    // Insert status record
                    string sqlString = String.Format("INSERT INTO {0}({1}, {2}, {3})"
                        + " VALUES ('{4}', '{5}', '{6}')",
                        /* 0 */ SignStatusRecord.TableName,
                        /* 1 */ SignStatusRecord.ColumnSignId,
                        /* 2 */ SignStatusRecord.ColumnStatus,
                        /* 3 */ SignStatusRecord.ColumnInfo,
                        /* 4 */ _id,
                        /* 5 */ type,
                        /* 6 */ info);
                    SqlCommand sqlCommand = new SqlCommand(sqlString, sqlConnection);
                    sqlCommand.ExecuteNonQuery();

                    LogInfo(String.Format("{0} STATUS: {1}", type, info));

                    return true;
                }
            }
            catch (Exception e)
            {
                LogError(e.ToString());
            }

            return false;
        }

        private void SetStatus(string type, string info)
        {
            // No existing status value?
            string status;
            if (!_statuses.TryGetValue(type, out status))
            {
                // Log status change
                LogStatus(type, info);

                // Update status
                if (UpdateStatus(type, info))
                {
                    // Only add it when DB update succeeds so that we always match DB
                    _statuses.Add(type, info);
                }
            }
            // Status value changed (basic check)?
            else if (status != info)
            {
                // Status value changed?
                string newStatus = XmlHelper.Merge(info, status);
                if (newStatus != status)
                {
                    // Log status change
                    LogStatus(type, newStatus);

                    // Update status
                    if (UpdateStatus(type, newStatus))
                    {
                        // Only set it when DB update succeeds so that we always match DB
                        _statuses[type] = newStatus;
                    }
                }
            }
        }

        // Logging
        private void LogStatus(string type, string info)
        {
            // Get status log template
            string template = GetTemplate(SignTemplateRecord.TypeLogStatus, type);
            if (template != null)
            {
                // Build log message
                string logMessage = XmlHelper.StringFromXPathTemplate(template, info);
                if (logMessage != null)
                {
                    try
                    {
                        using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                        {
                            sqlConnection.Open();

                            // Insert status log record
                            string sqlString = String.Format("INSERT INTO {0}({1}, {2}, {3}, {4})"
                                + " VALUES ('{5}', '{6}', '{7}', '{8}')",
                                /* 0 */ SignLogRecord.TableName,
                                /* 1 */ SignLogRecord.ColumnSignId,
                                /* 2 */ SignLogRecord.ColumnType,
                                /* 3 */ SignLogRecord.ColumnSubtype,
                                /* 4 */ SignLogRecord.ColumnMessage,
                                /* 5 */ _id,
                                /* 6 */ SignLogRecord.TypeStatus,
                                /* 7 */ type,
                                /* 8 */ logMessage);
                            SqlCommand sqlCommand = new SqlCommand(sqlString, sqlConnection);
                            sqlCommand.ExecuteNonQuery();
                        }
                    }
                    catch (Exception e)
                    {
                        LogError(e.ToString());
                    }
                }
            }
        }

        private void LogAction(SignActionRecord action)
        {
            // Get action log template
            string templateType = action.Status == SignActionRecord.StatusBusy ? SignTemplateRecord.TypeLogActionStart : SignTemplateRecord.TypeLogActionEnd;
            string template = GetTemplate(templateType, action.Action);
            if (template != null)
            {
                // Add some fields from action record
                string info = "";
                if (action.Info != null) info += action.Info;
                info += String.Format("<{0}>{1}</{0}>", SignActionRecord.ColumnStatus, action.Status);
                info += String.Format("<{0}>{1}</{0}>", SignActionRecord.ColumnStartTime, action.StartTime);

                // Build log message
                string logMessage = XmlHelper.StringFromXPathTemplate(template, info);
                if (logMessage != null)
                {
                    try
                    {
                        using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                        {
                            sqlConnection.Open();

                            // Insert status log record
                            string sqlString = String.Format("INSERT INTO {0}({1}, {2}, {3}, {4}, {5})"
                                + " VALUES ('{6}', '{7}', '{8}', '{9}', {10})",
                                /* 0 */ SignLogRecord.TableName,
                                /* 1 */ SignLogRecord.ColumnSignId,
                                /* 2 */ SignLogRecord.ColumnType,
                                /* 3 */ SignLogRecord.ColumnSubtype,
                                /* 4 */ SignLogRecord.ColumnMessage,
                                /* 5 */ SignLogRecord.ColumnUserId,
                                /* 6 */ _id,
                                /* 7 */ SignLogRecord.TypeAction,
                                /* 8 */ action.Action,
                                /* 9 */ logMessage,
                                /* 10 */ action.UserId != 0 ? action.UserId.ToString() : "NULL");
                            SqlCommand sqlCommand = new SqlCommand(sqlString, sqlConnection);
                            sqlCommand.ExecuteNonQuery();
                        }
                    }
                    catch (Exception e)
                    {
                        LogError(e.ToString());
                    }
                }
            }
        }

        private void LogInfo(string text)
        {
            SnmpSignManager.LogInfo(String.Format("SIGN {0}: {1}", _id, text));
        }

        private void LogError(string text)
        {
            SnmpSignManager.LogError(String.Format("SIGN {0}: {1}", _id, text));
        }
    }
}