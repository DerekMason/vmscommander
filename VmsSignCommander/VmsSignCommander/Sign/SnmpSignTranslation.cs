﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace VMS_SNMP
{
    public partial class SnmpSign
    {
        private Dictionary<string, List<TranslationRecord>> _translations;

        private void LoadTranslations()
        {
            // Build a map of all translations for this sign type
            _translations = new Dictionary<string, List<TranslationRecord>>();
            List<TranslationRecord> translations;
            using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
            {
                sqlConnection.Open();

                SqlCommand command = new SqlCommand(String.Format("SELECT * FROM {0} WHERE {1} = '{2}' ORDER BY {3}, {4}",
                    /* 0 */ TranslationRecord.TableName,
                    /* 1 */ TranslationRecord.ColumnSignType,
                    /* 2 */ _type,
                    /* 3 */ TranslationRecord.ColumnTranslationType,
                    /* 4 */ TranslationRecord.ColumnRangeStart),
                    sqlConnection);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    TranslationRecord translation = new TranslationRecord(reader);
                    if (!_translations.TryGetValue(translation.TranslationType, out translations))
                    {
                        // Create new map entry
                        translations = new List<TranslationRecord>();
                        _translations.Add(translation.TranslationType, translations);
                    }
                    translations.Add(translation);
                }
                reader.Close();
            }
        }

        private string TranslateValue(string translationType, string value)
        {
            // Valid translation type?
            List<TranslationRecord> translations;
            if (_translations.TryGetValue(translationType, out translations))
            {
                // Look for matching translation
                foreach (TranslationRecord translation in translations)
                {
                    if (translation.Compare(value))
                    {
                        // Apply it
                        return translation.Value;
                    }
                }
            }

            return value;
        }
    }
}