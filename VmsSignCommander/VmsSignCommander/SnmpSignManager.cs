﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Security.Permissions;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Runtime.CompilerServices;
using System.IO;
using ServiceBrokerListener.Domain;

namespace VMS_SNMP
{
    public class SnmpSignManager
    {
        // Configuaration
        private static string _dbAddress = ".";
        private static string _dbName = "RCTTM";
        private static string _dbSchema = "dbo";
        private static string _dbUser = "SignManager";
        private static string _dbPassword = "VmsRigel";
        private static string _mibDirectory = null;

        // Members
        private Dictionary<string, SnmpRequestManager> _snmpHandlers;

        private Dictionary<int, SnmpSign> _snmpSigns;
        private Dictionary<int, SnmpSignCluster> _snmpClusters;

        private string _clusterSqlString;
        private SqlDependencyEx _clusterDependency = null;

        private string _signSqlString;
        private SqlDependencyEx _signDependency = null;

        private static int _dbNextIdentity = 0;

        // Delegates
        public delegate void LogInfoDelegate(string text);
        public delegate void LogErrorDelegate(string text, string file, string member, int line);
        private static LogInfoDelegate _logInfoDelegate = null;
        private static LogErrorDelegate _logErrorDelegate = null;

        private static System.Timers.Timer NetWorkConnectionTimer;
        // Constructors
        public SnmpSignManager()
            :this(null) {}

        public SnmpSignManager(string[] args)
        {
            _snmpClusters = new Dictionary<int, SnmpSignCluster>();
            _snmpSigns = new Dictionary<int, SnmpSign>();
            _snmpHandlers = new Dictionary<string, SnmpRequestManager>();

            if (args != null && args.Length > 0)
            {
                // Check arguments
                for (int i = 0; i < args.Length; i++)
                {
                    switch (args[i])
                    {
                        case "-db":
                            // SQL server address specified?
                            if (i < args.Length - 1 && !args[i + 1].StartsWith("-"))
                            {
                                _dbAddress = args[++i];
                            }
                            break;

                        case "-dbuser":
                            // SQL server user name specified?
                            if (i < args.Length - 1 && !args[i + 1].StartsWith("-"))
                            {
                                _dbUser = args[++i];
                            }
                            break;

                        case "-dbpwd":
                        case "-dbpassword":
                            // SQL server user password specified?
                            if (i < args.Length - 1)
                            {
                                _dbPassword = args[++i];
                            }
                            break;

                        case "-dbname":
                            // SQL server user name specified?
                            if (i < args.Length - 1 && !args[i + 1].StartsWith("-"))
                            {
                                _dbName = args[++i];
                            }
                            break;

                        case "-mibdir":
                        case "-mibdirectory":
                            // SQL server user name specified?
                            if (i < args.Length - 1 && !args[i + 1].StartsWith("-"))
                            {
                                _mibDirectory = args[++i];
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
        }

        ~SnmpSignManager()
        {
            Stop();
        }

        public bool Initialise()
        {
            try
            {
                NetWorkConnectionTimer = new System.Timers.Timer(600000);
                NetWorkConnectionTimer.Elapsed += NetWorkConnectionTimer_Elapsed;
                // Get permission for SqlDependency notification callbacks
                NetWorkConnectionTimer.Interval=600000;
                NetWorkConnectionTimer.Enabled = true;
                SqlClientPermission permission = new SqlClientPermission(PermissionState.Unrestricted);
                permission.Demand();

                // Open SQL connection
                using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                {
                    sqlConnection.Open();

                    // Build a map of all active clusters
                    _snmpClusters.Clear();
                    _clusterSqlString = String.Format("SELECT {0}.{1},{0}.{2},{0}.{3},{0}.{4},{0}.{5},{0}.{6}"
                        + " FROM {0} WHERE {0}.{6} = 'True'",
                        /* 0 */ String.Format("{0}.{1}", SnmpSignManager.DbSchema, ClusterRecord.TableName),
                        /* 1 */ ClusterRecord.ColumnId,
                        /* 2 */ ClusterRecord.ColumnPlacementId,
                        /* 3 */ ClusterRecord.ColumnDisplayName,
                        /* 4 */ ClusterRecord.ColumnDescription,
                        /* 5 */ ClusterRecord.ColumnInfo,
                        /* 6 */ ClusterRecord.ColumnActive);
                    SqlCommand command = new SqlCommand(_clusterSqlString, sqlConnection);
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        // Initialise cluster and add it to map
                        SnmpSignCluster cluster = new SnmpSignCluster(reader);
                        if (cluster.Initialise(this))
                        {
                            _snmpClusters.Add(cluster.Id, cluster);
                        }
                        else
                        {
                            LogError(String.Format("Could not initialise cluster {0}", cluster.Id));
                        }
                    }
                    reader.Close();

                    // Build a map of all active signs
                    _snmpSigns.Clear();
                    _signSqlString = String.Format("SELECT {0}.{1},{0}.{2},{0}.{3},{0}.{4},{0}.{5},{0}.{6},{0}.{7},{0}.{8}"
                        + " FROM {0} WHERE {0}.{8} = 'True'",
                        /* 0 */ String.Format("{0}.{1}", SnmpSignManager.DbSchema, SignRecord.TableName),
                        /* 1 */ SignRecord.ColumnId,
                        /* 2 */ SignRecord.ColumnType,
                        /* 3 */ SignRecord.ColumnPlacementId,
                        /* 4 */ SignRecord.ColumnHaAddress,
                        /* 5 */ SignRecord.ColumnNetAddress,
                        /* 6 */ SignRecord.ColumnUnit,
                        /* 7 */ SignRecord.ColumnInfo,
                        /* 8 */ SignRecord.ColumnActive);
                    command = new SqlCommand(_signSqlString, sqlConnection);
                    reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        // Initialise sign and add it to map
                        SnmpSign sign = new SnmpSign(reader);
                        if (sign.Initialise(this))
                        {
                            _snmpSigns.Add(sign.Id, sign);
                        }
                        else
                        {
                            LogError(String.Format("Could not initialise sign {0}", sign.Id));
                        }
                    }
                    reader.Close();
                }

                return true;
            }
            catch (Exception e)
            {
                LogError(e.ToString());
            }

            return false;
        }

        private void NetWorkConnectionTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
             bool result = NetworkHelper.IsNetworkAvailable(0);

            string nowTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            string sqlString = "";

            try
            {
                
                using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                {
          
                    sqlConnection.Open();

                 sqlString = String.Format("INSERT INTO {0} ({1},{2},{3}) VALUES ('{4}','{5}','{6}')",
                         /*0*/  ClusterActionRecord.NetworkConnectionValid,
                        /* 0 */ ClusterActionRecord.Network_Id,
                        /* 1 */ ClusterActionRecord.Networkstatus,
                        /* 2 */ ClusterActionRecord.NetworkTimeUp,
                        /* 3 */1,
                        /* 4 */ result,
                        /* 5 */ nowTime);
                                   

                    SqlCommand command = new SqlCommand(sqlString, sqlConnection);
                    command.ExecuteNonQuery();
                }
            }
            catch {
                LogInfo("Network Database Error: " + sqlString);
            }

            LogInfo("Is the network connected ? " + result);
        }

        public void Start()
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                {
                    sqlConnection.Open();

                    // Cancel any actions that are busy (from previous session) on start up
                    string sqlString = String.Format("UPDATE {0} SET {0}.{1} = '{2}' WHERE {0}.{1} = '{3}'",
                        /* 0 */ ClusterActionRecord.TableName,
                        /* 1 */ ClusterActionRecord.ColumnStatus,
                        /* 2 */ ClusterActionRecord.StatusCancelled,
                        /* 3 */ ClusterActionRecord.StatusBusy);
                    SqlCommand command = new SqlCommand(sqlString, sqlConnection);
                    command.ExecuteNonQuery();
                }
            }
            // Do nothing...
            catch { }

            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                {
                    sqlConnection.Open();

                    // Cancel any actions that are busy (from previous session) on start up
                    string sqlString = String.Format("UPDATE {0} SET {0}.{1} = '{2}' WHERE {0}.{1} = '{3}'",
                        /* 0 */ SignActionRecord.TableName,
                        /* 1 */ SignActionRecord.ColumnStatus,
                        /* 2 */ SignActionRecord.StatusCancelled,
                        /* 3 */ SignActionRecord.StatusBusy);
                    SqlCommand command = new SqlCommand(sqlString, sqlConnection);
                    command.ExecuteNonQuery();
                }
            }
            // Do nothing...
            catch { }

            // Start each cluster
            foreach (KeyValuePair<int, SnmpSignCluster> item in _snmpClusters)
            {
                item.Value.Start();
            }

            // Wait for cluster changes
            WaitForCluster();

            // Start each sign
            foreach (KeyValuePair<int, SnmpSign> item in _snmpSigns)
            {
                item.Value.Start();
            }

            // Wait for sign changes
            WaitForSign();
        }

        public void Stop()
        {
            // Stop each sign
            foreach (KeyValuePair<int, SnmpSign> item in _snmpSigns)
            {
                item.Value.Stop();
            }

            // Stop each cluster
            foreach (KeyValuePair<int, SnmpSignCluster> item in _snmpClusters)
            {
                item.Value.Stop();
            }

            // Dispose of SqlDependencyEx objects
            if (_clusterDependency != null)
            {
                _clusterDependency.Stop();
                _clusterDependency.TableChanged -= sql_OnClusterChangeHandler;
                _clusterDependency = null;
            }
            if (_signDependency != null)
            {
                _signDependency.Stop();
                _signDependency.TableChanged -= sql_OnSignChangeHandler;
                _signDependency = null;
            }
        }

        public void Restart()
        {
            try
            {
                // Stop manager
                Stop();

                // Restart SqlDependency notifications
				// Note: Left here as comment as a guide to implementing this in the
                // previous SqlDependency implementation if necessary. Remove when no 
                // longer needed.
                //SqlDependency.Stop(SqlConnectionString);
                //SqlDependency.Start(SqlConnectionString);

                // Start manager
                Start();
            }
            catch (Exception e)
            {
                LogError(e.ToString());
            }
        }

        public SnmpRequestManager GetSnmpRequestHandler(string remoteHost)
        {
            // Get host address to use for remote address
            string hostAddress = NetworkHelper.GetHostAddress(remoteHost);
            if (hostAddress != null)
            {
                SnmpRequestManager handler;
                if (!_snmpHandlers.TryGetValue(remoteHost, out handler))
                {
                    // Create SNMP request object
                    LogInfo("Look at addresses " + hostAddress + " " + remoteHost);
                    handler = new SnmpRequestManager(hostAddress, remoteHost, MibDirectory);
                    _snmpHandlers.Add(remoteHost, handler);
                }

                return handler;
            }
            else
            {
                LogError(String.Format("Could not create connection to {0}", remoteHost));

                // No route to sign
                return null;
            }
        }

        private void WaitForCluster()
        {
            try
            {
                // Didn't start change notification yet?
                if (_clusterDependency == null)
                {
                    // Start change notification for Cluster table
                    _clusterDependency = new SqlDependencyEx(SnmpSignManager.SqlConnectionString, SnmpSignManager.DbName, ClusterRecord.TableName,
                        SnmpSignManager.DbUser, SnmpSignManager.DbSchema, SqlDependencyEx.NotificationType.All,
                        false, SnmpSignManager.DbNextIdentity);

                    _clusterDependency.TableChanged += sql_OnClusterChangeHandler;
                    _clusterDependency.Start();
                }
            }
            catch (Exception e)
            {
                LogError(e.ToString());
            }
        }

        private void WaitForSign()
        {
            try
            {
                // Didn't start change notification yet?
                if (_signDependency == null)
                {
                    // Start change notification for Sign table
                    _signDependency = new SqlDependencyEx(SnmpSignManager.SqlConnectionString, SnmpSignManager.DbName, SignRecord.TableName,
                        SnmpSignManager.DbUser, SnmpSignManager.DbSchema, SqlDependencyEx.NotificationType.All,
                        false, SnmpSignManager.DbNextIdentity);

                    _signDependency.TableChanged += sql_OnSignChangeHandler;
                    _signDependency.Start();
                }
            }
            catch (Exception e)
            {
                LogError(e.ToString());
            }
        }

        private void sql_OnClusterChangeHandler(object sender, SqlDependencyEx.TableChangedEventArgs e)
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                {
                    sqlConnection.Open();

                    // Iterate through all clusters
                    string sqlString = String.Format("SELECT * FROM {0}", SnmpSignCluster.TableName);
                    SqlCommand command = new SqlCommand(sqlString, sqlConnection);
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        int clusterId = IDataRecordConvert.ToInt(reader, SnmpSignCluster.ColumnId);
                        bool active = IDataRecordConvert.ToBoolean(reader, SnmpSignCluster.ColumnActive);

                        // Cluster is already active?
                        SnmpSignCluster cluster;
                        if (_snmpClusters.TryGetValue(clusterId, out cluster))
                        {
                            // Going inactive?
                            if (!active)
                            {
                                // Shut it down and remove it
                                cluster.Stop();
                                _snmpClusters.Remove(clusterId);

                                LogInfo(String.Format("CLUSTER {0}: inactive", clusterId));
                            }
                        }
                        // Cluster is going active?
                        else if (active)
                        {
                            // Initialise cluster and add it to map
                            cluster = new SnmpSignCluster(reader);
                            if (cluster.Initialise(this))
                            {
                                LogInfo(String.Format("CLUSTER {0}: active", clusterId));

                                _snmpClusters.Add(cluster.Id, cluster);

                                // Start it
                                cluster.Start();
                            }
                            else
                            {
                                LogError(String.Format("Could not initialise cluster {0}", cluster.Id));
                            }
                        }
                    }
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                LogError(ex.ToString());
            }

            // Wait for cluster changes
            WaitForCluster();
        }

        private void sql_OnSignChangeHandler(object sender, SqlDependencyEx.TableChangedEventArgs e)
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                {
                    sqlConnection.Open();

                    string sqlString = String.Format("SELECT * FROM {0}", SignRecord.TableName);
                    SqlCommand command = new SqlCommand(sqlString, sqlConnection);
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        int signId = IDataRecordConvert.ToInt(reader, SignRecord.ColumnId);
                        bool active = IDataRecordConvert.ToBoolean(reader, SignRecord.ColumnActive);

                        // Sign is already active?
                        SnmpSign sign;
                        if (_snmpSigns.TryGetValue(signId, out sign))
                        {
                            // Going inactive?
                            if (!active)
                            {
                                // Shut it down and remove it
                                sign.Stop();
                                _snmpSigns.Remove(signId);

                                LogInfo(String.Format("SIGN {0}: inactive", signId));

                                // Cancel any actions that are waiting or busy
                                string signSqlString = String.Format("UPDATE {0} SET {0}.{1} = '{2}'"
                                    + " WHERE {0}.{3} = {4} AND {0}.{1} IN ('{5}', '{6}')",
                                    /* 0 */ SignActionRecord.TableName,
                                    /* 1 */ SignActionRecord.ColumnStatus,
                                    /* 2 */ SignActionRecord.StatusCancelled,
                                    /* 3 */ SignActionRecord.ColumnSignId,
                                    /* 4 */ signId,
                                    /* 5 */ SignActionRecord.StatusPending,
                                    /* 6 */ SignActionRecord.StatusBusy);
                                SqlCommand signCommand = new SqlCommand(signSqlString, sqlConnection);
                                signCommand.ExecuteNonQuery();
                            }
                        }
                        // Sign is going active?
                        else if (active)
                        {
                            // Initialise sign and add it to map
                            sign = new SnmpSign(reader);
                            if (sign.Initialise(this))
                            {
                                LogInfo(String.Format("SIGN {0}: active", signId));

                                _snmpSigns.Add(sign.Id, sign);

                                // Start it
                                sign.Start();
                            }
                            else
                            {
                                LogError(String.Format("Could not initialise sign {0}", sign.Id));
                            }
                        }
                    }
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                LogError(ex.ToString());
            }

            // Wait for sign changes
            WaitForSign();
        }

        // Logging
        public static void LogInfo(string text)
        {
            if (_logInfoDelegate != null)
            {
                _logInfoDelegate(text);
            }
        }

        public static void LogError(string text, [CallerFilePath] string file = "", [CallerMemberName] string member = "", [CallerLineNumber] int line = 0)
        {
            if (_logErrorDelegate != null)
            {
                _logErrorDelegate(text, Path.GetFileName(file), member, line);
            }
        }

        // Accessors
        public static string SqlConnectionString
        {
            get
            {
                return String.Format("server={0};"
                    + "User Id={1};Password={2};"
                    + "database={3};"
                    + "connection timeout=30;"
                    + "MultipleActiveResultSets=True;"
                    + "Max Pool Size=1000",
                    _dbAddress,
                    _dbUser,
                    _dbPassword,
                    _dbName);
            }
        }

        //public static string SqlConnectionString
        //{
        //    get { return @"Data Source=172.17.129.120\SQLEXPRESS;Initial Catalog=RCTTM;User ID=DatabaseManager;Password=VmsRigel;MultipleActiveResultSets=True;"; }
        //}
        public static string DbAddress
        {
            get { return _dbAddress; }
            set { _dbAddress = value; }
        }

        public static string DbUser
        {
            get { return _dbUser; }
            set { _dbUser = value; }
        }

        public static string DbPassword
        {
            get { return _dbPassword; }
            set { _dbPassword = value; }
        }

        public static string DbName
        {
            get { return _dbName; }
            set { _dbName = value; }
        }

        public static string DbSchema
        {
            get { return _dbSchema; }
            set { _dbSchema = value; }
        }

        public static int DbNextIdentity
        {
            get { return ++_dbNextIdentity; }
        }

        public string MibDirectory
        {
            get { return _mibDirectory; }
            set { _mibDirectory = value; }
        }

        public static LogInfoDelegate LogInfoHandler
        {
            set { _logInfoDelegate = value; }
        }

        public static LogErrorDelegate LogErrorHandler
        {
            set { _logErrorDelegate = value; }
        }
    }
}