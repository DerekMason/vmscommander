﻿using System;
using System.Xml;
using System.IO;
using System.Collections.Generic;
using nsoftware.IPWorksSSNMP;
using System.Threading;

namespace VMS_SNMP
{
    public partial class SnmpRequestManager
    {

        enum SnmpmgrEncryptionAlgorithms
        {
            encraDES =1,
            encraAES =2,
            encra3DES=3,
            encraAES192 =4,
            encraAES256 =5
        }

        enum SnmpmgrAuthenticationProtocols
        {
            authpHMACMD596 =1,
            authpHMACSHA96 =2,
            authpHMAC192SHA256=3
        }
        // Constants
        private const string AttributeStart = "start";
        private const string AttributeCount = "count";
        private const string AttributeIndex = "index";
        private const string ElementRoot = "snmprequest";
        private const string NodeNameText = "#text";

      

        

        private const int _defaultTimeout = 10;
        private const int _validateTimeOut = 30;

        private const SnmpmgrSNMPVersions _defaultSnmpVersion = SnmpmgrSNMPVersions.snmpverV3;
        private const string _defaultSnmpCommunity = "private";

        public const int PacketSize = 1408; // MTU - (6% SNMP overhead? Using this value to allow 64x64 bitmaps to fit into 3 packets)

        // Members
        private Mibbrowser _mibbrowser;
        private Snmpmgr _snmpmgr;

        private static int _timeout = _defaultTimeout;

        private object _snmpLock = new object();

        private Dictionary<string, List<SnmpRequestObject>> _requestObjects;
        private AutoResetEvent _requestEvent;
        private bool _requestResult;

        public delegate string TranslateValue(string translation, string value);
        private TranslateValue _translateDelegate;

        private class SnmpRequestNode
        {
            // Members
            private SnmpRequestObject _snmpObject;
            private XmlNode _node;
            private bool _set;

            // Constructor
            public SnmpRequestNode(XmlNode node, SnmpRequestObject snmpObject)
            {
                this._snmpObject = snmpObject;
                this._node = node;
                this._set = false;
            }

            // Accessors
            public XmlNode Node
            {
                get { return _node; }
            }

            public SnmpRequestObject SnmpObject
            {
                get { return _snmpObject; }
            }

            public bool Set
            {
                get { return _set; }
                set { _set = value; }
            }
        }

        private void SnmpInit()
        {
              _snmpmgr = new Snmpmgr();
     
            _snmpmgr.User = "vmssys";

            _snmpmgr.EncryptionAlgorithm = (nsoftware.IPWorksSSNMP.SnmpmgrEncryptionAlgorithms)(SnmpmgrEncryptionAlgorithms.encraAES);
            _snmpmgr.EncryptionPassword = "gak7280smDEM8smems";

            _snmpmgr.AuthenticationProtocol = (nsoftware.IPWorksSSNMP.SnmpmgrAuthenticationProtocols)(SnmpmgrAuthenticationProtocols.authpHMACSHA96);
                                                                                                                                                                            
            _snmpmgr.AuthenticationPassword = "14aThwfjo18pqjltcn";
            _snmpmgr.OnError += new Snmpmgr.OnErrorHandler(this.snmpmgr_OnError);
            _snmpmgr.OnResponse += new Snmpmgr.OnResponseHandler(this.snmpmgr_OnResponse);
            //_snmpmgr.OnDiscoveryRequest += _snmpmgr_OnDiscoveryRequest;
            //_snmpmgr.OnDiscoveryResponse += _snmpmgr_OnDiscoveryResponse;
            _snmpmgr.SNMPVersion = _defaultSnmpVersion;
            _snmpmgr.Community = _defaultSnmpCommunity;

            Console.WriteLine(" SNMP V3 ReSync::::version: " + _defaultSnmpVersion);
        }
        // Constructors
        public SnmpRequestManager()
            : this("") { }

        public SnmpRequestManager(string mibDirectory)
            : this("", "", mibDirectory) { }

        private string stored_localHost = "";
        private string stored_remoteHost = "";

        public SnmpRequestManager(string localHost, string remoteHost, string mibDirectory)
        {
            SnmpInit();
            // Initiliase SNMP manager

            stored_localHost = localHost;
            stored_remoteHost = remoteHost;
  

            if (localHost.Length > 0) _snmpmgr.LocalHost = localHost;
            if (remoteHost.Length > 0) _snmpmgr.RemoteHost = remoteHost;

        
            // Initialise MIB browser
            _mibbrowser = new Mibbrowser();

            // If no MIB directory specified, use module directory
            if (mibDirectory == null || mibDirectory.Length == 0)
            {
                mibDirectory = Path.GetDirectoryName(
                    new System.Uri(System.Reflection.Assembly.GetExecutingAssembly().CodeBase).LocalPath);
            }

            // Load all MIB files in MIB directory
            string[] mibFiles = System.IO.Directory.GetFiles(mibDirectory, "*.MIB");
            if (mibFiles.Length > 0)
            {
                foreach (string mibFile in mibFiles)
                {
                    try
                    {
                        _mibbrowser.LoadMib(mibFile);
                    }
                    catch (Exception e)
                    {
                        SnmpSignManager.LogError("Could not load MIB: " + mibFile + " - " + e.ToString());
                    }
                }
            }
            else
            {
                SnmpSignManager.LogError("No MIB files to load from: " + mibDirectory);
            }
          
          
            // Create query completion event
            _requestEvent = new AutoResetEvent(false);
        }

        private void GetAttributeSnmpObjects(XmlNode node, ref Dictionary<XmlNode, SnmpRequestObject> snmpObjects)
        {
            // Node has children?
            if (node.HasChildNodes)
            {
                // Iterate through children
                foreach (XmlNode childNode in node.ChildNodes)
                {
                    // Any attributes?
                    if (childNode.Attributes != null)
                    {
                        // Iterate through node attributes
                        foreach (XmlAttribute attribute in childNode.Attributes)
                        {
                            // Got count attribute?
                            if (attribute.Name == AttributeCount)
                            {
                                // Is attribute referring to SNMP oid?
                                SnmpRequestObject snmpObject = SnmpRequestObject.Create(attribute.Value.Trim(), ref _mibbrowser);
                                if (snmpObject != null)
                                {
                                    // Add it to the map
                                    snmpObjects[childNode] = snmpObject;
                                }
                                break;
                            }
                        }
                    }
                    // Not a text node?
                    if (childNode.Name != NodeNameText)
                    {
                        // Get attribute objects from children
                        GetAttributeSnmpObjects(childNode, ref snmpObjects);
                    }
                    else continue;
                }
            }
        }

        private void FillSnmpSequence(XmlNode node, int index)
        {
            // Node has children?
            if (node.HasChildNodes)
            {
                // Iterate through children
                foreach (XmlNode childNode in node.ChildNodes)
                {
                    // Text node?
                    if (childNode.Name == NodeNameText)
                    {
                        // Create temporary request object to 
                        SnmpRequestObject snmpObject = SnmpRequestObject.Create(childNode.Value.Trim(), ref _mibbrowser);
                        if (snmpObject != null)
                        {
                            // Instance placeholder present?
                            if (snmpObject.Instance.Contains("n"))
                            {
                                // Replace it with index
                                childNode.Value = childNode.Value.Replace("[n]", "[" + index.ToString() + "]");
                            }
                            // Field index or substring length placeholder present?
                            if (snmpObject.Index == -1
                                || snmpObject.Length == -1)
                            {
                                // Replace it with index
                                childNode.Value = childNode.Value.Replace(",n", "," + index.ToString());
                            }
                            // Substring start placeholder present?
                            if (snmpObject.Start == -1)
                            {
                                // Replace it with index
                                childNode.Value = childNode.Value.Replace("{n", "{" + index.ToString());
                            }
                        }
                        // Invalid Oid so ignore it
                        else continue;
                    }
                    else
                    {
                        // Fill node's children
                        FillSnmpSequence(childNode, index);
                    }
                }
            }
        }

        private int EvaluateParameters(string value, Dictionary<string,int> parameters)
        {
            if (parameters != null && value.Contains("@"))
            {
                // Replace all parameters with values
                foreach (KeyValuePair<string, int> parameter in parameters)
                {
                    value = value.Replace("@" + parameter.Key, parameter.Value.ToString());
                    if (!value.Contains("@")) break;
                }
            }

            // Evaluate expression and return it as integer
            return Convert.ToInt32(new NCalc.Expression(value).Evaluate());
        }

        private void ExpandSnmpSequences(XmlNode node, Dictionary<XmlNode, SnmpRequestObject> snmpObjects, Dictionary<string, int> parameters)
        {
            // Node has children?
            if (node.HasChildNodes)
            {
                try
                {
                    // Iterate through children
                    foreach (XmlNode childNode in node.ChildNodes)
                    {
                        int startIndex = 1, offsetIndex = -1;

                        if (childNode.Attributes != null)
                        {
                            // Iterate through node attributes (looking for start attribute)
                            foreach (XmlAttribute attribute in childNode.Attributes)
                            {
                                // Got start attribute?
                                if (attribute.Name == AttributeStart)
                                {
                                    // Use attribute value
                                    startIndex = EvaluateParameters(attribute.Value, parameters);

                                    // Remove attribute
                                    childNode.Attributes.Remove(attribute);
                                    break;
                                }
                            }

                            // Iterate through node attributes (looking for index attribute)
                            foreach (XmlAttribute attribute in childNode.Attributes)
                            {
                                // Got index attribute?
                                if (attribute.Name == AttributeIndex)
                                {
                                    // Use attribute value
                                    offsetIndex = EvaluateParameters(attribute.Value, parameters);

                                    // Remove attribute
                                    childNode.Attributes.Remove(attribute);
                                    break;
                                }
                            }

                            // Iterate through node attributes (looking for count attribute)
                            foreach (XmlAttribute attribute in childNode.Attributes)
                            {
                                // Got count attribute?
                                if (attribute.Name == AttributeCount)
                                {
                                    // Is attribute referring to SNMP oid?
                                    SnmpRequestObject snmpObject;
                                    string value;
                                    if (snmpObjects != null && snmpObjects.TryGetValue(childNode, out snmpObject))
                                    {
                                        // Use object value
                                        value = snmpObject.Value;
                                    }
                                    // Use attribute value
                                    else value = attribute.Value;

                                    // Remove attribute
                                    childNode.Attributes.Remove(attribute);

                                    XmlAttribute newAttribute;
                                    int count = EvaluateParameters(value, parameters);
                                    if (count > 0)
                                    {
                                        count += (startIndex - 1);
                                        for (int i = (startIndex + 1); i <= count; i++)
                                        {
                                            // Create clone of node
                                            XmlNode newChild = childNode.Clone();

                                            // Add index attribute
                                            newAttribute = childNode.OwnerDocument.CreateAttribute(AttributeIndex);
                                            newAttribute.Value = offsetIndex >= 0 ? ((i - startIndex) + offsetIndex).ToString() : i.ToString();
                                            newChild.Attributes.Append(newAttribute);

                                            // Replace placeholder with index
                                            FillSnmpSequence(newChild, i);

                                            // Append new node
                                            node.AppendChild(newChild);
                                        }

                                        // Replace placeholder with index
                                        FillSnmpSequence(childNode, startIndex);

                                        // Add index attribute
                                        newAttribute = childNode.OwnerDocument.CreateAttribute(AttributeIndex);
                                        newAttribute.Value = offsetIndex >= 0 ? offsetIndex.ToString() : startIndex.ToString();
                                        childNode.Attributes.Append(newAttribute);
                                    }
                                    else
                                    {
                                        // Remove node
                                        if (childNode.ParentNode != null)
                                        {
                                            childNode.ParentNode.RemoveChild(childNode);
                                        }
                                    }

                                    // No nested sequences
                                    return;
                                }
                            }
                        }

                        // Not a text node?
                        if (childNode.Name != NodeNameText)
                        {
                            // Process child nodes
                            ExpandSnmpSequences(childNode, snmpObjects, parameters);
                        }
                        else continue;
                    }
                }
                // Do nothing...
                catch { }
            }
        }

        private bool ExpandSnmpSequences(XmlNode node, Dictionary<string, int> parameters, int timeout, bool commsFault)
        {
            Dictionary<string, List<SnmpRequestObject>> requestObjects = new Dictionary<string, List<SnmpRequestObject>>();

            // Look for any oids used as sequence variables
            Dictionary<XmlNode, SnmpRequestObject> requestNodeObjects = new Dictionary<XmlNode, SnmpRequestObject>();
            GetAttributeSnmpObjects(node, ref requestNodeObjects);
            if (requestNodeObjects.Count > 0)
            {
                // Add all sequence variable oids to request object list
                foreach (KeyValuePair<XmlNode, SnmpRequestObject> item in requestNodeObjects)
                {
                    string oid = item.Value.Oid;
                    if (!requestObjects.ContainsKey(oid)) requestObjects.Add(oid, new List<SnmpRequestObject>() { item.Value });
                    else requestObjects[oid].Add(item.Value);
                }
                // Any objects to request?
                if (requestObjects.Count > 0)
                {
                    // Get values for request objects
                    if (!GetValues(requestObjects, parameters, timeout, commsFault))
                    {
                        return false;
                    }
                }
            }

            // Expand any sequences
            ExpandSnmpSequences(node, requestNodeObjects, parameters);

            return true;
        }

        private void GetRequestNodes(XmlNode node, ref List<SnmpRequestNode> requestNodes, ref Dictionary<string, List<SnmpRequestObject>> requestObjects)
        {
            GetRequestNodes(node, null, null, ref requestNodes, ref requestObjects);
        }

        private void GetRequestNodes(XmlNode node, Dictionary<string, int> parameters, ref List<SnmpRequestNode> requestNodes, ref Dictionary<string, List<SnmpRequestObject>> requestObjects)
        {
            GetRequestNodes(node, parameters, null, ref requestNodes, ref requestObjects);
        }

        private void GetRequestNodes(XmlNode node, Dictionary<string, int> parameters, Dictionary<string, string> attributes, ref List<SnmpRequestNode> requestNodes, ref Dictionary<string, List<SnmpRequestObject>> requestObjects)
        {
            // Node has children?
            if (node.HasChildNodes)
            {
                // Iterate through children
                foreach (XmlNode childNode in node.ChildNodes)
                {
                    // Text node?
                    if (childNode.Name == NodeNameText)
                    {
                        string value = childNode.Value.Trim();
                        string oid = SnmpRequestObject.GetOid(value, parameters, ref _mibbrowser);

                        if (oid.Length > 0)
                        {
                            SnmpRequestObject snmpObject = SnmpRequestObject.Create(value, parameters, attributes, ref _mibbrowser, _translateDelegate);
                            if (snmpObject != null)
                            {
                                // Add object to map
                                if (requestObjects != null)
                                {
                                    if (!requestObjects.ContainsKey(oid)) requestObjects.Add(oid, new List<SnmpRequestObject>() { snmpObject });
                                    else requestObjects[oid].Add(snmpObject);
                                }

                                // Add node to list
                                requestNodes.Add(new SnmpRequestNode(childNode, snmpObject));
                            }
                        }
                        // Invalid Oid so ignore it
                        else continue;
                    }
                    else
                    {
                        // Build request for node's children
                        GetRequestNodes(childNode, parameters, attributes, ref requestNodes, ref requestObjects);
                    }
                }
            }
        }

        public bool GetValues(ref string xml, bool commsFault)
        {
            // Use default timeout if none specified
            return GetValues(ref xml, null, 0, _defaultTimeout, commsFault);
        }

        public bool GetValues(ref string xml, Dictionary<string, int> parameters, bool commsFault)
        {
            // Use default timeout if none specified
            return GetValues(ref xml, parameters, 0, _defaultTimeout, commsFault);
        }

        public bool GetValues(ref string xml, Dictionary<string, int> parameters, int split, bool commsFault)
        {
            return GetValues(ref xml, parameters, split, _defaultTimeout, commsFault);
        }

        public bool GetValues(ref string xml, Dictionary<string, int> parameters, int split, int timeout, bool commsFault)
        {
            // Create XML reader (embed supplied xml in dummy root)
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            XmlReader xmlReader = XmlReader.Create(new StringReader(String.Format("<{0}>{1}</{0}>", ElementRoot, xml)), settings);
            XmlDocument xmlRequest = new XmlDocument();
            xmlRequest.Load(xmlReader);


            // Expand any SNMP sequences
            if (!ExpandSnmpSequences(xmlRequest, parameters, timeout, commsFault))
            {
                // Error...
                return false;
            }

            // Get list of nodes to request values for
            Dictionary<string, List<SnmpRequestObject>> requestObjects = new Dictionary<string, List<SnmpRequestObject>>();
            List<SnmpRequestNode> requestNodes = new List<SnmpRequestNode>();
            GetRequestNodes(xmlRequest, parameters, ref requestNodes, ref requestObjects);
            if (requestNodes.Count > 0)
            {

                try
                {
                //    Console.WriteLine("GetValue 521 " + DateTime.Now .ToString());

                //   this._snmpmgr.Discover();

                }
                catch (Exception ex)
                {
                    ex.ToString();
                }

                // Get values for request objects
                if (GetValues(requestObjects, parameters, split, timeout, commsFault))
                {
                    // Iterate through all returned values
                    foreach (KeyValuePair<string, List<SnmpRequestObject>> item in requestObjects)
                    {
                        // Iterate through all XML nodes in request
                        foreach (SnmpRequestNode requestNode in requestNodes)
                        {
                            foreach (SnmpRequestObject requestObject in item.Value)
                            {
                                // Matching object?
                                if (requestNode.SnmpObject == requestObject
                                    && requestObject.Set)
                                {
                                    // Get 'cooked' value, substitute and flag that value was set
                                    requestNode.Node.Value = requestNode.SnmpObject.Value;
                                    requestNode.Set = true;
                                }
                            }
                        }
                    }

                    // Iterate through all XML nodes in request
                    foreach (SnmpRequestNode requestNode in requestNodes)
                    {
                        // Value was not set for node?
                        if (!requestNode.Set && requestNode.Node.ParentNode != null)
                        {
                            // Remove node
                            requestNode.Node.ParentNode.RemoveChild(requestNode.Node);
                        }
                    }

                    // Set result xml
                    xml = xmlRequest.ChildNodes[0].InnerXml;

                    return true;
                }
            }

            return false;
        }

        public bool GetValues(ref string[] xml, bool commsFault)
        {
            // Use default timeout if none specified
            return GetValues(ref xml, null, 0, _defaultTimeout, commsFault);
        }

        public bool GetValues(ref string[] xml, Dictionary<string, int> parameters, bool commsFault)
        {
            // Use default timeout if none specified
            return GetValues(ref xml, parameters, 0, _defaultTimeout, commsFault);
        }

        public bool GetValues(ref string[] xml, Dictionary<string, int> parameters, int split, bool commsFault)
        {
            return GetValues(ref xml, parameters, split, _defaultTimeout, commsFault);
        }

        public bool GetValues(ref string[] xml, Dictionary<string, int> parameters, int split, int timeout, bool commsFault)
        {
            Dictionary<string, List<SnmpRequestObject>> requestObjects = new Dictionary<string, List<SnmpRequestObject>>();
            List<SnmpRequestNode> requestNodes = new List<SnmpRequestNode>();
            XmlDocument[] xmlRequests = new XmlDocument[xml.Length];

            XmlReaderSettings settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;

            try
            {
               // Console.WriteLine("GetValue 602");
              //this._snmpmgr.Discover();

          
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            int index = 0;
            foreach (string xmlItem in xml)
            {
                // Create XML reader (embed supplied xml in dummy root)
                XmlReader xmlReader = XmlReader.Create(new StringReader(String.Format("<{0}>{1}</{0}>", ElementRoot, xmlItem)), settings);
                xmlRequests[index] = new XmlDocument();
                xmlRequests[index].Load(xmlReader);

                // Expand any SNMP sequences
                if (!ExpandSnmpSequences(xmlRequests[index], parameters, timeout, commsFault))
                {
                    // Error...
                    return false;
                }

                // Get list of nodes to request values for
                GetRequestNodes(xmlRequests[index++], parameters, ref requestNodes, ref requestObjects);
            }

            if (requestNodes.Count > 0)
            {
                // Get values for request objects
                if (GetValues(requestObjects, parameters, split, timeout, commsFault))
                {
                    // Iterate through all returned values
                    foreach (KeyValuePair<string, List<SnmpRequestObject>> item in requestObjects)
                    {
                        // Iterate through all XML nodes in request
                        foreach (SnmpRequestNode requestNode in requestNodes)
                        {
                            foreach (SnmpRequestObject requestObject in item.Value)
                            {
                                // Matching object?
                                if (requestNode.SnmpObject == requestObject
                                    && requestObject.Set)
                                {
                                    // Get 'cooked' value, substitute and flag that value was set
                                    requestNode.Node.Value = requestNode.SnmpObject.Value;
                                    requestNode.Set = true;
                                }
                            }
                        }
                    }

                    // Iterate through all XML nodes in request
                    foreach (SnmpRequestNode requestNode in requestNodes)
                    {
                        // Value was not set for node?
                        if (!requestNode.Set && requestNode.Node.ParentNode != null)
                        {
                            // Remove node
                            requestNode.Node.ParentNode.RemoveChild(requestNode.Node);
                        }
                    }

                    index = 0;
                    foreach (XmlDocument xmlRequest in xmlRequests)
                    {
                        // Set result xml
                        xml[index++] = xmlRequest.ChildNodes[0].InnerXml;
                    }

                    return true;
                }
            }

            return false;
        }

        private bool GetValues(Dictionary<string, List<SnmpRequestObject>> requestObjects, Dictionary<string, int> parameters, int timeout, bool commsFault)
        {
            return GetValues(requestObjects, parameters, 0, timeout, commsFault);
        }

       
        private bool GetValues(Dictionary<string, List<SnmpRequestObject>> requestObjects, Dictionary<string, int> parameters, int split, int timeout, bool CommsFault)
        {
            if (requestObjects.Count > 0)
            {
                lock (_snmpLock)
                {
                    try
                    {
                   if(initDiscover || CommsFault)
                    this._snmpmgr.Discover();
                        initDiscover = false;
                     }
                    catch (IPWorksSSNMPException ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                    int objects = requestObjects.Count, index = 0;

                    // Build a list of all objects in request
                    List<SNMPObject> snmpObjects = new List<SNMPObject>(objects);
                    foreach (KeyValuePair<string, List<SnmpRequestObject>> item in requestObjects)
                    {
                        snmpObjects.Add(new SNMPObject(item.Key));
                        foreach (SnmpRequestObject requestObject in item.Value)
                        {
                            requestObject.Set = false;
                        }
                    }

                    _requestObjects = requestObjects;
                    while (objects > 0)
                    {
                        // Reduce this chunk to remaining objects? Or no chunk size specified?
                        if (split > objects || split == 0) split = objects;

                        // Add oids to list of objects to query
                        _snmpmgr.Objects.Clear();
                        for (int i = 0; i < split; i++)
                        {
                            _snmpmgr.Objects.Add(snmpObjects[index++]);
                            objects--;
                        }

                        try
                        {
                            // Send SNMP request
                            _requestResult = false;
                            _snmpmgr.Timeout = timeout;
                            _snmpmgr.SendGetRequest();

                            // Wait for response
                            if (!(_requestEvent.WaitOne(timeout * 1000)
                                && _requestResult))
                            {
                                // Something went wrong...
                                return false;
                            }
                                
                        }
                        catch (Exception e)
                        {
                            // Not a timeout?
                            SnmpSignManager.LogError(e.ToString());

                            try
                            {
                                SnmpSignManager.LogInfo("Going into SnmpInit()");
                                SnmpInit();
                                if (stored_localHost.Length > 0) _snmpmgr.LocalHost = stored_localHost;
                                if (stored_remoteHost.Length > 0) _snmpmgr.RemoteHost = stored_remoteHost;
                                SnmpSignManager.LogInfo("Trying to Discover");
                                _snmpmgr.Discover();
                                
                            }
                            catch(IPWorksSSNMPException ex)
                            {
                                SnmpSignManager.LogError(ex.ToString());
                            }

                            if (!e.Message.ToUpper().Contains("TIMEOUT"))
                            {
                                // Pass it on
                                Console.WriteLine("Thrown Timout Exception");
                                throw e;
                            }
                            else
                            {
                            //    Console.WriteLine("Going to SnmpInit()");
                              //  SnmpInit();
                               
                            }

                            return false;
                        }
                    }

                    return true;
                }
            }

            return false;
        }

        public bool SetValues(ref string xml, bool commsFault)
        {
            // Use default timeout if none specified
            return SetValues(ref xml, null, null, 0, _defaultTimeout, commsFault);
        }

        public bool SetValues(ref string xml, Dictionary<string, int> parameters, bool commsFault)
        {
            // Use default timeout if none specified
            return SetValues(ref xml, parameters, null, 0, _defaultTimeout, commsFault);
        }

        public bool SetValues(ref string xml, Dictionary<string, int> parameters, int timeout, bool commsFault)
        {
            return SetValues(ref xml, parameters, null, 0, timeout, commsFault);
        }

        public bool SetValues(ref string xml, Dictionary<string, int> parameters, Dictionary<string, string> attributes, bool commsFault)
        {
            // Use default timeout if none specified
            return SetValues(ref xml, parameters, attributes, 0, _defaultTimeout, commsFault);
        }

        public bool SetValues(ref string xml, Dictionary<string, int> parameters, Dictionary<string, string> attributes, int split, bool commsFault)
        {
            // Use default timeout if none specified
            return SetValues(ref xml, parameters, attributes, split, _defaultTimeout, commsFault);
        }

        bool initDiscover = true;
        public bool SetValues(ref string xml, Dictionary<string, int> parameters, Dictionary<string, string> attributes, int split, int timeout, bool commsFault)
        {
            // Create XML reader (embed supplied xml in dummy root)
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            XmlReader xmlReader = XmlReader.Create(new StringReader(String.Format("<{0}>{1}</{0}>", ElementRoot, xml)), settings);
            XmlDocument xmlRequest = new XmlDocument();
            xmlRequest.Load(xmlReader);
            try
            {
                
                if (initDiscover || commsFault)
                _snmpmgr.Discover();
                initDiscover = false;
            }
            catch(IPWorksSSNMPException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            
            // Expand any SNMP sequences
            if (!ExpandSnmpSequences(xmlRequest, parameters, timeout, commsFault))
            {
                // Error...
                return false;
            }

            // Get list of nodes to set values for
            Dictionary<string, List<SnmpRequestObject>> requestObjects = new Dictionary<string, List<SnmpRequestObject>>();
            List<SnmpRequestNode> requestNodes = new List<SnmpRequestNode>();
            GetRequestNodes(xmlRequest, parameters, attributes, ref requestNodes, ref requestObjects);
            if (requestNodes.Count > 0)
            {
                // Get values for request objects
                if (SetValues(requestObjects, parameters, split, timeout))
                {
                    // Iterate through all returned values
                    foreach (KeyValuePair<string, List<SnmpRequestObject>> item in requestObjects)
                    {
                        // Iterate through all XML nodes in request
                        foreach (SnmpRequestNode requestNode in requestNodes)
                        {
                            foreach (SnmpRequestObject requestObject in item.Value)
                            {
                                // Matching object?
                                if (requestNode.SnmpObject == requestObject
                                    && requestObject.Set)
                                {
                                    // Get 'cooked' value, substitute and flag that value was set
                                    requestNode.Node.Value = requestNode.SnmpObject.Value;
                                    requestNode.Set = true;
                                }
                            }
                        }
                    }

                    // Iterate through all XML nodes in request
                    foreach (SnmpRequestNode requestNode in requestNodes)
                    {
                        // Value was not set for node?
                        if (!requestNode.Set && requestNode.Node.ParentNode != null)
                        {
                            // Remove node
                            requestNode.Node.ParentNode.RemoveChild(requestNode.Node);
                        }
                    }

                    // Set result xml
                    xml = xmlRequest.ChildNodes[0].InnerXml;

                    return true;
                }
            }

            return false;
        }

        private bool SetValues(Dictionary<string, List<SnmpRequestObject>> requestObjects, Dictionary<string, int> parameters, int timeout)
        {
            return SetValues(requestObjects, parameters, 0, timeout);
        }

        private bool SetValues(Dictionary<string, List<SnmpRequestObject>> requestObjects, Dictionary<string, int> parameters, int split, int timeout)
        {
            if (requestObjects.Count > 0)
            {
                lock (_snmpLock)
                {
                    try
                    {
                        Console.WriteLine("Discover Snmp 884p");
                       // _snmpmgr.Discover();

                    }
                    catch(Exception ex)
                    {
                        ex.ToString();
                        Console.WriteLine("SetValue Exception 890p ");


                    }
                    int objects = requestObjects.Count, index = 0;

                    // Build a list of all objects in request
                    List<SNMPObject> snmpObjects = new List<SNMPObject>(objects);
                    foreach (KeyValuePair<string, List<SnmpRequestObject>> item in requestObjects)
                    {
                        foreach (SnmpRequestObject requestObject in item.Value)
                        {
                            snmpObjects.Add(new SNMPObject(item.Key,
                                    System.Text.ASCIIEncoding.UTF8.GetBytes(requestObject.Value), requestObject.Type));
                            requestObject.Set = false;
                        }
                    }

                    _requestObjects = requestObjects;
                    while (objects > 0)
                    {
                        // Reduce this chunk to remaining objects? Or no chunk size specified?
                        if (split > objects || split == 0) split = objects;

                        // Add oids to list of objects to query
                        _snmpmgr.Objects.Clear();
                        for (int i = 0; i < split; i++)
                        {
                            _snmpmgr.Objects.Add(snmpObjects[index++]);
                            objects--;
                        }

                        try
                        {
                            // Send SNMP request
                            _requestResult = false;
                            _snmpmgr.Timeout = timeout;
                            Console.WriteLine("SendSetRequest");

                            _snmpmgr.SendSetRequest();

                            // Wait for response
                            if (!(_requestEvent.WaitOne(timeout * 1000)
                                && _requestResult))
                            {
                                // Something went wrong...
                                return false;
                            }

                        }
                        catch (Exception e)
                        {
                            // Not a timeout?
                            if (!e.Message.ToUpper().Contains("TIMEOUT"))
                            {
                                // Pass it on
                                throw e;
                            }

                            return false;
                        }
                    }

                    return true;
                }
            }

            return false;
        }

        private void snmpmgr_OnResponse(object sender, SnmpmgrResponseEventArgs e)
        {
           
            try
            {
                Console.WriteLine("Discover snmp On Response");
             //  this._snmpmgr.Discover();

            }
            catch(Exception ex)
            {
                ex.ToString();
            }
            // No errors?
            if (e.ErrorStatus == 0)
            {
                // Iterate through all objects in response
                foreach (SNMPObject snmpObject in _snmpmgr.Objects)
                {
                    // Look for request object for oid
                    List<SnmpRequestObject> requestObjects;
                    if (_requestObjects.TryGetValue(snmpObject.Oid, out requestObjects))
                    {
                        foreach (SnmpRequestObject requestObject in requestObjects)
                        {
                            // Set 'raw' value
                            requestObject.Value = snmpObject.Value;
                        }
                    }
                }

                _requestResult = true;
            }
            // Don't log 'commitFailed' errors
            else if (!e.ErrorDescription.ToUpper().Contains("COMMITFAILED"))
            {
                SnmpSignManager.LogError(e.ErrorDescription);
            }

            // Flag that response was received
            Console.WriteLine("On Response requestEventSet");
            _requestEvent.Set();
        }

        private void snmpmgr_OnError(object sender, SnmpmgrErrorEventArgs e)
        {
            SnmpSignManager.LogError(e.Description);
        }

        // Accessors
        public string LocalHost
        {
            get { return _snmpmgr.LocalHost; }

            set { _snmpmgr.LocalHost = value; }
        }

        public string RemoteHost
        {
            get { return _snmpmgr.RemoteHost; }

            set { _snmpmgr.RemoteHost = value; }
        }

        public int SnmpVersion
        {
            get { return (int) _snmpmgr.SNMPVersion; }
            set { _snmpmgr.SNMPVersion = (SnmpmgrSNMPVersions) value; }
        }

        public string SnmpCommunity
        {
            get { return _snmpmgr.Community; }
            set { _snmpmgr.Community = value; }
        }

        public static int DefaultTimeout
        {
            get { return _defaultTimeout; }
        }

        public static int ValidateTimeOut
        {
            get { return _validateTimeOut; }
        }

        public int Timeout
        {
            get { return _timeout; }
            set { _timeout = value; }
        }

        public TranslateValue TranslateDelegate
        {
            set { _translateDelegate = value; }
        }
    }
}
