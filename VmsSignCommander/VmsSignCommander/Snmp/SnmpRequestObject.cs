﻿using System;
using System.Xml;
using System.IO;
using System.Collections.Generic;
using nsoftware.IPWorksSSNMP;
using System.Threading;

namespace VMS_SNMP
{
    public partial class SnmpRequestManager
    {
        private class SnmpRequestObject
        {
            private string _oid;
            private string _instance;
            private string _value;
            private string _function;
            private string[] _functionParameters;
            private int _index;
            private int _start;
            private int _length;
            private char _fill;
            private SNMPObjectTypes _type;
            private bool _set;

            private TranslateValue _translateDelegate;

            public SnmpRequestObject(string oid, string instance, string value, int index, SNMPObjectTypes type)
            {
                this._oid = oid;
                this._instance = instance;
                this._value = value;
                this._function = "";
                this._index = index;
                this._start = 0;
                this._length = 0;
                this._fill = ' ';
                this._type = type;
            }

            public static string GetOid(string name, Dictionary<string,int> parameters, ref Mibbrowser mibBrowser)
            {
                string instance = "";

                // Function?
                if (name.StartsWith("$"))
                {
                    // Check for function parameters (oid must be last parameter)
                    int parameterStartIndex = name.LastIndexOf('(');
                    int parameterEndIndex = name.LastIndexOf(')');
                    if (parameterStartIndex > 0 && parameterEndIndex > 0
                        && parameterEndIndex > parameterStartIndex + 1)
                    {
                        name = name.Substring(parameterStartIndex + 1, (parameterEndIndex - parameterStartIndex) - 1);
                    }
                }

                // Check for value
                int valueStartIndex = name.IndexOf(':');
                if (valueStartIndex > 0)
                {
                    name = name.Substring(0, valueStartIndex);
                }

                // Check for substrings
                int subsStringStartIndex = name.IndexOf('{');
                if (subsStringStartIndex > 0)
                {
                    name = name.Substring(0, subsStringStartIndex);
                }

                // Check for delimiters
                int delimiterStartIndex = name.IndexOf(',');
                if (delimiterStartIndex > 0)
                {
                    name = name.Substring(0, delimiterStartIndex);
                }

                // Check for array subscript
                int subscriptStartIndex = name.IndexOf('[');
                int subscriptEndIndex = name.LastIndexOf(']');
                if (subscriptStartIndex > 0 && subscriptEndIndex > 0
                    && subscriptEndIndex > subscriptStartIndex + 1)
                {
                    string item = name.Substring(subscriptStartIndex + 1, (subscriptEndIndex - subscriptStartIndex) - 1);
                    try
                    {
                        // Use subscript as instance
                        instance = EvaluateParameters(item, parameters).ToString();
                        name = name.Substring(0, subscriptStartIndex);
                    }
                    // Do nothing...
                    catch { }
                }

             

                    // Get oid from value
                    string oid = OidFromName(name, ref mibBrowser);
                if (oid.Length > 0)
                {
                    // No instance specified?
                    if (instance.Length == 0)
                    {
                        // Default instance is '1' for table otherwise '0'
                        instance = ((IsOidTable(oid, ref mibBrowser)) ? "1" : "0");
                    }

                    return String.Format("{0}.{1}", oid, instance);
                }
                // Possibly numeric oid?
                else if (name.Contains("."))
                {
                    // Verify that oid is numeric
                    bool isValidOid = true;
                    foreach (string subNode in name.Split(new char[] { '.' }))
                    {
                        try
                        {
                            Convert.ToInt32(subNode);
                        }
                        catch
                        {
                            isValidOid = false;
                            break;
                        }
                    }

                    // Valid oid?
                    if (isValidOid)
                    {
                        if (instance.Length == 0)
                        {
                            oid = OidNode(name);
                            instance = OidInstance(name);
                        }
                        else
                        {
                            oid = name;
                        }

                        return String.Format("{0}.{1}", oid, instance);
                    }
                }

                return "";
            }

            public static SnmpRequestObject Create(string name, ref Mibbrowser mibBrowser)
            {
                return Create(name, null, null, ref mibBrowser, null);
            }

            public static SnmpRequestObject Create(string name, Dictionary<string, int> parameters, ref Mibbrowser mibBrowser)
            {
                return Create(name, parameters, null, ref mibBrowser, null);
            }

            public static SnmpRequestObject Create(string name, Dictionary<string, int> parameters, Dictionary<string, string> attributes, ref Mibbrowser mibBrowser, TranslateValue translateDelegate)
            {
                string function = "";
                string[] functionParameters = null;
                string instance = "";
                string value = "";
                int index = 0;

                // Function?
                if (name.StartsWith("$"))
                {
                    // Check for function parameters
                    int parameterStartIndex = name.IndexOf('(');
                    int parameterEndIndex = name.LastIndexOf(')');
                    if (parameterStartIndex > 0 && parameterEndIndex > 0
                        && parameterEndIndex > parameterStartIndex + 1)
                    {
                        // Get function name
                        function = name.Substring(1, parameterStartIndex - 1);

                        // Get list of function parameters
                        string[] fnParameters = name.Substring(parameterStartIndex + 1,
                            (parameterEndIndex - parameterStartIndex)).Split(new char[] { '(' });
                        List<string> parameterList = new List<string>();
                        foreach (string parameter in fnParameters)
                        {
                            if (parameter.EndsWith(")"))
                            {
                                parameterList.Add(parameter.Remove(parameter.Length - 1));
                            }
                            // Error, unbalanced parentheses
                            else return null;
                        }

                        // Last parameter is oid so remove it
                        parameterList.RemoveAt(parameterList.Count - 1);
                        functionParameters = parameterList.ToArray();

                        // Oid is last paramter
                        parameterStartIndex = name.LastIndexOf('(');
                        name = name.Substring(parameterStartIndex + 1, (parameterEndIndex - parameterStartIndex) - 1);
                    }
                }

                // Check for value
                int valueStartIndex = name.IndexOf(':');
                if (valueStartIndex > 0)
                {
                    // Save value
                    value = name.Substring(valueStartIndex + 1);
                    name = name.Substring(0, valueStartIndex);

                    // Any attributes to replace?
                    if (attributes != null && value.Contains("@"))
                    {
                        // Replace any attributes 
                        foreach (KeyValuePair<string, string> attribute in attributes)
                        {
                            value = value.Replace("@" + attribute.Key, attribute.Value);
                        }
                    }
                }

                // Check for substrings
                int subStringStart = 0, subStringLength = 0;
                char subStringFill = ' ';
                int subsStringStartIndex = name.IndexOf('{');
                int subsStringEndIndex = name.LastIndexOf('}');
                if (subsStringStartIndex > 0 && subsStringEndIndex > 0
                    && subsStringEndIndex > subsStringStartIndex + 1)
                {
                    string subString = name.Substring(subsStringStartIndex + 1, (subsStringEndIndex - subsStringStartIndex) - 1);
                    string[] subStringParameters = subString.Split(new char[] { ',' });
                    if (subStringParameters.Length >= 1)
                    {
                        try
                        {
                            if (subStringParameters[0] != "n")
                            {
                                subStringStart = Convert.ToInt32(subStringParameters[0]);
                            }
                            else subStringStart = -1;
                            if (subStringParameters.Length >= 2)
                            {
                                if (subStringParameters[1] != "n")
                                {
                                    subStringLength = Convert.ToInt32(subStringParameters[1]);
                                }
                                else subStringLength = -1;
                            }
                            else subStringLength = 1;
                            if (subStringParameters.Length >= 3)
                            {
                                subStringFill = subStringParameters[3][0];
                            }
                            else subStringFill = ' ';
                            name = name.Substring(0, subsStringStartIndex);
                        }
                        // Do nothing...
                        catch { }
                    }
                }

                // Check for delimiters
                int delimiterStartIndex = name.IndexOf(',');
                if (delimiterStartIndex > 0)
                {
                    string item = name.Substring(delimiterStartIndex + 1);

                    // Placeholder?
                    if (item == "n")
                    {
                        index = -1;
                        name = name.Substring(0, delimiterStartIndex);
                    }
                    else
                    {
                        try
                        {
                            index = Convert.ToInt32(item);
                            name = name.Substring(0, delimiterStartIndex);
                        }
                        // Do nothing...
                        catch { }
                    }
                }

                // Check for array subscript
                int subscriptStartIndex = name.IndexOf('[');
                int subscriptEndIndex = name.LastIndexOf(']');
                if (subscriptStartIndex > 0 && subscriptEndIndex > 0
                    && subscriptEndIndex > subscriptStartIndex + 1)
                {
                    string item = name.Substring(subscriptStartIndex + 1, (subscriptEndIndex - subscriptStartIndex) - 1);
                    // Placeholder?
                    if (item == "n")
                    {
                        instance = "n";
                        name = name.Substring(0, subscriptStartIndex);
                    }
                    else
                    {
                        try
                        {
                            // Use subscript as instance
                            instance = EvaluateParameters(item, parameters).ToString();
                            name = name.Substring(0, subscriptStartIndex);
                        }
                        // Do nothing...
                        catch { }
                    }
                }

                // Get oid from value
                string oid = OidFromName(name, ref mibBrowser);
                if (oid.Length > 0)
                {
                    // No instance specified?
                    if (instance.Length == 0)
                    {
                        // Default instance is '1' for table otherwise '0'
                        instance = ((IsOidTable(oid, ref mibBrowser)) ? "1" : "0");
                    }

                    SnmpRequestObject snmpObject = new SnmpRequestObject(oid, instance, value, index,
                        ObjectTypeFromSyntax(SyntaxFromName(name, ref mibBrowser)));
                    snmpObject._translateDelegate = translateDelegate;

                    if (function.Length != 0)
                    {
                        snmpObject._function = function;
                        snmpObject._functionParameters = functionParameters;
                    }
                
                    // Use substring?
                    if (subStringStart > 0 || subStringLength > 0)
                    {
                        snmpObject._start = subStringStart;
                        snmpObject._length = subStringLength;
                        snmpObject._fill = subStringFill;
                    }

                    return snmpObject;
                }
                // Possibly numeric oid?
                else if (name.Contains("."))
                {
                    // Verify that oid is numeric
                    bool isValidOid = true;
                    foreach (string subNode in name.Split(new char[] { '.' }))
                    {
                        try
                        {
                            Convert.ToInt32(subNode);
                        }
                        catch
                        {
                            isValidOid = false;
                            break;
                        }
                    }

                    // Valid oid?
                    if (isValidOid)
                    {
                        if (instance.Length == 0)
                        {
                            oid = OidNode(name);
                            instance = OidInstance(name);
                        }
                        else
                        {
                            oid = name;
                        }

                        SnmpRequestObject snmpObject = new SnmpRequestObject(oid, instance, value, index, SNMPObjectTypes.otOctetString);
                        snmpObject._translateDelegate = translateDelegate;

                        return snmpObject;
                    }
                }

                return null;
            }

            private static string OidToName(string oid, ref Mibbrowser mibBrowser)
            {
                // Extract instance from oid
                int lastDotIndex = oid.LastIndexOf('.');
                if (lastDotIndex > 0)
                {
                    string instance = oid.Substring(lastDotIndex + 1);
                    string node = oid.Substring(0, lastDotIndex);
                    try
                    {
                        // Find oid in MIB browser
                        mibBrowser.SelectNode(node);

                        // Return node label (name)
                        return mibBrowser.NodeLabel;
                    }
                    // Do nothing...
                    catch { }
                }

                return oid;
            }

            private static string OidNode(string oid)
            {
                // Remove instance from oid
                int lastDotIndex = oid.LastIndexOf('.');
                if (lastDotIndex > 0)
                {
                    return oid.Substring(0, lastDotIndex);
                }

                return oid;
            }

            private static string OidInstance(string oid)
            {
                // Extract instance from oid
                int lastDotIndex = oid.LastIndexOf('.');
                if (lastDotIndex > 0)
                {
                    return oid.Substring(lastDotIndex + 1);
                }

                return "";
            }

            private static bool IsOidTable(string oid, ref Mibbrowser mibBrowser)
            {
                // Get parent oid
                int lastDotIndex = oid.LastIndexOf('.');
                if (lastDotIndex > 0)
                {
                    string node = oid.Substring(0, lastDotIndex);
                    try
                    {
                        // Find parent node in MIB browser
                        mibBrowser.SelectNode(node);

                        // Is parent node a table?
                        return (mibBrowser.NodeSyntax == MibbrowserNodeSyntaxes.stxSequence);
                    }
                    // Do nothing...
                    catch { }
                }

                return false;
            }

            private static string OidFromName(string name, ref Mibbrowser mibBrowser)
            {
                try
                {
                    // Find node in MIB browser
                    mibBrowser.SelectNode(name);

                    // Return node oid
                    return mibBrowser.NodeOid;
                }
                // Do nothing...
                catch { }

                return "";
            }

            private static MibbrowserNodeSyntaxes SyntaxFromName(string name, ref Mibbrowser mibBrowser)
            {
                try
                {
                    // Find node in MIB browser
                    mibBrowser.SelectNode(name);

                    // Return node syntax
                    return mibBrowser.NodeSyntax;
                }
                // Do nothing...
                catch { }

                return MibbrowserNodeSyntaxes.stxOctetString;
            }

            private static SNMPObjectTypes ObjectTypeFromSyntax(MibbrowserNodeSyntaxes syntax)
            {
                switch (syntax)
                {
                    case MibbrowserNodeSyntaxes.stxBoolean:
                    case MibbrowserNodeSyntaxes.stxInteger:
                    case MibbrowserNodeSyntaxes.stxUInteger:
                        return SNMPObjectTypes.otInteger;

                    case MibbrowserNodeSyntaxes.stxIpAddress:
                    case MibbrowserNodeSyntaxes.stxNetworkAddress:
                        return SNMPObjectTypes.otIPAddress;

                    case MibbrowserNodeSyntaxes.stxTimeTicks:
                        return SNMPObjectTypes.otTimeTicks;

                    case MibbrowserNodeSyntaxes.stxCounter:
                        return SNMPObjectTypes.otCounter32;

                    case MibbrowserNodeSyntaxes.stxCounter64:
                        return SNMPObjectTypes.otCounter64;

                    default:
                        return SNMPObjectTypes.otOctetString;
                }
            }

            private static int EvaluateParameters(string value, Dictionary<string, int> parameters)
            {
                if (parameters != null && value.Contains("@"))
                {
                    // Replace all parameters with values
                    foreach (KeyValuePair<string, int> parameter in parameters)
                    {
                        value = value.Replace("@" + parameter.Key, parameter.Value.ToString());
                        if (!value.Contains("@")) break;
                    }
                }

                // Evaluate expression and return it as integer
                return Convert.ToInt32(new NCalc.Expression(value).Evaluate());
            }

            private string FunctionValue()
            {
                // Numeric function?
                if (_function == "number")
                {
                    try
                    {
                        int value = Convert.ToInt32(_value);
                        if (_functionParameters.Length >= 1)
                        {
                            // Check function type
                            if (_functionParameters[0] == "isZero")
                            {
                                return (value == 0) ? "1" : "0";
                            }
                            if (_functionParameters[0] == "isNotZero")
                            {
                                return (value == 0) ? "0" : "1";
                            }
                            else if (_functionParameters[0] == "equals")
                            {
                                if (_functionParameters.Length >= 2)
                                {
                                    try
                                    {
                                        int parameter = Convert.ToInt32(_functionParameters[1]);
                                        return (value == parameter) ? "1" : "0";
                                    }
                                    catch
                                    {
                                        return "0";
                                    }
                                }
                                else return "0";
                            }
                            else if (_functionParameters[0] == "notEquals")
                            {
                                if (_functionParameters.Length >= 2)
                                {
                                    try
                                    {
                                        int parameter = Convert.ToInt32(_functionParameters[1]);
                                        return (value == parameter) ? "0" : "1";
                                    }
                                    catch
                                    {
                                        return "1";
                                    }
                                }
                                else return "1";
                            }
                        }
                    }
                    // Do nothing...
                    catch { }
                }
                // String function?
                else if (_function == "string")
                {
                    if (_functionParameters.Length >= 1)
                    {
                        // Check function type
                        if (_functionParameters[0] == "isEmpty")
                        {
                            return (_value.Length > 0) ? "0" : "1";
                        }
                        else if (_functionParameters[0] == "isNotEmpty")
                        {
                            return (_value.Length > 0) ? "1" : "0";
                        }
                        else if (_functionParameters[0] == "contains")
                        {
                            if (_functionParameters.Length >= 2)
                            {
                                if (_functionParameters.Length >= 3)
                                {
                                    if (_value.Contains(_functionParameters[2])) 
                                        {
                                        return "2";
                                    }
                                }                          
                                    return (_value.Contains(_functionParameters[1])) ? "1" : "0";
                             } 
                            else return "0";
                        }
                        else if (_functionParameters[0] == "notContains")
                        {
                            if (_functionParameters.Length >= 2)
                            {
                                return (_value.Contains(_functionParameters[1])) ? "0" : "1";
                            }
                            else return "1";
                        }
                        else if (_functionParameters[0] == "equals")
                        {
                            if (_functionParameters.Length >= 2)
                            {
                                return (_value.Equals(_functionParameters[1])) ? "1" : "0";
                            }
                            else return "0";
                        }
                        else if (_functionParameters[0] == "notEquals")
                        {
                            if (_functionParameters.Length >= 2)
                            {
                                return (_value.Equals(_functionParameters[1])) ? "0" : "1";
                            }
                            else return "1";
                        }
                    }
                }
                else if (_function == "base64")
                {
                    byte[] value = System.Text.Encoding.UTF8.GetBytes(_value);
                    return System.Convert.ToBase64String(value);
                }
                return _value;
            }

            public string Oid
            {
                get { return _oid + "." + _instance; }
            }

            public string Instance
            {
                get { return _instance; }
            }

            public int Index
            {
                get { return _index; }
            }

            public int Start
            {
                get { return _start; }
            }

            public int Length
            {
                get { return _length; }
            }

            public SNMPObjectTypes Type
            {
                get { return _type; }
            }

            public string Value
            {
                get
                {
                    string value = this._value;

                    // Value was set?
                    if (_set)
                    {
                        // Split value on delimiters?
                        if (_index > 0)
                        {
                            string[] subStrings = value.Split(new char[] { ',' });
                            if (_index <= subStrings.Length)
                            {
                                // Substitute value and flag that value was set
                                value = subStrings[_index - 1];
                            }
                        }

                        // Using a substring of the value?
                        if ((_start > 0 || _length > 0) && value.Length > 0)
                        {
                            // Needs padding?
                            if (value.Length < (_start + _length))
                            {
                                try
                                {
                                    // If it's a number, right justify
                                    System.Convert.ToInt32(value);
                                    value = value.PadLeft(_start + _length, _fill);
                                }
                                catch
                                {
                                    // Left justify
                                    value = value.PadRight(_start + _length, _fill);
                                }
                            }
                            // Get specified substring
                            value = value.Substring(_start, _length);
                        }

                        // Call function on value?
                        if (value.Length > 0 && _function.Length > 0)
                        {
                            // Translate function?
                            if (_function == "translate")
                            {
                                // Translate delegate registered?
                                if (_translateDelegate != null
                                    && _functionParameters != null && _functionParameters.Length == 1)
                                {
                                    // Translate value using delegate
                                    value = _translateDelegate(_functionParameters[0], value);
                                }
                            }
                            else
                            {
                                // Built-in function
                                value = FunctionValue();
                            }
                        }
                    }

                    return value;
                }

                set
                {
                    this._value = value;
                    _set = true;
                }
            }

            public bool Set
            {
                get { return _set; }
                set { _set = value; }
            }
        }
    }
}