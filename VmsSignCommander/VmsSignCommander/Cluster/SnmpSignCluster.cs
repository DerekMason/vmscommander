﻿using System;
using System.Xml;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Timers;
using System.Threading;
using ServiceBrokerListener.Domain;

namespace VMS_SNMP
{
    public partial class SnmpSignCluster : ClusterRecord
    {
        // Constants
        public const string ProcedureSignFault = "SignIsInFault";

        // Defaults
        public const int DefaultTimeoutMargin = 5;

        // Members
        private List<ClusterMemberRecord> _clusterMembers;
        private string _clusterMemberSet;

        private SqlDependencyEx _actionDependency = null;
        private SqlDependencyEx _signActionDependency = null;
        private SqlDependencyEx _signStatusDependency = null;
        private SqlDependencyEx _signActiveDependency = null;

        private System.Timers.Timer _actionTimer, _watchdogTimer;
        private ElapsedEventHandler _actionTimerHandler, _watchdogTimerHandler;

        private Semaphore _actionProcessing;

        private delegate void ProcessActionDelegate(ClusterActionRecord action);

        private Dictionary<string, ProcessActionDelegate> _actions;
        private ClusterActionRecord _currentAction;
        private object _actionLock;

        private Dictionary<string, string> _statuses;

        // Constructors
        public SnmpSignCluster(IDataRecord record)
            : this(IDataRecordConvert.ToInt(record, ColumnId),
            IDataRecordConvert.ToInt(record, ColumnPlacementId),
            IDataRecordConvert.ToString(record, ColumnDisplayName),
            IDataRecordConvert.ToString(record, ColumnDescription),
            IDataRecordConvert.ToString(record, ColumnInfo),
            IDataRecordConvert.ToBoolean(record, ColumnActive))
        { }

        public SnmpSignCluster(int id, int placementId, string displayName, string description, string info, bool active)
            : base(id, placementId, displayName, description, info, active)
        {
         
            // Populate actions
            _actions = new Dictionary<string, ProcessActionDelegate>()
            {
                { ClusterActionRecord.ActionBlank, ProcessBlank },
                { ClusterActionRecord.ActionPoll, ProcessPoll },
                { ClusterActionRecord.ActionSet, ProcessSet },
                { ClusterActionRecord.ActionValidate, ProcessValidate },
                { ClusterActionRecord.ActionSetLuminance, ProcessSetLuminance },
                { ClusterActionRecord.ActionClearLuminance, ProcessClearLuminance },
                {ClusterActionRecord.ActionSetLuminanceOverride, ProcessSetLuminanceOverride },
                {ClusterActionRecord.ActionDimBrightLuminance,ProcessSetDimBrightLuminance },
                {ClusterActionRecord.ActionFaultLedMode,ProcessFaultLedActionMode }
            };

            // Create event handlers
            _actionTimerHandler = new ElapsedEventHandler(timer_OnElapsedEventHandler);
            _watchdogTimerHandler = new ElapsedEventHandler(timer_OnElapsedEventHandler);

            // Create action semaphore
            _actionProcessing = new Semaphore(0, 1);
            _actionLock = new object();

            _statuses = new Dictionary<string, string>();
        }

        ~SnmpSignCluster()
        {
            Stop();
        }

        // Initialisation
        public bool Initialise(SnmpSignManager manager)
        {
            // Cluster is not active, can't initialise
            if (!_active) return false;

            try
            {
                // Load cluster members
                LoadClusterMembers();

                // Load XML templates
                LoadTemplates();

                // Set up notification for change in sign status
                WaitForSignStatus();

                // Set up notification for change in sign availability
                WaitForSignActive();

                return true;
            }
            catch (Exception e)
            {
                LogError(e.ToString());
            }

            return false;
        }

        public void Start()
        {
            // Flag that cluster is active
            _active = true;

            // Process next action in a thread from the pool
            ThreadPool.QueueUserWorkItem(ProcessNextActionItem, this);
        }

        public void Stop()
        {
            // Flag that cluster is inactive
            _active = false;

            // Stop timers
            timer_StopActionTimer();
            timer_StopWatchdogTimer();

            // Busy processing action?
            if (!_actionProcessing.WaitOne(0))
            {
                _actionProcessing.WaitOne();
            }

            // Dispose of SqlDependencyEx objects
            if (_actionDependency != null)
            {
                _actionDependency.Stop();
                _actionDependency.TableChanged -= sql_OnActionChangeHandler;
                _actionDependency = null;
            }
            if (_signActionDependency != null)
            {
                _signActionDependency.Stop();
                _signActionDependency.TableChanged -= sql_OnSignActionChangeHandler;
                _signActionDependency = null;
            }
            if (_signStatusDependency != null)
            {
                _signStatusDependency.Stop();
                _signStatusDependency.TableChanged -= sql_OnSignStatusChangeHandler;
                _signStatusDependency = null;
            }
            if (_signActiveDependency != null)
            {
                _signActiveDependency.Stop();
                _signActiveDependency.TableChanged -= sql_OnSignActiveChangeHandler;
                _signActiveDependency = null;
            }
        }

        private void LoadClusterMembers()
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                {
                    sqlConnection.Open();

                    // Build a list of all cluster members
                    _clusterMembers = new List<ClusterMemberRecord>();
                    _clusterMemberSet = "";
                    SqlCommand command = new SqlCommand(String.Format("SELECT * FROM {0} WHERE {1} = {2} ORDER BY {3}",
                        /* 0 */ ClusterMemberRecord.TableName,
                        /* 1 */ ClusterMemberRecord.ColumnClusterId,
                        /* 2 */ _id,
                        /* 3 */ ClusterMemberRecord.ColumnSequence),
                        sqlConnection);
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        ClusterMemberRecord clusterMember = new ClusterMemberRecord(reader);
                        _clusterMembers.Add(clusterMember);
                        if (_clusterMemberSet.Length > 0) _clusterMemberSet += ',';
                        _clusterMemberSet += clusterMember.SignId.ToString();
                    }
                    reader.Close();
                }
            }
            catch (Exception e)
            {
                LogError(e.ToString());
            }
        }

        private void TryProcessNextAction()
        {
            // Cluster still active?
            if (_active)
            {
                // Not busy processing action?
                if (_actionProcessing.WaitOne(0))
                {
                    // Process next action in a thread from the pool
                    ThreadPool.QueueUserWorkItem(ProcessNextActionItem, this);
                }
            }
        }

        static private void ProcessNextActionItem(Object stateInfo)
        {
            // Get context from state info
            SnmpSignCluster context = (SnmpSignCluster)stateInfo;
            if (context != null)
            {
                // Call in context
                context.ProcessNextAction();
            }
        }

        private void ProcessNextAction()
        {
            LogInfo("ProcessNextAction " + _active);
            // Cluster still active?
            if (_active)
            {
                bool releaseAction = true;
                try
                {
                    using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                    {
                        sqlConnection.Open();

                        LogInfo("sqlConnect is open ClusterAction :" + DateTime.Now);
                        string sqlString = String.Format("SELECT * FROM {0} WHERE {1} = {2}"
                            + " AND {3} = '{4}' ORDER BY {5} ASC",
                            /* 0 */ ClusterActionRecord.TableName,
                            /* 1 */ ClusterActionRecord.ColumnClusterId,
                            /* 2 */ _id,
                            /* 3 */ ClusterActionRecord.ColumnStatus,
                            /* 4 */ ClusterActionRecord.StatusPending,
                            /* 5 */ ClusterActionRecord.ColumnStartTime);
                        SqlCommand command = new SqlCommand(sqlString, sqlConnection);
                        SqlDataReader reader = command.ExecuteReader();
                        if (reader.Read())
                        {
                            // Check if action is due to start
                            DateTime actionStart = IDataRecordConvert.ToDateTime(reader, ClusterActionRecord.ColumnStartTime);
                            double interval = (actionStart.Ticks - DateTime.Now.Ticks) / 10000;
                            if (interval <= 0)
                            {

                                // Process action 
                                ProcessAction(new ClusterActionRecord(reader));

                                // Action will be released after cluster members complete action
                                releaseAction = false;
                            }
                            else
                            {
                                // Signal that next action can be processed
                                _actionProcessing.Release();
                                releaseAction = false;

                                // Start a timer that will fire when next action is ready
                                timer_StartActionTimer(interval);
                            }
                        }

                        reader.Close();
                    }
                }
                catch (Exception e)
                {
                    LogError(e.ToString());
                }

                if (releaseAction)
                {
                    // Signal that next action can be processed
                    _actionProcessing.Release();
                }

                // Set up notification to wait for next action
                LogInfo("WaitForAction " + DateTime.Now);
                WaitForAction();
                LogInfo("WaitForAction Cleared");
            }
        }

        private int GetActionTimeout(ClusterActionRecord action)
        {
            int timeout = 0;
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                {
                    sqlConnection.Open();

                    string sqlString = String.Format("SELECT MAX({2}.{3}) FROM {0}, {1}, {2}"
                        + " WHERE {0}.{4} = {12} AND {1}.{5} = {0}.{6} AND {1}.{7} = 'True'"
                        + " AND {2}.{8} = '{13}' AND {2}.{9} = {1}.{10} AND {2}.{11} = '{14}'",
                        /* 0 */ ClusterMemberRecord.TableName,
                        /* 1 */ SignRecord.TableName,
                        /* 2 */ ConfigurationRecord.TableName,
                        /* 3 */ ConfigurationRecord.ColumnValue,
                        /* 4 */ ClusterMemberRecord.ColumnClusterId,
                        /* 5 */ SignRecord.ColumnId,
                        /* 6 */ ClusterMemberRecord.ColumnSignId,
                        /* 7 */ SignRecord.ColumnActive,
                        /* 8 */ ConfigurationRecord.ColumnType,
                        /* 9 */ ConfigurationRecord.ColumnSubType,
                        /* 10 */ SignRecord.ColumnType,
                        /* 11 */ ConfigurationRecord.ColumnName,
                        /* 12 */ _id,
                        /* 13 */ ConfigurationRecord.TypeSign,
                        /* 14 */ ConfigurationRecord.ValueSnmpTimeout);

                    // Get longest timeout delay for signs in this cluster
                    SqlCommand command = new SqlCommand(sqlString, sqlConnection);
                    System.Object result = command.ExecuteScalar();
                    if (result != null)
                    {
                        timeout = Convert.ToInt32(result);
                    }
                }
            }
            // Do nothing...
            catch { }

            // Use default?
            if (timeout == 0)
            {
                timeout = SnmpRequestManager.DefaultTimeout;
            }
            timeout += DefaultTimeoutMargin;

            // SET action?
            if (action.Action == ClusterActionRecord.ActionSet)
            {
                try
                {
                    using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                    {
                        sqlConnection.Open();

                        string sqlString = String.Format("SELECT MAX({3}.{4})"
                            + "FROM {0}, {1}, {2}, {3} "
                            + "WHERE {0}.{5} = {15}  AND {1}.{6} = {0}.{7} AND {2}.{8} = {1}.{9} "
                            + "AND {2}.{10} = 'True' "
                            + "AND ({3}.{11} = {0}.{12}.value('(/{13}/@{14})[1]', 'int') "
                            + "AND {3}.{11} != 0)",
                            /* 0 */ ClusterActionRecord.TableName,
                            /* 1 */ ClusterMemberRecord.TableName,
                            /* 2 */ SignRecord.TableName,
                            /* 3 */ SchemeMemberRecord.TableName,
                            /* 4 */ SchemeMemberRecord.ColumnSetDelay,
                            /* 5 */ ClusterActionRecord.ColumnId,
                            /* 6 */ ClusterMemberRecord.ColumnClusterId,
                            /* 7 */ ClusterActionRecord.ColumnClusterId,
                            /* 8 */ SignRecord.ColumnId,
                            /* 9 */ ClusterMemberRecord.ColumnSignId,
                            /* 10 */ SignRecord.ColumnActive,
                            /* 11 */ SchemeMemberRecord.ColumnSchemeId,
                            /* 12 */ ClusterActionRecord.ColumnInfo,
                            /* 13 */ ClusterActionRecord.ElementScheme,
                            /* 14 */ ClusterActionRecord.AttributeId,
                            /* 15 */ action.Id);

                        // Get longest offset delay for SET actions
                        SqlCommand command = new SqlCommand(sqlString, sqlConnection);
                        System.Object result = command.ExecuteScalar();
                        if (result != null)
                        {
                            timeout += Convert.ToInt32(result);
                        }
                    }
                }
                // Do nothing...
                catch { }
            }

            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                {
                    sqlConnection.Open();

                    string sqlString = String.Format("SELECT MAX({2}.{3}) FROM {0}, {1}, {2}"
                        + " WHERE {0}.{4} = {12} AND {1}.{5} = {0}.{6} AND {1}.{7} = 'True'"
                        + " AND {2}.{8} = '{13}' AND {2}.{9} = {1}.{10} AND {2}.{11} = 'DURATION_{14}'",
                        /* 0 */ ClusterMemberRecord.TableName,
                        /* 1 */ SignRecord.TableName,
                        /* 2 */ ConfigurationRecord.TableName,
                        /* 3 */ ConfigurationRecord.ColumnValue,
                        /* 4 */ ClusterMemberRecord.ColumnClusterId,
                        /* 5 */ SignRecord.ColumnId,
                        /* 6 */ ClusterMemberRecord.ColumnSignId,
                        /* 7 */ SignRecord.ColumnActive,
                        /* 8 */ ConfigurationRecord.ColumnType,
                        /* 9 */ ConfigurationRecord.ColumnSubType,
                        /* 10 */ SignRecord.ColumnType,
                        /* 11 */ ConfigurationRecord.ColumnName,
                        /* 12 */ _id,
                        /* 13 */ ConfigurationRecord.TypeSign,
                        /* 14 */ action.Action);

                    // Get action duration for signs in this cluster
                    SqlCommand command = new SqlCommand(sqlString, sqlConnection);
                    System.Object result = command.ExecuteScalar();
                    if (result != null)
                    {
                        timeout += Convert.ToInt32(result);
                    }
                }
            }
            // Do nothing...
            catch { }

            return timeout;
        }

        private void ProcessAction(ClusterActionRecord action)
        {
            LogInfo("Process Action" + action.Action);
            // Use action handler if there is one
            if (_actions.ContainsKey(action.Action)
                && _actions[action.Action] != null)
            {
                // Set action status to busy
                action.Status = ClusterActionRecord.StatusBusy;
                action.Update();

                // Log action 
                LogAction(action);
                LogInfo("StartTimer " + DateTime.Now);
                // Start a timer that will fire when the action times out
                timer_StartWatchdogTimer(GetActionTimeout(action) * 1000);

                // Set current action
                lock (_actionLock)
                {
                    _currentAction = action;
                }

                // Execute action handler
                _actions[action.Action](action);

                // Check if action is complete
                CheckActionComplete();
            }
            else
            {
                // Invalid action
                LogInfo("Invalid Action " +DateTime.Now);
                action.Status = ClusterActionRecord.StatusFailure;
                action.Update();
            }
        }

        private void CompleteAction(ClusterActionRecord action)
        {
            // Log action 
            LogAction(action);

            // Update action status
            action.Update();

            LogInfo(String.Format("{0} ({1}): {2}: {3}", action.Action,
                action.Id, action.Status, (action.Info != null) ? action.Info : ""));

            // Use action handler if there is one
            if (_actions.ContainsKey(action.Action)
                && _actions[action.Action] != null)
            {
                // Execute action handler
                _actions[action.Action](action);
            }
        }

        private void TimeoutAction(ClusterActionRecord action)
        {
            // Set its status to timed out
            action.Status = ClusterActionRecord.StatusTimeout;

            // Complete action
            CompleteAction(action);

            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                {
                    sqlConnection.Open();

                    // Time out all pending sign action records for the timed out cluster action
                    string sqlString = String.Format("UPDATE {0} SET {0}.{1} = '{2}', last_updated = CURRENT_TIMESTAMP " 
                        + "WHERE {0}.{3}.value('(/{4}/@{5})[1]', 'int') = {6} AND {0}.{1} = '{7}'",
                        /* 0 */ SignActionRecord.TableName,
                        /* 1 */ SignActionRecord.ColumnStatus,
                        /* 2 */ SignActionRecord.StatusTimeout,
                        /* 3 */ SignActionRecord.ColumnInfo,
                        /* 4 */ SignActionRecord.ElementClusterAction,
                        /* 5 */ SignActionRecord.AttributeId,
                        /* 6 */ action.Id,
                        /* 7 */ SignActionRecord.StatusPending);
                    SqlCommand sqlCommand = new SqlCommand(sqlString, sqlConnection);
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                LogError(e.ToString());
            }
        }

        private static void ProcessCheckActionCompleteItem(Object stateInfo)
        {
            // Get context from state info
            SnmpSignCluster context = (SnmpSignCluster)stateInfo;
            if (context != null)
            {
                // Call in context
                context.CheckActionComplete();
            }
        }

        private static void ProcessCheckSignStatusItem(Object stateInfo)
        {
            // Get context from state info
            SnmpSignCluster context = (SnmpSignCluster)stateInfo;
            if (context != null)
            {
                // Call in context
                context.CheckSignStatus();

                // Wait for the next sign status change
                context.WaitForSignStatus();
            }
        }

        private static void ProcessCheckSignActiveItem(Object stateInfo)
        {
            // Get context from state info
            SnmpSignCluster context = (SnmpSignCluster)stateInfo;
            if (context != null)
            {
                // Call in context
                context.CheckSignStatus();

                // Wait for the next sign availability change
                context.WaitForSignActive();
            }
        }

        private void WaitForAction()
        {
            // Cluster still active?
            if (_active)
            {
                try
                {
                    // Didn't start change notification yet?
                    if (_actionDependency == null)
                    {
                        // Start change notification for ClusterAction table
                        _actionDependency = new SqlDependencyEx(SnmpSignManager.SqlConnectionString, SnmpSignManager.DbName, ClusterActionRecord.TableName, 
                            SnmpSignManager.DbUser, SnmpSignManager.DbSchema, SqlDependencyEx.NotificationType.All,
                            false, SnmpSignManager.DbNextIdentity);

                        _actionDependency.TableChanged += sql_OnActionChangeHandler;
                        _actionDependency.Start();
                    }
                }
                catch (Exception e)
                {
                    LogError(e.ToString());
                }
            }
        }

        private void WaitForSignAction()
        {
            try
            {
                // Didn't start change notification yet?
                if (_signActionDependency == null)
                {
                    // Start change notification for SignAction table
                    _signActionDependency = new SqlDependencyEx(SnmpSignManager.SqlConnectionString, SnmpSignManager.DbName, SignActionRecord.TableName, 
                        SnmpSignManager.DbUser, SnmpSignManager.DbSchema, SqlDependencyEx.NotificationType.All,
                        false, SnmpSignManager.DbNextIdentity);

                    _signActionDependency.TableChanged += sql_OnSignActionChangeHandler;
                    _signActionDependency.Start();
                }
            }
            catch (Exception e)
            {
                LogError(e.ToString());
            }
        }

        private void WaitForSignStatus()
        {
            try
            {
                // Didn't start change notification yet?
                if (_signStatusDependency == null)
                {
                    // Start change notification for SignStatus table
                    _signStatusDependency = new SqlDependencyEx(SnmpSignManager.SqlConnectionString, SnmpSignManager.DbName, SignStatusRecord.TableName,
                        SnmpSignManager.DbUser, SnmpSignManager.DbSchema, SqlDependencyEx.NotificationType.All,
                        false, SnmpSignManager.DbNextIdentity);

                    _signStatusDependency.TableChanged += sql_OnSignStatusChangeHandler;
                    _signStatusDependency.Start();
                }
            }
            catch (Exception e)
            {
                LogError(e.ToString());
            }
        }

        private void WaitForSignActive()
        {
            try
            {
                // Didn't start change notification yet?
                if (_signActiveDependency == null)
                {
                    // Start change notification for Sign table
                    _signActiveDependency = new SqlDependencyEx(SnmpSignManager.SqlConnectionString, SnmpSignManager.DbName, SignRecord.TableName,
                        SnmpSignManager.DbUser, SnmpSignManager.DbSchema, SqlDependencyEx.NotificationType.All,
                        false, SnmpSignManager.DbNextIdentity);

                    _signActiveDependency.TableChanged += sql_OnSignActiveChangeHandler;
                    _signActiveDependency.Start();
                }
            }
            catch (Exception e)
            {
                LogError(e.ToString());
            }
        }

        private void ProcessSet(ClusterActionRecord action)
        {
            // Starting processing?
            if (action.Status == ClusterActionRecord.StatusBusy)
            {
                LogInfo(String.Format("{0} ({1}): {2}", action.Action, action.Id, action.Info));

                // Blank signs before setting?
                string blankAttribute = XmlHelper.StringFromXPath(
                    String.Format("/{0}/@{1}", ClusterActionRecord.ElementScheme, ClusterActionRecord.AttributeBlank), action.Info);
                bool blank = true;
                try
                {
                    blank = (blankAttribute == null || Convert.ToBoolean(blankAttribute));
                }
                // Do nothing...
                catch { }

                // Use offset delay when setting signs?
                string delayAttribute = XmlHelper.StringFromXPath(
                    String.Format("/{0}/@{1}", ClusterActionRecord.ElementScheme, ClusterActionRecord.AttributeDelay), action.Info);
                bool delay = true;
                try
                {
                    delay = (blankAttribute == null || Convert.ToBoolean(delayAttribute));
                }
                // Do nothing...
                catch { }

                // Valid scheme id supplied?
                if (CheckValidSchemeId(action))
                {
                    // Get scheme id
                    string schemeId = XmlHelper.StringFromXPath(
                        String.Format("/{0}/@{1}", ClusterActionRecord.ElementScheme, ClusterActionRecord.AttributeId), action.Info);
                    if (schemeId != null)
                    {
                        try
                        {
                            using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                            {
                                string sqlString;

                                sqlConnection.Open();

                                // Blank signs before setting?
                                if (blank && Convert.ToInt32(schemeId) != 0)
                                {
                                    // Insert a 'BLANK' sign action record for each member of cluster
                                    sqlString = String.Format("INSERT INTO {0}({4},{5},{6},{7},{8},{9}) "
                                        + "SELECT {1}.{10}, '{11}', '<{12} {13}=\"{14}\"/>', '{15}', DATEADD(ss, -1, @startTime), {16} "
                                        + "FROM {2}, {1}, {3} WHERE {2}.{17} = {18} AND {1}.{10} = {2}.{19} AND {1}.{20} = 'True' "
                                        + "AND {3}.{21} = {2}.{22} AND {3}.{23} = {24}",
                                        /* 0 */ SignActionRecord.TableName,
                                        /* 1 */ SignRecord.TableName,
                                        /* 2 */ ClusterMemberRecord.TableName,
                                        /* 3 */ SchemeMemberRecord.TableName,
                                        /* 4 */ SignActionRecord.ColumnSignId,
                                        /* 5 */ SignActionRecord.ColumnAction,
                                        /* 6 */ SignActionRecord.ColumnInfo,
                                        /* 7 */ SignActionRecord.ColumnStatus,
                                        /* 8 */ SignActionRecord.ColumnStartTime,
                                        /* 9 */ SignActionRecord.ColumnUserId,
                                        /* 10 */ SignRecord.ColumnId,
                                        /* 11 */ SignActionRecord.ActionBlank,
                                        /* 12 */ SignActionRecord.ElementClusterAction,
                                        /* 13 */ SignActionRecord.AttributeId,
                                        /* 14 */ action.Id,
                                        /* 15 */ SignActionRecord.StatusPending,
                                        /* 16 */ action.UserId != 0 ? action.UserId.ToString() : "NULL",
                                        /* 17 */ ClusterMemberRecord.ColumnClusterId,
                                        /* 18 */ action.ClusterId,
                                        /* 19 */ ClusterMemberRecord.ColumnSignId,
                                        /* 20 */ SignRecord.ColumnActive,
                                        /* 21 */ SchemeMemberRecord.ColumnSequence,
                                        /* 22 */ ClusterMemberRecord.ColumnSequence,
                                        /* 23 */ SchemeMemberRecord.ColumnSchemeId,
                                        /* 24 */ schemeId);

                                    SqlCommand sqlCommand = new SqlCommand(sqlString, sqlConnection);
                                    sqlCommand.Parameters.AddWithValue("@startTime", DateTime.Now);
                                    sqlCommand.ExecuteNonQuery();

                                    LogInfo ("Insert Blank - ProcessSet SnmpSignCLuster");
                                }

                                // Scheme 0 (BLANK)?
                                if (Convert.ToInt32(schemeId) == 0)
                                {
                                    // Insert a 'BLANK' sign action record for each member of cluster
                                    sqlString = String.Format("INSERT INTO {0}({3},{4},{5},{6},{7},{8}) "
                                        + "SELECT {1}.{9}, '{10}', '<{11} {12}=\"{13}\"/>', '{14}', @startTime, {15} "
                                        + "FROM {2}, {1} WHERE {2}.{16} = {17} AND {1}.{9} = {2}.{18} AND {1}.{19} = 'True'",
                                        /* 0 */ SignActionRecord.TableName,
                                        /* 1 */ SignRecord.TableName,
                                        /* 2 */ ClusterMemberRecord.TableName,
                                        /* 3 */ SignActionRecord.ColumnSignId,
                                        /* 4 */ SignActionRecord.ColumnAction,
                                        /* 5 */ SignActionRecord.ColumnInfo,
                                        /* 6 */ SignActionRecord.ColumnStatus,
                                        /* 7 */ SignActionRecord.ColumnStartTime,
                                        /* 8 */ SignActionRecord.ColumnUserId,
                                        /* 9 */ SignRecord.ColumnId,
                                        /* 10 */ SignActionRecord.ActionBlank,
                                        /* 11 */ SignActionRecord.ElementClusterAction,
                                        /* 12 */ SignActionRecord.AttributeId,
                                        /* 13 */ action.Id,
                                        /* 14 */ SignActionRecord.StatusPending,
                                        /* 15 */ action.UserId != 0 ? action.UserId.ToString() : "NULL",
                                        /* 16 */ ClusterMemberRecord.ColumnClusterId,
                                        /* 17 */ action.ClusterId,
                                        /* 18 */ ClusterMemberRecord.ColumnSignId,
                                        /* 19 */ SignRecord.ColumnActive);

                                    SqlCommand sqlCommand = new SqlCommand(sqlString, sqlConnection);
                                    sqlCommand.Parameters.AddWithValue("@startTime", DateTime.Now);
                                    sqlCommand.ExecuteNonQuery();

                                    LogInfo(" Blank - ProcessSet SnmpSignCLuster p758");
                                }
                                else
                                {
                                    // Insert a 'SET' sign action record for each member of cluster
                                    sqlString = String.Format("INSERT INTO {0}({4},{5},{6},{7},{8},{9}) "
                                        + "SELECT {1}.{10}, '{11}', '<{12} {13}=\"{14}\"/> "
                                        + "<{15}><{16}>' + {3}.{17} + '</{16}><{18}>' + CAST({2}.{19} AS VARCHAR) + {2}.{20} + '</{18}></{15}>', "
                                        + "'{21}', {22}, {23} "
                                        + "FROM {2}, {1}, {3} WHERE {2}.{24} = {25} AND {1}.{10} = {2}.{26} AND {1}.{27} = 'True' "
                                        + "AND {3}.{28} = {2}.{29} AND {3}.{30} = {31}",
                                        /* 0 */ SignActionRecord.TableName,
                                        /* 1 */ SignRecord.TableName,
                                        /* 2 */ ClusterMemberRecord.TableName,
                                        /* 3 */ SchemeMemberRecord.TableName,
                                        /* 4 */ SignActionRecord.ColumnSignId,
                                        /* 5 */ SignActionRecord.ColumnAction,
                                        /* 6 */ SignActionRecord.ColumnInfo,
                                        /* 7 */ SignActionRecord.ColumnStatus,
                                        /* 8 */ SignActionRecord.ColumnStartTime,
                                        /* 9 */ SignActionRecord.ColumnUserId,
                                        /* 10 */ SignRecord.ColumnId,
                                        /* 11 */ SignActionRecord.ActionSet,
                                        /* 12 */ SignActionRecord.ElementClusterAction,
                                        /* 13 */ SignActionRecord.AttributeId,
                                        /* 14 */ action.Id,
                                        /* 15 */ SignActionRecord.ElementMessage,
                                        /* 16 */ SignActionRecord.ElementMnemonic,
                                        /* 17 */ SchemeMemberRecord.ColumnMessage,
                                        /* 18 */ SignActionRecord.ElementDistance,
                                        /* 19 */ ClusterMemberRecord.ColumnDistance,
                                        /* 20 */ ClusterMemberRecord.ColumnUnits,
                                        /* 21 */ SignActionRecord.StatusPending,
                                        /* 22 */ delay ? String.Format("DATEADD(ss, {0}.{1}, @startTime)",
                                            SchemeMemberRecord.TableName, SchemeMemberRecord.ColumnSetDelay) : "@startTime",
                                        /* 23 */ action.UserId != 0 ? action.UserId.ToString() : "NULL",
                                        /* 24 */ ClusterMemberRecord.ColumnClusterId,
                                        /* 25 */ action.ClusterId,
                                        /* 26 */ ClusterMemberRecord.ColumnSignId,
                                        /* 27 */ SignRecord.ColumnActive,
                                        /* 28 */ SchemeMemberRecord.ColumnSequence,
                                        /* 29 */ ClusterMemberRecord.ColumnSequence,
                                        /* 30 */ SchemeMemberRecord.ColumnSchemeId,
                                        /* 31 */ schemeId);

                                    SqlCommand sqlCommand = new SqlCommand(sqlString, sqlConnection);
                                    sqlCommand.Parameters.AddWithValue("@startTime", DateTime.Now);
                                    sqlCommand.ExecuteNonQuery();

                                     LogInfo ("ProcessSet SnmpSignCLuster - p808 " + sqlString);
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            LogError(e.ToString());
                        }
                    }
                }
            }
            // Finished processing successfully?
            else if (action.Status == ClusterActionRecord.StatusSuccess)
            {
                try
                {
                    using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                    {
                        sqlConnection.Open();

                        // Insert a 'SET' cluster status record
                        string sqlString = String.Format("INSERT INTO {0}({1}, {2}, {3}) "
                            + "VALUES({4}, '{5}', '{6}')",
                            /* 0 */ ClusterStatusRecord.TableName,
                            /* 1 */ ClusterStatusRecord.ColumnClusterId,
                            /* 2 */ ClusterStatusRecord.ColumnStatus,
                            /* 3 */ ClusterStatusRecord.ColumnInfo,
                            /* 4 */ action.ClusterId,
                            /* 5 */ ClusterActionRecord.ActionSet,
                            /* 6 */ action.Info);
                        SqlCommand sqlCommand = new SqlCommand(sqlString, sqlConnection);
                        sqlCommand.ExecuteNonQuery();
                    }
                }
                catch (Exception e)
                {
                    LogError(e.ToString());
                }

                // Get scheme id
                string schemeId = XmlHelper.StringFromXPath(
                    String.Format("/{0}/@{1}", ClusterActionRecord.ElementScheme, ClusterActionRecord.AttributeId), action.Info);
                if (schemeId != null)
                {
                    try
                    {
                        using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                        {
                            sqlConnection.Open();

                            // Insert a 'SET' scheme status record
                            string sqlString = String.Format("INSERT INTO {0}({1}, {2}, {3}, {4}, {5}) "
                                + "VALUES({6}, {7}, '{8}', NULL, {9})",
                                /* 0 */ SchemeStatusRecord.TableName,
                                /* 1 */ SchemeStatusRecord.ColumnClusterId,
                                /* 2 */ SchemeStatusRecord.ColumnSchemeId,
                                /* 3 */ SchemeStatusRecord.ColumnStatus,
                                /* 4 */ SchemeStatusRecord.ColumnInfo,
                                /* 5 */ SchemeStatusRecord.ColumnUserId,
                                /* 6 */ action.ClusterId,
                                /* 7 */ schemeId,
                                /* 8 */ ClusterActionRecord.ActionSet,
                                /* 8 */ action.UserId != 0 ? action.UserId.ToString() : "NULL");
                            SqlCommand sqlCommand = new SqlCommand(sqlString, sqlConnection);
                            sqlCommand.ExecuteNonQuery();
                        }
                    }
                    catch (Exception e)
                    {
                        LogError(e.ToString());
                    }
                }
            }
        }

        private void ProcessFaultLedActionMode(ClusterActionRecord action)
        {
            if (action.Status == ClusterActionRecord.StatusBusy)
            {
                LogInfo(String.Format("{0} ({1}): {2}", action.Action, action.Id, action.Info));

                try
                {
                    using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                    {
                        sqlConnection.Open();

                        string sqlString = String.Format("INSERT INTO {0}({3},{4},{5},{6},{7},{8}) "
                            + "SELECT {1}.{9}, '{10}', '<{11} {12}=\"{13}\"/>{14}', '{15}', @startTime, {16} "
                            + "FROM {2}, {1} WHERE {2}.{17} = {18} AND {1}.{9} = {2}.{19} AND {1}.{20} = 'True'",
                            /* 0 */ SignActionRecord.TableName,
                            /* 1 */ SignRecord.TableName,
                            /* 2 */ ClusterMemberRecord.TableName,
                            /* 3 */ SignActionRecord.ColumnSignId,
                            /* 4 */ SignActionRecord.ColumnAction,
                            /* 5 */ SignActionRecord.ColumnInfo,
                            /* 6 */ SignActionRecord.ColumnStatus,
                            /* 7 */ SignActionRecord.ColumnStartTime,
                            /* 8 */ SignActionRecord.ColumnUserId,
                            /* 9 */ SignRecord.ColumnId,
                            /* 10 */ SignActionRecord.ActionLedFaultReportingMode,
                            /* 11 */ SignActionRecord.ElementClusterAction,
                            /* 12 */ SignActionRecord.AttributeId,
                            /* 13 */ action.Id,
                            /* 14 */ action.Info,
                            /* 15 */ SignActionRecord.StatusPending,
                            /* 16 */ action.UserId != 0 ? action.UserId.ToString() : "NULL",
                            /* 17 */ ClusterMemberRecord.ColumnClusterId,
                            /* 18 */ action.ClusterId,
                            /* 19 */ ClusterMemberRecord.ColumnSignId,
                            /* 20 */ SignRecord.ColumnActive);
                        SqlCommand sqlCommand = new SqlCommand(sqlString, sqlConnection);
                        sqlCommand.Parameters.AddWithValue("@startTime", DateTime.Now);
                        sqlCommand.ExecuteNonQuery();
                    }
                }
                catch (Exception e)
                {
                    LogError(e.ToString());
                }
            }
        }

        private void ProcessValidate(ClusterActionRecord action)
        {
            // Starting processing?
            if (action.Status == ClusterActionRecord.StatusBusy)
            {
                LogInfo(String.Format("{0} ({1}): {2}", action.Action, action.Id, action.Info));

                // Valid scheme id supplied?
                if (CheckValidSchemeId(action))
                {
                    // Get scheme id
                    string schemeId = XmlHelper.StringFromXPath(
                        String.Format("/{0}/@{1}", ClusterActionRecord.ElementScheme, ClusterActionRecord.AttributeId), action.Info);
                    if (schemeId != null)
                    {
                        try
                        {
                            using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                            {
                                string sqlString;

                                sqlConnection.Open();
                                
                                // Scheme 0 (BLANK)?
                                if (Convert.ToInt32(schemeId) == 0)
                                {
                                    // Insert a 'VALIDATE' sign action record for each member of cluster
                                    sqlString = String.Format("INSERT INTO {0}({3},{4},{5},{6},{7},{8}) "
                                        + "SELECT {1}.{9}, '{10}', '<{11} {12}=\"{13}\"/><{14}><{15}>1</{15}><{16}>1</{16}></{14}>', '{17}', @startTime, {18} "
                                        + "FROM {2}, {1} WHERE {2}.{19} = {20} AND {1}.{9} = {2}.{21} AND {1}.{22} = 'True'",
                                        /* 0 */ SignActionRecord.TableName,
                                        /* 1 */ SignRecord.TableName,
                                        /* 2 */ ClusterMemberRecord.TableName,
                                        /* 3 */ SignActionRecord.ColumnSignId,
                                        /* 4 */ SignActionRecord.ColumnAction,
                                        /* 5 */ SignActionRecord.ColumnInfo,
                                        /* 6 */ SignActionRecord.ColumnStatus,
                                        /* 7 */ SignActionRecord.ColumnStartTime,
                                        /* 8 */ SignActionRecord.ColumnUserId,
                                        /* 9 */ SignRecord.ColumnId,
                                        /* 10 */ SignActionRecord.ActionValidate,
                                        /* 11 */ SignActionRecord.ElementClusterAction,
                                        /* 12 */ SignActionRecord.AttributeId,
                                        /* 13 */ action.Id,
                                        /* 14 */ SignActionRecord.ElementMessage,
                                        /* 15 */ SignActionRecord.ElementMnemonic,
                                        /* 16 */ SignActionRecord.ElementDistance,
                                        /* 17 */ SignActionRecord.StatusPending,
                                        /* 18 */ action.UserId != 0 ? action.UserId.ToString() : "NULL",
                                        /* 19 */ ClusterMemberRecord.ColumnClusterId,
                                        /* 20 */ action.ClusterId,
                                        /* 21 */ ClusterMemberRecord.ColumnSignId,
                                        /* 22 */ SignRecord.ColumnActive);
                                }
                                else
                                {
                                    // Insert a 'VALIDATE' sign action record for each member of cluster
                                    sqlString = String.Format("INSERT INTO {0}({4},{5},{6},{7},{8},{9}) "
                                        + "SELECT {1}.{10}, '{11}', '<{12} {13}=\"{14}\"/> "
                                        + "<{15}><{16}>' + {3}.{17} + '</{16}><{18}>' + CAST({2}.{19} AS VARCHAR) + {2}.{20} + '</{18}></{15}>', "
                                        + "'{21}', @startTime, {22} "
                                        + "FROM {2}, {1}, {3} WHERE {2}.{23} = {24} AND {1}.{10} = {2}.{25} AND {1}.{26} = 'True' "
                                        + "AND {3}.{27} = {2}.{28} AND {3}.{29} = {30}",
                                        /* 0 */ SignActionRecord.TableName,
                                        /* 1 */ SignRecord.TableName,
                                        /* 2 */ ClusterMemberRecord.TableName,
                                        /* 3 */ SchemeMemberRecord.TableName,
                                        /* 4 */ SignActionRecord.ColumnSignId,
                                        /* 5 */ SignActionRecord.ColumnAction,
                                        /* 6 */ SignActionRecord.ColumnInfo,
                                        /* 7 */ SignActionRecord.ColumnStatus,
                                        /* 8 */ SignActionRecord.ColumnStartTime,
                                        /* 9 */ SignActionRecord.ColumnUserId,
                                        /* 10 */ SignRecord.ColumnId,
                                        /* 11 */ SignActionRecord.ActionValidate,
                                        /* 12 */ SignActionRecord.ElementClusterAction,
                                        /* 13 */ SignActionRecord.AttributeId,
                                        /* 14 */ action.Id,
                                        /* 15 */ SignActionRecord.ElementMessage,
                                        /* 16 */ SignActionRecord.ElementMnemonic,
                                        /* 17 */ SchemeMemberRecord.ColumnMessage,
                                        /* 18 */ SignActionRecord.ElementDistance,
                                        /* 19 */ ClusterMemberRecord.ColumnDistance,
                                        /* 20 */ ClusterMemberRecord.ColumnUnits,
                                        /* 21 */ SignActionRecord.StatusPending,
                                        /* 22 */ action.UserId != 0 ? action.UserId.ToString() : "NULL",
                                        /* 23 */ ClusterMemberRecord.ColumnClusterId,
                                        /* 24 */ action.ClusterId,
                                        /* 25 */ ClusterMemberRecord.ColumnSignId,
                                        /* 26 */ SignRecord.ColumnActive,
                                        /* 27 */ SchemeMemberRecord.ColumnSequence,
                                        /* 28 */ ClusterMemberRecord.ColumnSequence,
                                        /* 29 */ SchemeMemberRecord.ColumnSchemeId,
                                        /* 30 */ schemeId);
                                }


                                LogInfo(sqlString);

                                SqlCommand sqlCommand = new SqlCommand(sqlString, sqlConnection);
                                sqlCommand.Parameters.AddWithValue("@startTime", DateTime.Now);
                                sqlCommand.ExecuteNonQuery();
                            }
                        }
                        catch (Exception e)
                        {
                            LogError(e.ToString());
                        }
                    }
                }
            }
        }

        private void ProcessBlank(ClusterActionRecord action)
        {
            // Starting processing?
            if (action.Status == ClusterActionRecord.StatusBusy)
            {
                LogInfo(String.Format("{0} ({1})", action.Action, action.Id));

                try
                {
                    using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                    {
                        sqlConnection.Open();

                        // Insert a 'BLANK' sign action record for each member of cluster
                        string sqlString = String.Format("INSERT INTO {0}({3},{4},{5},{6},{7},{8}) "
                            + "SELECT {1}.{9}, '{10}', '<{11} {12}=\"{13}\"/>', '{14}', @startTime, {15} "
                            + "FROM {2}, {1} WHERE {2}.{16} = {17} AND {1}.{9} = {2}.{18} AND {1}.{19} = 'True'",
                            /* 0 */ SignActionRecord.TableName,
                            /* 1 */ SignRecord.TableName,
                            /* 2 */ ClusterMemberRecord.TableName,
                            /* 3 */ SignActionRecord.ColumnSignId,
                            /* 4 */ SignActionRecord.ColumnAction,
                            /* 5 */ SignActionRecord.ColumnInfo,
                            /* 6 */ SignActionRecord.ColumnStatus,
                            /* 7 */ SignActionRecord.ColumnStartTime,
                            /* 8 */ SignActionRecord.ColumnUserId,
                            /* 9 */ SignRecord.ColumnId,
                            /* 10 */ SignActionRecord.ActionBlank,
                            /* 11 */ SignActionRecord.ElementClusterAction,
                            /* 12 */ SignActionRecord.AttributeId,
                            /* 13 */ action.Id,
                            /* 14 */ SignActionRecord.StatusPending,
                            /* 15 */ action.UserId != 0 ? action.UserId.ToString() : "NULL",
                            /* 16 */ ClusterMemberRecord.ColumnClusterId,
                            /* 17 */ action.ClusterId,
                            /* 18 */ ClusterMemberRecord.ColumnSignId,
                            /* 19 */ SignRecord.ColumnActive);
                        SqlCommand sqlCommand = new SqlCommand(sqlString, sqlConnection);
                        sqlCommand.Parameters.AddWithValue("@startTime", DateTime.Now);
                        sqlCommand.ExecuteNonQuery();
                    }
                }
                catch (Exception e)
                {
                    LogError(e.ToString());
                }
            }
            // Finished processing successfully?
            else if (action.Status == ClusterActionRecord.StatusSuccess)
            {
                try
                {
                    using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                    {
                        sqlConnection.Open();

                        // Insert a 'SET' cluster status record
                        string sqlString = String.Format("INSERT INTO {0}({1}, {2}, {3}) "
                            + "VALUES({4}, '{5}', '<{6} {7}=\"0\"/>')",
                            /* 0 */ ClusterStatusRecord.TableName,
                            /* 1 */ ClusterStatusRecord.ColumnClusterId,
                            /* 2 */ ClusterStatusRecord.ColumnStatus,
                            /* 3 */ ClusterStatusRecord.ColumnInfo,
                            /* 4 */ action.ClusterId,
                            /* 5 */ ClusterActionRecord.ActionSet,
                            /* 6 */ ClusterActionRecord.ElementScheme,
                            /* 7 */ ClusterActionRecord.AttributeId);
                        SqlCommand sqlCommand = new SqlCommand(sqlString, sqlConnection);
                        sqlCommand.ExecuteNonQuery();
                    }
                }
                catch (Exception e)
                {
                    LogError(e.ToString());
                }

                try
                {
                    using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                    {
                        sqlConnection.Open();

                        // Insert a 'SET' scheme status record
                        string sqlString = String.Format("INSERT INTO {0}({1}, {2}, {3}, {4}, {5}) "
                            + "VALUES({6}, 0, '{7}', NULL, {8})",
                            /* 0 */ SchemeStatusRecord.TableName,
                            /* 1 */ SchemeStatusRecord.ColumnClusterId,
                            /* 2 */ SchemeStatusRecord.ColumnSchemeId,
                            /* 3 */ SchemeStatusRecord.ColumnStatus,
                            /* 4 */ SchemeStatusRecord.ColumnInfo,
                            /* 5 */ SchemeStatusRecord.ColumnUserId,
                            /* 6 */ action.ClusterId,
                            /* 7 */ ClusterActionRecord.ActionSet,
                            /* 8 */ action.UserId != 0 ? action.UserId.ToString() : "NULL");
                        SqlCommand sqlCommand = new SqlCommand(sqlString, sqlConnection);
                        sqlCommand.ExecuteNonQuery();
                    }
                }
                catch (Exception e)
                {
                    LogError(e.ToString());
                }
            }
        }

        private void ProcessPoll(ClusterActionRecord action)
        {
            // Starting processing?
            if (action.Status == ClusterActionRecord.StatusBusy)
            {
                LogInfo(String.Format("{0} ({1})", action.Action, action.Id));

                try
                {
                    using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                    {
                        sqlConnection.Open();

                        // Insert a 'POLL' sign action record for each member of cluster
                        string sqlString = String.Format("INSERT INTO {0}({3},{4},{5},{6},{7},{8}) "
                            + "SELECT {1}.{9}, '{10}', '<{11} {12}=\"{13}\"/>', '{14}', @startTime, {15} "
                            + "FROM {2}, {1} WHERE {2}.{16} = {17} AND {1}.{9} = {2}.{18} AND {1}.{19} = 'True'",
                            /* 0 */ SignActionRecord.TableName,
                            /* 1 */ SignRecord.TableName,
                            /* 2 */ ClusterMemberRecord.TableName,
                            /* 3 */ SignActionRecord.ColumnSignId,
                            /* 4 */ SignActionRecord.ColumnAction,
                            /* 5 */ SignActionRecord.ColumnInfo,
                            /* 6 */ SignActionRecord.ColumnStatus,
                            /* 7 */ SignActionRecord.ColumnStartTime,
                            /* 8 */ SignActionRecord.ColumnUserId,
                            /* 9 */ SignRecord.ColumnId,
                            /* 10 */ SignActionRecord.ActionPoll,
                            /* 11 */ SignActionRecord.ElementClusterAction,
                            /* 12 */ SignActionRecord.AttributeId,
                            /* 13 */ action.Id,
                            /* 14 */ SignActionRecord.StatusPending,
                            /* 15 */ action.UserId != 0 ? action.UserId.ToString() : "NULL",
                            /* 16 */ ClusterMemberRecord.ColumnClusterId,
                            /* 17 */ action.ClusterId,
                            /* 18 */ ClusterMemberRecord.ColumnSignId,
                            /* 19 */ SignRecord.ColumnActive);
                        SqlCommand sqlCommand = new SqlCommand(sqlString, sqlConnection);
                        sqlCommand.Parameters.AddWithValue("@startTime", DateTime.Now);
                        sqlCommand.ExecuteNonQuery();
                    }
                }
                catch (Exception e)
                {
                    LogError(e.ToString());
                }
            }
        }

        private void ProcessSetDimBrightLuminance(ClusterActionRecord action)
        {
            if (action.Status == ClusterActionRecord.StatusBusy)
            {
                LogInfo(String.Format("{0} ({1}): {2}", action.Action, action.Id, action.Info));

                try
                {
                    using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                    {
                        sqlConnection.Open();

                        // Insert a 'SETLUMINANCE' sign action record for each member of cluster
                        string sqlString = String.Format("INSERT INTO {0}({3},{4},{5},{6},{7},{8}) "
                            + "SELECT {1}.{9}, '{10}', '<{11} {12}=\"{13}\"/>{14}', '{15}', @startTime, {16} "
                            + "FROM {2}, {1} WHERE {2}.{17} = {18} AND {1}.{9} = {2}.{19} AND {1}.{20} = 'True'",
                            /* 0 */ SignActionRecord.TableName,
                            /* 1 */ SignRecord.TableName,
                            /* 2 */ ClusterMemberRecord.TableName,
                            /* 3 */ SignActionRecord.ColumnSignId,
                            /* 4 */ SignActionRecord.ColumnAction,
                            /* 5 */ SignActionRecord.ColumnInfo,
                            /* 6 */ SignActionRecord.ColumnStatus,
                            /* 7 */ SignActionRecord.ColumnStartTime,
                            /* 8 */ SignActionRecord.ColumnUserId,
                            /* 9 */ SignRecord.ColumnId,
                            /* 10 */ SignActionRecord.ActionDimBrightLuminance,
                            /* 11 */ SignActionRecord.ElementClusterAction,
                            /* 12 */ SignActionRecord.AttributeId,
                            /* 13 */ action.Id,
                            /* 14 */ action.Info,
                            /* 15 */ SignActionRecord.StatusPending,
                            /* 16 */ action.UserId != 0 ? action.UserId.ToString() : "NULL",
                            /* 17 */ ClusterMemberRecord.ColumnClusterId,
                            /* 18 */ action.ClusterId,
                            /* 19 */ ClusterMemberRecord.ColumnSignId,
                            /* 20 */ SignRecord.ColumnActive);
                        SqlCommand sqlCommand = new SqlCommand(sqlString, sqlConnection);
                        sqlCommand.Parameters.AddWithValue("@startTime", DateTime.Now);
                        sqlCommand.ExecuteNonQuery();
                    }
                }
                catch (Exception e)
                {
                    LogError(e.ToString());
                }
            }
        }

        private void ProcessSetLuminance(ClusterActionRecord action)
        {
            // Starting processing?
            if (action.Status == ClusterActionRecord.StatusBusy)
            {
                LogInfo(String.Format("{0} ({1}): {2}", action.Action, action.Id, action.Info));

                try
                {
                    using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                    {
                        sqlConnection.Open();

                        // Insert a 'SETLUMINANCE' sign action record for each member of cluster
                        string sqlString = String.Format("INSERT INTO {0}({3},{4},{5},{6},{7},{8}) "
                            + "SELECT {1}.{9}, '{10}', '<{11} {12}=\"{13}\"/>{14}', '{15}', @startTime, {16} "
                            + "FROM {2}, {1} WHERE {2}.{17} = {18} AND {1}.{9} = {2}.{19} AND {1}.{20} = 'True'",
                            /* 0 */ SignActionRecord.TableName,
                            /* 1 */ SignRecord.TableName,
                            /* 2 */ ClusterMemberRecord.TableName,
                            /* 3 */ SignActionRecord.ColumnSignId,
                            /* 4 */ SignActionRecord.ColumnAction,
                            /* 5 */ SignActionRecord.ColumnInfo,
                            /* 6 */ SignActionRecord.ColumnStatus,
                            /* 7 */ SignActionRecord.ColumnStartTime,
                            /* 8 */ SignActionRecord.ColumnUserId,
                            /* 9 */ SignRecord.ColumnId,
                            /* 10 */ SignActionRecord.ActionSetLuminance,
                            /* 11 */ SignActionRecord.ElementClusterAction,
                            /* 12 */ SignActionRecord.AttributeId,
                            /* 13 */ action.Id,
                            /* 14 */ action.Info,
                            /* 15 */ SignActionRecord.StatusPending,
                            /* 16 */ action.UserId != 0 ? action.UserId.ToString() : "NULL",
                            /* 17 */ ClusterMemberRecord.ColumnClusterId,
                            /* 18 */ action.ClusterId,
                            /* 19 */ ClusterMemberRecord.ColumnSignId,
                            /* 20 */ SignRecord.ColumnActive);
                        SqlCommand sqlCommand = new SqlCommand(sqlString, sqlConnection);
                        sqlCommand.Parameters.AddWithValue("@startTime", DateTime.Now);
                        sqlCommand.ExecuteNonQuery();
                    }
                }
                catch (Exception e)
                {
                    LogError(e.ToString());
                }
            }
        }

        private void ProcessSetLuminanceOverride(ClusterActionRecord action)
        {
            // Starting processing?
            if (action.Status == ClusterActionRecord.StatusBusy)
            {
                LogInfo(String.Format("{0} ({1})", action.Action, action.Id));

                try
                {
                    using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                    {
                        sqlConnection.Open();

                        // Insert a 'CLEARLUMINANCE' sign action record for each member of cluster
                        string sqlString = String.Format("INSERT INTO {0}({3},{4},{5},{6},{7},{8}) "
                            + "SELECT {1}.{9}, '{10}', '<{11} {12}=\"{13}\"/>', '{14}', @startTime, {15} "
                            + "FROM {2}, {1} WHERE {2}.{16} = {17} AND {1}.{9} = {2}.{18} AND {1}.{19} = 'True'",
                            /* 0 */ SignActionRecord.TableName,
                            /* 1 */ SignRecord.TableName,
                            /* 2 */ ClusterMemberRecord.TableName,
                            /* 3 */ SignActionRecord.ColumnSignId,
                            /* 4 */ SignActionRecord.ColumnAction,
                            /* 5 */ SignActionRecord.ColumnInfo,
                            /* 6 */ SignActionRecord.ColumnStatus,
                            /* 7 */ SignActionRecord.ColumnStartTime,
                            /* 8 */ SignActionRecord.ColumnUserId,
                            /* 9 */ SignRecord.ColumnId,
                            /* 10 */ SignActionRecord.ActionSetLuminaceToOverride,
                            /* 11 */ SignActionRecord.ElementClusterAction,
                            /* 12 */ SignActionRecord.AttributeId,
                            /* 13 */ action.Id,
                            /* 14 */ SignActionRecord.StatusPending,
                            /* 15 */ action.UserId != 0 ? action.UserId.ToString() : "NULL",
                            /* 16 */ ClusterMemberRecord.ColumnClusterId,
                            /* 17 */ action.ClusterId,
                            /* 18 */ ClusterMemberRecord.ColumnSignId,
                            /* 19 */ SignRecord.ColumnActive);
                        SqlCommand sqlCommand = new SqlCommand(sqlString, sqlConnection);
                        sqlCommand.Parameters.AddWithValue("@startTime", DateTime.Now);
                        sqlCommand.ExecuteNonQuery();
                    }
                }
                catch (Exception e)
                {
                    LogError(e.ToString());
                }
            }
        }


        private void ProcessClearLuminance(ClusterActionRecord action)
        {
            // Starting processing?
            if (action.Status == ClusterActionRecord.StatusBusy)
            {
                LogInfo(String.Format("{0} ({1})", action.Action, action.Id));

                try
                {
                    using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                    {
                        sqlConnection.Open();

                        // Insert a 'CLEARLUMINANCE' sign action record for each member of cluster
                        string sqlString = String.Format("INSERT INTO {0}({3},{4},{5},{6},{7},{8}) "
                            + "SELECT {1}.{9}, '{10}', '<{11} {12}=\"{13}\"/>', '{14}', @startTime, {15} "
                            + "FROM {2}, {1} WHERE {2}.{16} = {17} AND {1}.{9} = {2}.{18} AND {1}.{19} = 'True'",
                            /* 0 */ SignActionRecord.TableName,
                            /* 1 */ SignRecord.TableName,
                            /* 2 */ ClusterMemberRecord.TableName,
                            /* 3 */ SignActionRecord.ColumnSignId,
                            /* 4 */ SignActionRecord.ColumnAction,
                            /* 5 */ SignActionRecord.ColumnInfo,
                            /* 6 */ SignActionRecord.ColumnStatus,
                            /* 7 */ SignActionRecord.ColumnStartTime,
                            /* 8 */ SignActionRecord.ColumnUserId,
                            /* 9 */ SignRecord.ColumnId,
                            /* 10 */ SignActionRecord.ActionClearLuminance,
                            /* 11 */ SignActionRecord.ElementClusterAction,
                            /* 12 */ SignActionRecord.AttributeId,
                            /* 13 */ action.Id,
                            /* 14 */ SignActionRecord.StatusPending,
                            /* 15 */ action.UserId != 0 ? action.UserId.ToString() : "NULL",
                            /* 16 */ ClusterMemberRecord.ColumnClusterId,
                            /* 17 */ action.ClusterId,
                            /* 18 */ ClusterMemberRecord.ColumnSignId,
                            /* 19 */ SignRecord.ColumnActive);
                        SqlCommand sqlCommand = new SqlCommand(sqlString, sqlConnection);
                        sqlCommand.Parameters.AddWithValue("@startTime", DateTime.Now);
                        sqlCommand.ExecuteNonQuery();
                    }
                }
                catch (Exception e)
                {
                    LogError(e.ToString());
                }
            }
        }

        private bool CheckValidSchemeId(ClusterActionRecord action)
        {
            bool valid = true;

            // Get scheme id
            string schemeId = XmlHelper.StringFromXPath(
                String.Format("/{0}/@{1}", ClusterActionRecord.ElementScheme, ClusterActionRecord.AttributeId), action.Info);
            if (schemeId != null)
            {
                try
                {
                    using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                    {
                        sqlConnection.Open();

                        string sqlString = String.Format("SELECT 1 FROM {0} WHERE {0}.{1} = {2}",
                            /* 0 */ SchemeRecord.TableName,
                            /* 1 */ SchemeRecord.ColumnId,
                            /* 2 */ schemeId);
                        SqlCommand sqlCommand = new SqlCommand(sqlString, sqlConnection);
                        valid = (sqlCommand.ExecuteScalar() != null);
                    }
                }
                catch (Exception e)
                {
                    LogError(e.ToString());
                }
            }
            else valid = false;

            // Not a valid scheme id?
            if (!valid)
            {
                // Fail action
                action.Status = ClusterActionRecord.StatusFailure;
                action.Update();
            }

            return valid;
        }

        private bool CheckActionComplete()
        {
            lock (_actionLock)
            {
                if (_currentAction != null)
                {
                    try
                    {
                        using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                        {
                            sqlConnection.Open();

                            // Build SQL command to find all sign action records for the current cluster action
                            string sqlString = String.Format("SELECT {2} FROM {0}, {1} WHERE {3} IN ({4})"
                            + " AND {1}.{5} = {3} AND {1}.{6} = 'True'"
                            + " AND {0}.{7}.value('(/{8}/@{9})[1]', 'int') = {10}",
                                /* 0 */ SignActionRecord.TableName,
                                /* 1 */ SignRecord.TableName,
                                /* 2 */ SignActionRecord.ColumnStatus,
                                /* 3 */ SignActionRecord.ColumnSignId,
                                /* 4 */ _clusterMemberSet,
                                /* 5 */ SignRecord.ColumnId,
                                /* 6 */ SignRecord.ColumnActive,
                                /* 7 */ SignActionRecord.ColumnInfo,
                                /* 8 */ SignActionRecord.ElementClusterAction,
                                /* 9 */ SignActionRecord.AttributeId,
                                /* 10 */ _currentAction.Id);
                            SqlCommand command = new SqlCommand(sqlString, sqlConnection);

                            // Check status of all sign action records
                            SqlDataReader reader = command.ExecuteReader();
                            bool complete = true, error = false, valid = false;
                            while (reader.Read())
                            {
                                // Flag that at least one sign action record was created
                                valid = true;

                                // Not complete?
                                string status = reader[0].ToString();
                                if (status == SignActionRecord.StatusBusy
                                    || status == SignActionRecord.StatusPending)
                                {
                                    complete = false;
                                }
                                // Failed?
                                else if (status == SignActionRecord.StatusFailure
                                    || status == SignActionRecord.StatusCancelled
                                    || status == SignActionRecord.StatusTimeout)
                                {
                                    error = true;
                                }
                            }
                            reader.Close();

                            // Invalid?
                            if (!valid) complete = error = true;

                            LogInfo(String.Format("{0} ({1}): COMPLETE:{2} ERROR:{3}", _currentAction.Action,
                                _currentAction.Id, complete, error));

                            // Action is complete?
                            if (complete)
                            {
                                // Stop time out timer
                                timer_StopWatchdogTimer();

                                // Update action status
                                if (!error) _currentAction.Status = ClusterActionRecord.StatusSuccess;
                                else _currentAction.Status = ClusterActionRecord.StatusFailure;

                                // Complete action
                                CompleteAction(_currentAction);

                                // Signal that next action can be processed
                                _currentAction = null;
                                _actionProcessing.WaitOne(0);
                                _actionProcessing.Release();

                                // Try to process next action
                                TryProcessNextAction();

                                return true;
                            }
                            else
                            {
                                // Wait for the next sign status change
                                WaitForSignAction();
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        LogError(e.ToString());
                    }
                }
            }

            return false;
        }

        private void CheckSignStatus()
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                {
                    sqlConnection.Open();

                    // Process each sign in cluster
                    bool fault = false;
                    foreach (ClusterMemberRecord member in _clusterMembers)
                    {
                        try
                        {
                            // Is sign in fault?
                            string sqlString = String.Format("EXEC {0} {1}", ProcedureSignFault, member.SignId);
                            SqlCommand command = new SqlCommand(sqlString, sqlConnection);
                            System.Object result = command.ExecuteScalar();
                            if (result != null && Convert.ToInt32(result) != 0)
                            {
                                fault = true;
                                break;
                            }
                        }
                        catch (Exception e)
                        {
                            LogError(e.ToString());
                        }
                    }

                    // Get fault status template
                    string template = GetTemplate(ClusterTemplateRecord.TypeFormat, ClusterStatusRecord.StatusFault);
                    if (template != null)
                    {
                        // Set fault status
                        template = String.Format(template, fault ? 1 : 0);
                        SetStatus(ClusterStatusRecord.StatusFault, template);
                    }
                }
            }
            catch (Exception e)
            {
                LogError(e.ToString());
            }
        }

        private void sql_OnActionChangeHandler(object sender, SqlDependencyEx.TableChangedEventArgs e)
        {
            // Try to process next action
            TryProcessNextAction();
        }

        private void sql_OnSignActionChangeHandler(object sender, SqlDependencyEx.TableChangedEventArgs e)
        {
            // Check if action is complete in a thread from the pool
            ThreadPool.QueueUserWorkItem(ProcessCheckActionCompleteItem, this);
        }

        private void sql_OnSignStatusChangeHandler(object sender, SqlDependencyEx.TableChangedEventArgs e)
        {
            // Check cluster member status in a thread from the pool
            ThreadPool.QueueUserWorkItem(ProcessCheckSignStatusItem, this);
        }

        private void sql_OnSignActiveChangeHandler(object sender, SqlDependencyEx.TableChangedEventArgs e)
        {
            // Check cluster member availability in a thread from the pool
            ThreadPool.QueueUserWorkItem(ProcessCheckSignActiveItem, this);
        }

        private void timer_StartActionTimer(double interval)
        {
            _actionTimer = new System.Timers.Timer();
            _actionTimer.Elapsed += _actionTimerHandler;
            _actionTimer.Interval = interval;
            _actionTimer.Enabled = true;
            _actionTimer.AutoReset = false;
        }

        private void timer_StopActionTimer()
        {
            if (_actionTimer != null)
            {
                _actionTimer.Elapsed -= _actionTimerHandler;
                _actionTimer.Stop();
                _actionTimer = null;
            }
        }

        private void timer_StartWatchdogTimer(double interval)
        {
            _watchdogTimer = new System.Timers.Timer();
            _watchdogTimer.Elapsed += _watchdogTimerHandler;
            _watchdogTimer.Interval = interval;
            _watchdogTimer.Enabled = true;
            _watchdogTimer.AutoReset = false;
        }

        private void timer_StopWatchdogTimer()
        {
            if (_watchdogTimer != null)
            {
                _watchdogTimer.Elapsed -= _watchdogTimerHandler;
                _watchdogTimer.Stop();
                _watchdogTimer = null;
            }
        }

        private void timer_OnElapsedEventHandler(object source, ElapsedEventArgs e)
        {
            // Next action is due?
            if (source == _actionTimer)
            {
                // Release timer
                _actionTimer = null;

                // Try to process next action
                TryProcessNextAction();
            }
            // Action has timed out?
            else if (source == _watchdogTimer)
            {
                // Release timer
                _watchdogTimer = null;

                // Action is incomplete?
                if (!CheckActionComplete())
                {
                    lock (_actionLock)
                    {
                        // Have an action in progress?
                        if (_currentAction != null)
                        {
                            // Time out action
                            TimeoutAction(_currentAction);

                            // Signal that next action can be processed
                            _currentAction = null;
                            _actionProcessing.WaitOne(0);
                            _actionProcessing.Release();

                            // Try to process next action
                            TryProcessNextAction();
                        }
                    }
                }
            }
        }

        private bool UpdateStatus(string type, string info)
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                {
                    sqlConnection.Open();

                    // Insert status record
                    string sqlString = String.Format("INSERT INTO {0}({1}, {2}, {3})"
                        + " VALUES ('{4}', '{5}', '{6}')",
                        /* 0 */ ClusterStatusRecord.TableName,
                        /* 1 */ ClusterStatusRecord.ColumnClusterId,
                        /* 2 */ ClusterStatusRecord.ColumnStatus,
                        /* 3 */ ClusterStatusRecord.ColumnInfo,
                        /* 4 */ _id,
                        /* 5 */ type,
                        /* 6 */ info);
                    SqlCommand sqlCommand = new SqlCommand(sqlString, sqlConnection);
                    sqlCommand.ExecuteNonQuery();

                    LogInfo(String.Format("{0}: {1}", type, info));

                    return true;
                }
            }
            catch (Exception e)
            {
                LogError(e.ToString());
            }

            return false;
        }

        private void SetStatus(string type, string info)
        {
            // No existing status value?
            string status;
            if (!_statuses.TryGetValue(type, out status))
            {
                // Log status change
                LogStatus(type, info);

                // Update status
                if (UpdateStatus(type, info))
                {
                    // Only add it when DB update succeeds so that we always match DB
                    _statuses.Add(type, info);
                }
            }
            // Status value changed (basic check)?
            else if (status != info)
            {
                // Status value changed?
                string newStatus = XmlHelper.Merge(info, status);
                if (newStatus != status)
                {
                    // Log status change
                    LogStatus(type, newStatus);

                    // Update status
                    if (UpdateStatus(type, newStatus))
                    {
                        // Only set it when DB update succeeds so that we always match DB
                        _statuses[type] = newStatus;
                    }
                }
            }
        }

        // Logging
        private void LogStatus(string type, string info)
        {
            // Get status log template
            string template = GetTemplate(ClusterTemplateRecord.TypeLogStatus, type);
            if (template != null)
            {
                // Build log message
                string logMessage = XmlHelper.StringFromXPathTemplate(template, info);
                if (logMessage != null)
                {
                    try
                    {
                        using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                        {
                            sqlConnection.Open();

                            // Insert status log record
                            string sqlString = String.Format("INSERT INTO {0}({1}, {2}, {3}, {4})"
                                + " VALUES ('{5}', '{6}', '{7}', '{8}')",
                                /* 0 */ ClusterLogRecord.TableName,
                                /* 1 */ ClusterLogRecord.ColumnClusterId,
                                /* 2 */ ClusterLogRecord.ColumnType,
                                /* 3 */ ClusterLogRecord.ColumnSubtype,
                                /* 4 */ ClusterLogRecord.ColumnMessage,
                                /* 5 */ _id,
                                /* 6 */ ClusterLogRecord.TypeStatus,
                                /* 7 */ type,
                                /* 8 */ logMessage);
                            SqlCommand sqlCommand = new SqlCommand(sqlString, sqlConnection);
                            sqlCommand.ExecuteNonQuery();
                        }
                    }
                    catch (Exception e)
                    {
                        LogError(e.ToString());
                    }
                }
            }
        }

        private void LogAction(ClusterActionRecord action)
        {
            // Get action log template
            string templateType = action.Status == ClusterActionRecord.StatusBusy ? ClusterTemplateRecord.TypeLogActionStart : ClusterTemplateRecord.TypeLogActionEnd;
            string template = GetTemplate(templateType, action.Action);
            if (template != null)
            {
                // Add some fields from action record
                string info = "";
                if (action.Info != null) info += action.Info;
                info += String.Format("<{0}>{1}</{0}>", ClusterActionRecord.ColumnStatus, action.Status);
                info += String.Format("<{0}>{1}</{0}>", ClusterActionRecord.ColumnStartTime, action.StartTime);

                // Build log message
                string logMessage = XmlHelper.StringFromXPathTemplate(template, info);
                if (logMessage != null)
                {
                    try
                    {
                        using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                        {
                            sqlConnection.Open();

                            // Insert status log record
                            string sqlString = String.Format("INSERT INTO {0}({1}, {2}, {3}, {4}, {5})"
                                + " VALUES ('{6}', '{7}', '{8}', '{9}', {10})",
                                /* 0 */ ClusterLogRecord.TableName,
                                /* 1 */ ClusterLogRecord.ColumnClusterId,
                                /* 2 */ ClusterLogRecord.ColumnType,
                                /* 3 */ ClusterLogRecord.ColumnSubtype,
                                /* 4 */ ClusterLogRecord.ColumnMessage,
                                /* 5 */ ClusterLogRecord.ColumnUserId,
                                /* 6 */ _id,
                                /* 7 */ ClusterLogRecord.TypeAction,
                                /* 8 */ action.Action,
                                /* 9 */ logMessage,
                                /* 10 */ action.UserId != 0 ? action.UserId.ToString() : "NULL");
                            SqlCommand sqlCommand = new SqlCommand(sqlString, sqlConnection);
                            sqlCommand.ExecuteNonQuery();
                        }
                    }
                    catch (Exception e)
                    {
                        LogError(e.ToString());
                    }
                }
            }
        }

        private void LogInfo(string text)
        {
            SnmpSignManager.LogInfo(String.Format("CLUSTER {0}: {1}", _id, text));
        }

        private void LogError(string text)
        {
            SnmpSignManager.LogError(String.Format("CLUSTER {0}: {1}", _id, text));
        }
    }
}