﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace VMS_SNMP
{
    public partial class SnmpSignCluster
    {
        private Dictionary<string, Dictionary<string, string>> _templates;

        private void LoadTemplates()
        {
            using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
            {
                sqlConnection.Open();

                // Build a map of all templates
                _templates = new Dictionary<string, Dictionary<string, string>>();
                SqlCommand command = new SqlCommand(String.Format("SELECT * FROM {0} ORDER BY {1}, {2}",
                    /* 0 */ ClusterTemplateRecord.TableName,
                    /* 1 */ ClusterTemplateRecord.ColumnType,
                    /* 2 */ ClusterTemplateRecord.ColumnSubType),
                    sqlConnection);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ClusterTemplateRecord template = new ClusterTemplateRecord(reader);
                    Dictionary<string, string> templates;
                    if (!_templates.TryGetValue(template.Type, out templates))
                    {
                        // Create new map entry
                        templates = new Dictionary<string, string>();
                        _templates.Add(template.Type, templates);
                    }
                    templates.Add(template.SubType, template.Template);
                }
                reader.Close();
            }
        }

        private string GetTemplate(string type, string subType)
        {
            // Look up type
            Dictionary<string, string> templates;
            if (_templates.TryGetValue(type, out templates))
            {
                // Look up subtype
                string template;
                if (templates.TryGetValue(subType, out template)
                    && template != null)
                {
                    return template;
                }
            }

            // Only log errors for non log types
            foreach (string logType in ClusterTemplateRecord.TypeLogs)
            {
                if (type == logType) return null;
            }

            LogError(String.Format("Missing CLUSTER {0}:{1} template!", type, subType));

            return null;
        }

        private Dictionary<string, string> GetTemplates(string type)
        {
            // Look up type
            Dictionary<string, string> templates;
            if (_templates.TryGetValue(type, out templates)
                && templates != null)
            {
                return templates;
            }

            LogError(String.Format("Missing CLUSTER {0} templates!", type));

            return null;
        }
    }
}