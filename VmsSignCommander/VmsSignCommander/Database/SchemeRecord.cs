﻿namespace VMS_SNMP
{
    public class SchemeRecord
    {
        // Constants
        public const string TableName = "Scheme";
        public const string ColumnId = "id";
        public const string ColumnClusterId = "cluster_id";
        public const string ColumnType = "type";
        public const string ColumnStartTime = "start_time";
        public const string ColumnEndTime = "end_time";
        public const string ColumnDisplayName = "display_name";
        public const string ColumnDescription = "description";
        public const string ColumnInfo = "info";
    }
}

