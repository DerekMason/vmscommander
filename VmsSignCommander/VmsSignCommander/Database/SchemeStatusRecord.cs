﻿namespace VMS_SNMP
{
    public class SchemeStatusRecord
    {
        // Constants
        public const string TableName = "SchemeStatus";
        public const string ColumnClusterId = "cluster_id";
        public const string ColumnSchemeId = "scheme_id";
        public const string ColumnStatus = "status";
        public const string ColumnInfo = "info";
        public const string ColumnTimestamp = "timestamp";
        public const string ColumnUserId = "user_id";
    }
}
