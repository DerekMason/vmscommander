﻿using System;
using System.Data;

namespace VMS_SNMP
{
    public class IDataRecordConvert
    {
        static public int ToInt(IDataRecord record, string name)
        {
            string field = record[name].ToString();
            int value = 0;

            Int32.TryParse(field, out value);

            return value;
        }

        static public bool ToBoolean(IDataRecord record, string name)
        {
            string field = record[name].ToString();
            bool value = false;

            Boolean.TryParse(field, out value);

            return value;
        }

        static public float ToFloat(IDataRecord record, string name)
        {
            string field = record[name].ToString();
            double value = 0;

            Double.TryParse(field, out value);

            return (float)value;
        }

        static public DateTime ToDateTime(IDataRecord record, string name)
        {
            string field = record[name].ToString();
            DateTime value = new DateTime(DateTime.Now.Ticks, DateTimeKind.Local);

            DateTime.TryParse(field, out value);

            return value;
        }

        static public string ToString(IDataRecord record, string name)
        {
            return record[name].ToString();
        }
    }
}