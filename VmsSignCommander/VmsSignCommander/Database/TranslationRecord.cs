﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace VMS_SNMP
{
    public class TranslationRecord
    {
        // Constants
        public const string ValueTypeInt = "INT";
        public const string ValueTypeString = "STRING";

        public const string TableName = "SignTranslation";
        public const string ColumnSignType = "sign_type";
        public const string ColumnTranslationType = "translation_type";
        public const string ColumnType = "type";
        public const string ColumnRangeStart = "range_start";
        public const string ColumnRangeEnd = "range_end";
        public const string ColumnValue = "value";

        // Members
        private string _translationType;
        private string _type;
        private string _rangeStart, _rangeEnd;
        private string _value;

        // Constructors
        public TranslationRecord(IDataRecord record)
            : this(IDataRecordConvert.ToString(record, ColumnTranslationType),
            IDataRecordConvert.ToString(record, ColumnType),
            IDataRecordConvert.ToString(record, ColumnRangeStart),
            IDataRecordConvert.ToString(record, ColumnRangeEnd),
            IDataRecordConvert.ToString(record, ColumnValue))
        { }

        public TranslationRecord(string translationType, string type, string rangeStart, string rangeEnd, string value)
        {
            this._translationType = translationType;
            this._type = type;
            this._rangeStart = rangeStart;
            this._rangeEnd = rangeEnd;
            this._value = value;
        }

        public bool Compare(string value)
        {
            if (_type == ValueTypeInt)
            {
                int rangeStart, rangeEnd;
                int intValue;

                try
                {
                    rangeStart = Convert.ToInt32(_rangeStart);
                    rangeEnd = Convert.ToInt32(_rangeEnd);
                    intValue = Convert.ToInt32(value);

                    return (intValue >= rangeStart && intValue <= rangeEnd);
                }
                // Do nothing...
                catch { }
            }
            else if (_type == ValueTypeString)
            {
                return (String.Compare(value, _rangeStart) >= 0 && String.Compare(value, _rangeEnd) >= 0);
            }

            return false;
        }

        public string TranslationType
        {
            get { return _translationType; }
        }

        public string Type
        {
            get { return _type; }
        }

        public string RangeStart
        {
            get { return _rangeStart; }
        }

        public string RangeEnd
        {
            get { return _rangeEnd; }
        }

        public string Value
        {
            get { return _value; }
        }
    }
}