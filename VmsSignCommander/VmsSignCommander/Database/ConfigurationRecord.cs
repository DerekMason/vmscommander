﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace VMS_SNMP
{
    public class ConfigurationRecord
    {
        // Constants
        public const string TableName = "Configuration";
        public const string ColumnType = "type";
        public const string ColumnSubType = "sub_type";
        public const string ColumnName = "name";
        public const string ColumnValue = "value";
     
        public const string TypeManager = "MANAGER";
        public const string TypeCluster = "CLUSTER";
        public const string TypeSign = "SIGN";
        public const string ValueWidth = "WIDTH";
        public const string ValueHeight = "HEIGHT";
        public const string ValueColours = "COLOURS";
        public const string ValuePollInterval = "POLL_INTERVAL";
        public const string ValueSnmpVersion = "SNMP_VERSION";
        public const string ValueSnmpTimeout = "SNMP_TIMEOUT";
        public const string ValueSnmpCommunity = "SNMP_COMMUNITY";
        public const string ValueLuminanceDayLight = "LUM_DAYLIGHT";
        public const string ValueLedFaultActionMode = "LED_FAULT_MODE";

        public static int GetInt(string type, string sub_type, string name, int defaultValue)
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                {
                    sqlConnection.Open();

                    string sqlString = String.Format("SELECT {0}.{1} FROM {0}"
                        + " WHERE {0}.{2} = '{5}' AND {0}.{3} = '{6}' AND {0}.{4} = '{7}'",
                        /* 0 */ TableName,
                        /* 1 */ ColumnValue,
                        /* 2 */ ColumnType,
                        /* 3 */ ColumnSubType,
                        /* 4 */ ColumnName,
                        /* 5 */ type,
                        /* 6 */ sub_type,
                        /* 7 */ name);

                    // Get longest offset delay for SET actions
                    SqlCommand command = new SqlCommand(sqlString, sqlConnection);
                    System.Object result = command.ExecuteScalar();
                    if (result != null)
                    {
                        return Convert.ToInt32(result);
                    }
                }
            }
            // Do nothing...
            catch { }

            return defaultValue;
        }

        public static string GetString(string type, string sub_type, string name, string defaultValue)
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                {
                    sqlConnection.Open();

                    string sqlString = String.Format("SELECT {0}.{1} FROM {0}"
                        + " WHERE {0}.{2} = '{5}' AND {0}.{3} = '{6}' AND {0}.{4} = '{7}'",
                        /* 0 */ TableName,
                        /* 1 */ ColumnValue,
                        /* 2 */ ColumnType,
                        /* 3 */ ColumnSubType,
                        /* 4 */ ColumnName,
                        /* 5 */ type,
                        /* 6 */ sub_type,
                        /* 7 */ name);

                    // Get longest offset delay for SET actions
                    SqlCommand command = new SqlCommand(sqlString, sqlConnection);
                    System.Object result = command.ExecuteScalar();
                    if (result != null)
                    {
                        return result.ToString();
                    }
                }
            }
            // Do nothing...
            catch { }

            return defaultValue;
        }
    }
}