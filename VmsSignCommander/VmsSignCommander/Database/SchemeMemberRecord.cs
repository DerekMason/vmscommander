﻿namespace VMS_SNMP
{
    public class SchemeMemberRecord
    {
        // Constants
        public const string TableName = "SchemeMember";
        public const string ColumnSchemeId = "scheme_id";
        public const string ColumnSequence = "sequence";
        public const string ColumnMessage = "message";
        public const string ColumnSetDelay = "set_delay";
    }
}

