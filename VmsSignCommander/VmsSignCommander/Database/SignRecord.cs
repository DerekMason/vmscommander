﻿using System.Data;

namespace VMS_SNMP
{
    public class SignRecord
    {
        // Constants
        public const string TableName = "Sign";
        public const string ColumnId = "id";
        public const string ColumnType = "type";
        public const string ColumnPlacementId = "placement_id";
        public const string ColumnHaAddress = "ha_address";
        public const string ColumnNetAddress = "net_address";
        public const string ColumnUnit = "unit";
        public const string ColumnInfo = "info";
        public const string ColumnActive = "active";

        // Members
        protected int _id;
        protected string _type;
        protected int _placementId;
        protected string _haAddress;
        protected string _netAddress;
        protected int _unit;
        protected string _info;
        protected bool _active;

        protected bool _externalCommsFault = false;
        protected int _numberRetries = DEFAULT_RETRIES;
        public const int  DEFAULT_RETRIES = 3;
        // Constructors
        public SignRecord(IDataRecord record)
            : this(IDataRecordConvert.ToInt(record, ColumnId),
            IDataRecordConvert.ToString(record, ColumnType),
            IDataRecordConvert.ToInt(record, ColumnPlacementId),
            IDataRecordConvert.ToString(record, ColumnHaAddress),
            IDataRecordConvert.ToString(record, ColumnNetAddress),
            IDataRecordConvert.ToInt(record, ColumnUnit),
            IDataRecordConvert.ToString(record, ColumnInfo),
            IDataRecordConvert.ToBoolean(record, ColumnActive))
        { }

        public SignRecord(int id, string type, int placementId, string haAddress, string netAddress, int unit, string info, bool active)
        {
            _id = id;
            _type = type;
            _placementId = placementId;
            _haAddress = haAddress;
            _netAddress = netAddress;
            _unit = unit;
            _info = info;
            _active = active;
        }

        // Accessors
        public int Id
        {
            get { return _id; }
        }
    }
}