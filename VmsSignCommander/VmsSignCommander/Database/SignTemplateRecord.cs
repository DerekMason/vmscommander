﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace VMS_SNMP
{
    public class SignTemplateRecord
    {
        // Constants
        public const string TypeAction = "ACTION";
        public const string TypeStatus = "STATUS";
        public const string TypeFormat = "FORMAT";
        public const string TypeTemplate = "TEMPLATE";
        public const string TypeLogStatus = "LOG_STATUS";
        public const string TypeLogActionStart = "LOG_ACTION_START";
        public const string TypeLogActionEnd = "LOG_ACTION_END";

        public static string[] TypeLogs = { TypeLogStatus, TypeLogActionStart, TypeLogActionEnd };

        public const string ValueTypeInt = "INT";
        public const string ValueTypeString = "STRING";

        public const string TableName = "SignTemplate";
        public const string ColumnSignType = "sign_type";
        public const string ColumnType = "type";
        public const string ColumnSubType = "sub_type";
        public const string ColumnSequence = "sequence";
        public const string ColumnTemplate = "template";

        // Members
        private string _type;
        private string _subType;
        private int _sequence;
        private string _template;

        // Constructors
        public SignTemplateRecord(IDataRecord record)
            : this(IDataRecordConvert.ToString(record, ColumnType),
            IDataRecordConvert.ToString(record, ColumnSubType),
            IDataRecordConvert.ToInt(record, ColumnSequence),
            IDataRecordConvert.ToString(record, ColumnTemplate))
        { }

        public SignTemplateRecord(string type, string subType, int sequence, string template)
        {
            this._type = type;
            this._subType = subType;
            this._sequence = sequence;
            this._template = template;
        }

        public string Type
        {
            get { return _type; }
        }

        public string SubType
        {
            get { return _subType; }
        }

        public int Sequence
        {
            get { return _sequence; }
        }

        public string Template
        {
            get { return _template; }
        }
    }
}