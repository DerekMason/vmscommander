﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace VMS_SNMP
{
    public class SignActionRecord
    {
        // Constants
        public const string ActionSet = "SET";
        public const string ActionValidate = "VALIDATE";
        public const string ActionBlank = "BLANK";
        public const string ActionPoll = "POLL";
        public const string ActionGetBitmap = "GETBITMAP";
        public const string ActionGetFaultBitmap = "GETFAULTBITMAP";
        public const string ActionGetValidationBitmap = "GETVALIDATIONBITMAP";
        public const string ActionSetLuminance = "SETLUMINANCE";
        public const string ActionClearLuminance = "CLEARLUMINANCE";
        public const string ActionGetSnmp = "GETSNMP";
        public const string ActionSetSnmp = "SETSNMP";
        public const string ActionSetLuminaceToOverride = "SETLUMINANCEOVERRIDE";
        public const string ActionDimBrightLuminance = "SETDIMBRIGHTLUMINANCE";

        public const string ActionLedFaultReportingMode = "LEDFAULTREPORTINGMODE";
        public const string ActionGetLedFaultPercentage = "PERCENTLEDFAULTSFORLEDFAIL";

        public const string StatusSuccess = "SUCCESS";
        public const string StatusFailure = "FAILURE";
        public const string StatusPending = "WAITING";
        public const string StatusCancelled = "CANCELLED";
        public const string StatusBusy = "BUSY";
        public const string StatusTimeout = "TIMEOUT";

        public const string TableName = "SignAction";
        public const string ColumnId = "id";
        public const string ColumnSignId = "sign_id";
        public const string ColumnAction = "action";
        public const string ColumnInfo = "info";
        public const string ColumnStatus = "status";
        public const string ColumnStartTime = "start_time";
        public const string ColumnLastUpdated = "last_updated";
        public const string ColumnUserId = "user_id";

        public const string ElementMessage = "message";
        public const string ElementMnemonic = "mnemonic";
        public const string ElementDistance = "distance";
        public const string ElementLuminance = "luminance";
        public const string ElementLevel = "level";
        public const string ElementClusterAction = "clusteraction";
        public const string AttributeId = "id";

        public const string LedMode = "ledfaultmode";

        public const int UpdateRetries = 5;

        // Members
        private int _id;
        private int _signId;
        private string _action;
        private string _lastInfo, _info;
        private string _status;
        private DateTime _startTime;
        private DateTime _lastUpdated;
        private int _userId;

        // Constructors
        public SignActionRecord(IDataRecord record)
            : this(IDataRecordConvert.ToInt(record, ColumnId),
            IDataRecordConvert.ToInt(record, ColumnSignId),
            IDataRecordConvert.ToString(record, ColumnAction),
            IDataRecordConvert.ToString(record, ColumnInfo),
            IDataRecordConvert.ToString(record, ColumnStatus),
            IDataRecordConvert.ToDateTime(record, ColumnStartTime),
            IDataRecordConvert.ToDateTime(record, ColumnLastUpdated),
            IDataRecordConvert.ToInt(record, ColumnUserId))
        { }

        public SignActionRecord(int id, int signId, string action, string info, string status, DateTime startTime, DateTime lastUpdated, int userId)
        {
            _id = id;
            _signId = signId;
            _action = action;
            _lastInfo = _info = info;
            _status = status;
            _startTime = startTime;
            _lastUpdated = lastUpdated;
            _userId = userId;
        }

        public void Update()
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                {
                    sqlConnection.Open();

                    // Create command to update status
                    string sqlString = String.Format("UPDATE {0} SET {1} = '{2}', {3} = CURRENT_TIMESTAMP",
                        /* 0 */ TableName,
                        /* 1 */ ColumnStatus,
                        /* 2 */ _status,
                        /* 3 */ ColumnLastUpdated);

                    // Info has changed?
                    bool updateInfo = (_info != _lastInfo);
                    if (updateInfo)
                    {
                        // Update info
                        sqlString += String.Format(", {0} = '{1}'", ColumnInfo, _info);
                    }

                    sqlString += String.Format(" WHERE {0} = {1}", ColumnId, _id);

                    // Retry (to avoid deadlock issue)
                    for (int retry = 0; retry < UpdateRetries; retry++)
                    {
                        try
                        {
                            // Execute command
                            SqlCommand sqlCommand = new SqlCommand(sqlString, sqlConnection);
                            sqlCommand.ExecuteNonQuery();
                            break;
                        }
                        catch (Exception e)
                        {
                            if (retry >= UpdateRetries - 1) throw e;
                        }
                        
                        // Sleep for a little while to allow lock to be released
                        System.Threading.Thread.Sleep((retry + 1) * 250);
                    }

                    // Updated info?
                    if (updateInfo)
                    {
                        _lastInfo = _info;
                    }
                }
            }
            catch (Exception e)
            {
                SnmpSignManager.LogError(String.Format("SIGN {0}: ({1}): {2}", _signId, _id, e.ToString()));
            }
        }

        // Accessors
        public int Id
        {
            get { return _id; }
        }

        public int SignId
        {
            get { return _signId; }
        }

        public string Action
        {
            get { return _action; }
        }

        public string Info
        {
            get { return _info; }
            set { _info = value; }
        }

        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public DateTime StartTime
        {
            get { return _startTime; }
        }

        public int UserId
        {
            get { return _userId; }
        }
    }
}