﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace VMS_SNMP
{
    public class ClusterActionRecord
    {
        // Constants
        public const string ActionSet = "SET";
        public const string ActionValidate = "VALIDATE";
        public const string ActionBlank = "BLANK";
        public const string ActionPoll = "POLL";
        public const string ActionSetLuminance = "SETLUMINANCE";
        public const string ActionClearLuminance = "CLEARLUMINANCE";
        public const string ActionSetLuminanceOverride = "SETLUMINANCEOVERRIDE";
        public const string ActionDimBrightLuminance = "SETDIMBRIGHTLUMINACE";
        public const string ActionFaultLedMode = "LEDFAULTREPORTINGMODE";
        public const string StatusSuccess = "SUCCESS";
        public const string StatusFailure = "FAILURE";
        public const string StatusPending = "WAITING";
        public const string StatusCancelled = "CANCELLED";
        public const string StatusBusy = "BUSY";
        public const string StatusTimeout = "TIMEOUT";

        public const string TableName = "ClusterAction";
        public const string ColumnId = "id";
        public const string ColumnClusterId = "cluster_id";
        public const string ColumnAction = "action";
        public const string ColumnInfo = "info";
        public const string ColumnStatus = "status";
        public const string ColumnStartTime = "start_time";
        public const string ColumnLastUpdated = "last_updated";
        public const string ColumnUserId = "user_id";

        public const string ElementScheme = "scheme";
        public const string AttributeId = "id";
        public const string AttributeBlank = "blank";
        public const string AttributeDelay = "delay";

        public const string NetworkConnectionValid = "NetworkStatus";
        public const string NetworkTimeUp = "connection_time";
        public const string Networkstatus = "status";
        public const string Network_Id = "Id"; 
        public const int UpdateRetries = 5;

        // Members
        private int _id;
        private int _clusterId;
        private string _action;
        private string _info, _lastInfo;
        private string _status;
        private DateTime _startTime;
        private DateTime _lastUpdated;
        private int _userId;

        // Constructors
        public ClusterActionRecord(IDataRecord record)
            : this(IDataRecordConvert.ToInt(record, ColumnId),
            IDataRecordConvert.ToInt(record, ColumnClusterId),
            IDataRecordConvert.ToString(record, ColumnAction),
            IDataRecordConvert.ToString(record, ColumnInfo),
            IDataRecordConvert.ToString(record, ColumnStatus),
            IDataRecordConvert.ToDateTime(record, ColumnStartTime),
            IDataRecordConvert.ToDateTime(record, ColumnLastUpdated),
            IDataRecordConvert.ToInt(record, ColumnUserId))
        { }


        public static int InsertClusterActionRecord(string clusterAction, int clusterID, string clusterInfo)
        {
            using (var connection = new SqlConnection(SnmpSignManager.SqlConnectionString))
            {
                connection.Open();

                using (var command = new SqlCommand(@"INSERT INTO ClusterAction (
	                                                        ClusterAction.cluster_id,
	                                                        ClusterAction.action,
	                                                        ClusterAction.info)
                                                        VALUES (
	                                                        @clusterID,
	                                                        @clusterAction,
	                                                        @info);
                                                    SELECT SCOPE_IDENTITY();", connection))
                {
         
                    command.Parameters.Add("@info", SqlDbType.Xml).Value = clusterInfo;
                    command.Parameters.Add("@clusterAction", SqlDbType.VarChar).Value = clusterAction;
                    command.Parameters.Add("@clusterID", SqlDbType.Int).Value = clusterID;

                  return  Int32.Parse(command.ExecuteScalar().ToString());

                }
            }
        }

        public ClusterActionRecord(int id, int clusterId, string action, string info, string status, DateTime startTime, DateTime lastUpdated, int userId)
        {
            _id = id;
            _clusterId = clusterId;
            _action = action;
            _lastInfo = _info = info;
            _status = status;
            _startTime = startTime;
            _lastUpdated = lastUpdated;
            _userId = userId;
        }

        public void Update()
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(SnmpSignManager.SqlConnectionString))
                {
                    sqlConnection.Open();

                    // Create command to update status
                    string sqlString = String.Format("UPDATE {0} SET {1} = '{2}', {3} = CURRENT_TIMESTAMP",
                        /* 0 */ TableName,
                        /* 1 */ ColumnStatus,
                        /* 2 */ _status,
                        /* 3 */ ColumnLastUpdated);

                    // Info has changed?
                    bool updateInfo = (_info != _lastInfo);
                    if (updateInfo)
                    {
                        // Update info
                        sqlString += String.Format(", {0} = '{1}'", ColumnInfo, _info);
                    }

                    sqlString += String.Format(" WHERE {0} = {1} AND {2} != '{3}'",
                        /* 0 */ ColumnId,
                        /* 1 */ _id,
                        /* 2 */ ColumnStatus,
                        /* 3 */ StatusCancelled);

                    // Retry (to avoid deadlock issue)
                    for (int retry = 0; retry < UpdateRetries; retry++)
                    {
                        try
                        {
                            // Execute command
                            SqlCommand sqlCommand = new SqlCommand(sqlString, sqlConnection);
                            sqlCommand.ExecuteNonQuery();
                            break;
                        }
                        catch (Exception e)
                        {
                            if (retry >= UpdateRetries - 1) throw e;
                        }

                        // Sleep for a little while to allow lock to be released
                        System.Threading.Thread.Sleep((retry + 1) * 250);
                    }

                    // Updated info?
                    if (updateInfo)
                    {
                        _lastInfo = _info;
                    }
                }
            }
            catch (Exception e)
            {
                SnmpSignManager.LogError(String.Format("CLUSTER {0}: ({1}): {2}", _clusterId, _id, e.ToString()));
            }
        }

        public int Id
        {
            get { return _id; }
        }

        public int ClusterId
        {
            get { return _clusterId; }
        }

        public string Action
        {
            get { return _action; }
        }

        public string Info
        {
            get { return _info; }
            set { _info = value; }
        }

        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public DateTime StartTime
        {
            get { return _startTime; }
        }

        public int UserId
        {
            get { return _userId; }
        }
    }
}