﻿using System.Data;

namespace VMS_SNMP
{
    public class ClusterMemberRecord
    {
        // Constants
        public const string TableName = "ClusterMember";
        public const string ColumnClusterId = "cluster_id";
        public const string ColumnSequence = "sequence";
        public const string ColumnSignId = "sign_id";
        public const string ColumnSetDelay = "set_delay";
        public const string ColumnDistance = "distance";
        public const string ColumnUnits = "units";

        // Members
        protected int _sequence;
        protected int _signId;
        protected int _setDelay;
        protected float _distance;
        protected string _units;

        // Constructors
        public ClusterMemberRecord(IDataRecord record)
            : this(IDataRecordConvert.ToInt(record, ColumnSequence),
            IDataRecordConvert.ToInt(record, ColumnSignId),
            IDataRecordConvert.ToInt(record, ColumnSetDelay),
            IDataRecordConvert.ToFloat(record, ColumnDistance),
            IDataRecordConvert.ToString(record, ColumnUnits))
        { }

        public ClusterMemberRecord(int sequence, int signId, int setDelay, float distance, string units)
        {
            _sequence = sequence;
            _signId = signId;
            _setDelay = setDelay;
            _distance = distance;
            _units = units;
        }

        // Acessors
        public int Sequence
        {
            get { return _sequence; }
        }

        public int SignId
        {
            get { return _signId; }
        }

        public int SetDelay
        {
            get { return _setDelay; }
        }

        public float Distance
        {
            get { return _distance; }
        }

        public string Units
        {
            get { return _units; }
        }
    }
}