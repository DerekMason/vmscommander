﻿namespace VMS_SNMP
{
    public class ClusterLogRecord
    {
        // Constants
        public const string TableName = "ClusterLog";
        public const string ColumnClusterId = "cluster_id";
        public const string ColumnType = "type";
        public const string ColumnSubtype = "sub_type";
        public const string ColumnMessage = "message";
        public const string ColumnTimeStamp = "timestamp";
        public const string ColumnUserId = "user_id";

        public const string TypeStatus = "STATUS";
        public const string TypeAction = "ACTION";
    }
}