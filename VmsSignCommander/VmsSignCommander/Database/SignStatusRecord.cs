﻿namespace VMS_SNMP
{
    public class SignStatusRecord
    {
        // Constants
        public const string TableName = "SignStatus";
        public const string ColumnSignId = "sign_id";
        public const string ColumnStatus = "status";
        public const string ColumnInfo = "info";
        public const string ColumnTimeStamp = "timestamp";
    }
}