﻿using System.Data;

namespace VMS_SNMP
{
    public class ClusterRecord
    {
        // Constants
        public const string TableName = "Cluster";
        public const string ColumnId = "id";
        public const string ColumnPlacementId = "placement_id";
        public const string ColumnDisplayName = "display_name";
        public const string ColumnDescription = "description";
        public const string ColumnInfo = "info";
        public const string ColumnActive = "active";

        // Members
        protected int _id;
        protected int _placementId;
        protected string _displayName;
        protected string _description;
        protected string _info;
        protected bool _active;

        // Constructors
        public ClusterRecord(IDataRecord record)
            : this(IDataRecordConvert.ToInt(record, ColumnId),
            IDataRecordConvert.ToInt(record, ColumnPlacementId),
            IDataRecordConvert.ToString(record, ColumnDisplayName),
            IDataRecordConvert.ToString(record, ColumnDescription),
            IDataRecordConvert.ToString(record, ColumnInfo),
            IDataRecordConvert.ToBoolean(record, ColumnActive))
        { }

        public ClusterRecord(int id, int placementId, string displayName, string description, string info, bool active)
        {
            _id = id;
            _placementId = placementId;
            _displayName = displayName;
            _description = description;
            _info = info;
            _active = active;
        }

        // Accessors
        public int Id
        {
            get { return _id; }
        }
    }
}