﻿namespace VMS_SNMP
{
    public class ClusterStatusRecord
    {
        // Constants
        public const string TableName = "ClusterStatus";
        public const string ColumnClusterId = "cluster_id";
        public const string ColumnStatus = "status";
        public const string ColumnInfo = "info";
        public const string ColumnTimestamp = "timestamp";

        public const string StatusFault = "FAULT";
    }
}
