﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using VMS_SNMP;
using System.Configuration.Install;

namespace WindowsService
{
    static class Program
    {
        const string Banner = "VMS Sign Commander (c) VMS Limited 2018 -SNMPv3 1.1.2 - with Encryption";
        const string LogFileName = "VMSSignCommanderLog.txt";

        static bool _console = false;
        static private object _logFileLock = new object();

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static int Main(string[] args)
        {
            bool install = false, uninstall = false, rethrow = false;
            try
            {
                // Check arguments
                for (int i = 0; i < args.Length; i++)
                {
                    switch (args[i])
                    {
                        case "-i":
                        case "-install":
                            install = true;
                            break;

                        case "-u":
                        case "-uninstall":
                            uninstall = true;
                            break;

                        case "-c":
                        case "-console":
                            _console = true;
                            break;

                        default:
                            break;
                    }
                }

                // Show banner?
                if (install || uninstall || _console)
                {
                    Console.WriteLine(Banner);
                }

                // Uninstall?
                if (uninstall)
                {
                    Install(true, args);
                }

                // Install?
                if (install)
                {
                    Install(false, args);
                }

                // Run from console?
                if (_console)
                {
                    Console.WriteLine("Starting...");

                    SnmpSignManager manager = new SnmpSignManager(args);
                    SnmpSignManager.LogInfoHandler = LogInfo;
                    SnmpSignManager.LogErrorHandler = LogError;

                    // Start the sign manager
                    if (manager.Initialise())
                    {
                        manager.Start();

                        Console.WriteLine("System running; press ESC to stop");

                        for (; ; )
                        {
                            ConsoleKeyInfo key = Console.ReadKey(true);
                            if (key.Key == ConsoleKey.Escape) break;
                            else if (key.Key == ConsoleKey.Backspace)
                            {
                                Console.WriteLine("System performing Garbage Collection");
                                GC.Collect();
                                GC.WaitForPendingFinalizers();
                                GC.Collect();
                            }
                            else if (key.Key == ConsoleKey.Tab)
                            {
                                Console.WriteLine("System Restarting");
                                manager.Restart();
                            }
                        }

                        manager.Stop();

                        Console.WriteLine("System stopped");
                    }
                    else
                    {
                        return -1;
                    }
                }
                // Run as service
                else if (!(install || uninstall))
                {
                    rethrow = true; // so that windows sees error...

                    // Run service
                    ServiceBase[] ServicesToRun;
                    ServicesToRun = new ServiceBase[] { new VmsSignCommanderService() };
                    ServiceBase.Run(ServicesToRun);

                    rethrow = false;
                }

                return 0;
            }
            catch (Exception ex)
            {
                if (rethrow) throw;
                Console.Error.WriteLine(ex.Message);
                return -1;
            }
        }

        static void Install(bool undo, string[] args)
        {
            try
            {
                Console.WriteLine(undo ? "uninstalling" : "installing");
                using (AssemblyInstaller inst = new AssemblyInstaller(typeof(Program).Assembly, args))
                {
                    IDictionary state = new Hashtable();
                    inst.UseNewContext = true;
                    try
                    {
                        if (undo)
                        {
                            inst.Uninstall(state);
                        }
                        else
                        {
                            inst.Install(state);
                            inst.Commit(state);
                        }
                    }
                    catch
                    {
                        try
                        {
                            inst.Rollback(state);
                        }
                        catch { }
                        throw;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }

        public static void LogInfo(string text)
        {
            // Use a lock to serialise writing to the log
            lock (_logFileLock)
            {
                string logText = String.Format("{0}: {1}", DateTime.Now.ToString("s"), text);
                Console.WriteLine(logText);
                Console.WriteLine("===============================================================================");

                using (System.IO.StreamWriter logFile =
                    new System.IO.StreamWriter(LogFileName, true))
                {
                    logFile.WriteLine(logText);
                }
            }
        }

        public static void LogError(string text, string file, string member, int line)
        {
            // Use a lock to serialise writing to the log
            lock (_logFileLock)
            {
                string logText = String.Format("{0}: ERROR: {1}_{2}({3}): {4}", DateTime.Now.ToString("s"), file, member, line, text);
                Console.Error.WriteLine(logText);
                Console.Error.WriteLine("===============================================================================");

                using (System.IO.StreamWriter logFile =
                    new System.IO.StreamWriter(LogFileName, true))
                {
                    logFile.WriteLine(logText);
                }
            }
        }
    }
}
