﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace WindowsService
{
    public enum ServiceState
    {
        SERVICE_STOPPED = 0x00000001,
        SERVICE_START_PENDING = 0x00000002,
        SERVICE_STOP_PENDING = 0x00000003,
        SERVICE_RUNNING = 0x00000004,
        SERVICE_CONTINUE_PENDING = 0x00000005,
        SERVICE_PAUSE_PENDING = 0x00000006,
        SERVICE_PAUSED = 0x00000007,
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct ServiceStatus
    {
        public long dwServiceType;
        public ServiceState dwCurrentState;
        public long dwControlsAccepted;
        public long dwWin32ExitCode;
        public long dwServiceSpecificExitCode;
        public long dwCheckPoint;
        public long dwWaitHint;
    };
    
    public partial class VmsSignCommanderService : ServiceBase
    {
        // Constants
        new private const string ServiceName = "VMS Sign Commander";
        private const string EventLogSource = "VMSSignCommander";
        private const string EventLogName = "SignCommander";

        // Imports
        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool SetServiceStatus(IntPtr handle, ref ServiceStatus serviceStatus);

        // Members
        private System.Diagnostics.EventLog _eventLog;

        private VMS_SNMP.SnmpSignManager _signManager;

        // Constructor
        public VmsSignCommanderService()
        {
            InitializeComponent();

            // Create event log if necessary
            _eventLog = new System.Diagnostics.EventLog();
            if (!System.Diagnostics.EventLog.SourceExists(ServiceName))
            {
                System.Diagnostics.EventLog.CreateEventSource(ServiceName, null);
            }
            _eventLog.Source = ServiceName;
        }

        protected override void OnStart(string[] args)
        {
            // Update the service state to Start Pending.
            ServiceStatus serviceStatus = new ServiceStatus();
            serviceStatus.dwCurrentState = ServiceState.SERVICE_START_PENDING;
            serviceStatus.dwWaitHint = 100000;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);

            int workerThreads, completionThreads;
            System.Threading.ThreadPool.GetMaxThreads(out workerThreads, out completionThreads);
            _eventLog.WriteEntry(String.Format("Starting {0} Service", ServiceName));

            // Start the sign manager
            _signManager = new VMS_SNMP.SnmpSignManager(args);
            VMS_SNMP.SnmpSignManager.LogErrorHandler = LogError;
            if (_signManager.Initialise())
            {
                _signManager.Start();
                _eventLog.WriteEntry(String.Format("Started {0} Service", ServiceName));

                // Update the service state to Running.
                serviceStatus.dwCurrentState = ServiceState.SERVICE_RUNNING;
            }
            else
            {
                _signManager = null;
                _eventLog.WriteEntry(String.Format("Could not start {0} Service", ServiceName));

                // Update the service state to Stopped.
                serviceStatus.dwCurrentState = ServiceState.SERVICE_STOPPED;
            }

            SetServiceStatus(this.ServiceHandle, ref serviceStatus);
        }

        protected override void OnStop()
        {
            // Update the service state to Stop Pending.
            ServiceStatus serviceStatus = new ServiceStatus();
            serviceStatus.dwCurrentState = ServiceState.SERVICE_STOP_PENDING;
            serviceStatus.dwWaitHint = 100000;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);

            // Release sign manager?
            if (_signManager != null)
            {
                _eventLog.WriteEntry(String.Format("Stopping {0} Service", ServiceName));
                _signManager.Stop();
                _signManager = null;
                _eventLog.WriteEntry(String.Format("Stopped {0} Service", ServiceName));
            }

            // Update the service state to Stopped.
            serviceStatus.dwCurrentState = ServiceState.SERVICE_STOPPED;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);
        }

        protected void LogError(string text, string file, string member, int line)
        {
            _eventLog.WriteEntry(String.Format("ERROR: {0}_{1}({2}): {3}", file, member, line, text));
        }
    }
}
