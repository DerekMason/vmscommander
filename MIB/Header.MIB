Header DEFINITIONS ::= BEGIN


--IDENTIFICATION

--	Module  :  header.mib
--	Version :  V4.00
--	Author	:  A Kipling
--	Date	:  10/05/2010
--
--	Function:
--	Header file for VMS MIBs (provides access to UTMC and VMSl Private Nodes)	
--    	UTMC access taken from standard UTMC MIBs (produced by IDT)	
--
--    	VMS object definitions MIB
--
--    	Variable Message Signs Limited 	
--    	Unit 1,	
--   	Monkton Business Park North,
--   	Mill Lane,
--    	Hebburn,
--   	Tyne & Wear
--    	NE31 2JZ,
--    	United Kingdom


--	27/10/2009 
--		remaned testBitmaps node to testSequence
--		Added digitalIO and timeTable Nodes	
--	12/12/2009
--		Added in BitStringSyntax definition	
--	27/02/2010
--		Added alternate route back to ISO(1)
--	09/04/2010
--		Added canDevice NODE
--	10/05/2010
--		Added cpcGlobal and vehicleClassification NODEs


--The root back to the iso(1) node can be achieved using differing methods
--The method to use depends on the MIB Browser/Reader you are using.
--Simply comment out the method that y0u do not wish to use.


--METHOD 1

IMPORTS
	ccitt, iso, org, dod, internet      		FROM RFC1155-SMI
 	directory, mgmt, experimental, private		FROM RFC1155-SMI
	enterprises					FROM RFC1155-SMI;	

--METHOD 2

--	org OBJECT IDENTIFIER ::= { iso  3 }
--	dod OBJECT IDENTIFIER ::= { org  6 }
--	internet OBJECT IDENTIFIER ::= { dod  1 }
--	directory OBJECT IDENTIFIER  ::= { internet  1 }
--	mgmt  OBJECT IDENTIFIER  ::= { internet  2 }
--	mib-2  OBJECT IDENTIFIER  ::= { mgmt  1 }
--	transmission  OBJECT IDENTIFIER  ::= { mib-2  10 }
--	experimental  OBJECT IDENTIFIER  ::= { internet  3 }
--	private  OBJECT IDENTIFIER  ::= { internet  4 }
--	enterprises  OBJECT IDENTIFIER  ::= { private  1 }
--	security  OBJECT IDENTIFIER  ::= { internet  5 }
--	snmpV2  OBJECT IDENTIFIER  ::= { internet  6 }
--	snmpDomains  OBJECT IDENTIFIER  ::= { snmpV2  1 }
--	snmpProxys  OBJECT IDENTIFIER  ::= { snmpV2  2 }
--	snmpModules  OBJECT IDENTIFIER  ::= { snmpV2  3 }


--END OF root to iso(1) node


	TruthTable ::= INTEGER{true (1), false (2)}
	DisplayString ::= OCTET STRING
	UTMCTime ::= DisplayString (SIZE(13))
	BitStringSyntax ::= DisplayString (SIZE(100))

--	CoYC UTMC OID
	
--	Next line identifies VMS Limited enterprise Node
	vmsLimited	OBJECT IDENTIFIER ::= { enterprises 11660 }

	utmc		OBJECT IDENTIFIER ::= { enterprises 13267 }
	
--	UTMC sub0branches - Registration points

--	Air Quality
	
	utmcAirQualityMonitor	OBJECT IDENTIFIER ::= { utmc 1 }
	utmcAirQualType1	OBJECT IDENTIFIER ::= { utmcAirQualityMonitor 1 }

--	Dial Up UTC

	utmcDialUpUTC		OBJECT IDENTIFIER ::= { utmc 2 }
	utmcDialUpUTCType1	OBJECT IDENTIFIER ::= { utmcDialUpUTC 1 }

--	Full UTC

	utmcFullUTC		OBJECT IDENTIFIER ::= { utmc 3 }
	utmcFullUTCType1	OBJECT IDENTIFIER ::= { utmcFullUTC 1 }

--	Simple UTC

	utmcSimpleUTC		OBJECT IDENTIFIER ::= { utmc 4 }
	utmcSimpleUTCType1	OBJECT IDENTIFIER ::= { utmcSimpleUTC 1 }

--	Traffic Counter
	
	utmcTrafficCounter	OBJECT IDENTIFIER ::= { utmc 5 }
	utmcTrafficCounterType1	OBJECT IDENTIFIER ::= { utmcTrafficCounter 1 }

--	VMS

	utmcVMS			OBJECT IDENTIFIER ::= { utmc 6 }
	utmcVMSType1		OBJECT IDENTIFIER ::= { utmcVMS 1 }

--	Car Parks

	utmcCarParks		OBJECT IDENTIFIER ::= { utmc 7 }
	utmcCarParksType1	OBJECT IDENTIFIER ::= { utmcCarParks 1 }

--	VMS Private Nodes found under Enterprise 11660


	vmslGlobal		OBJECT IDENTIFIER ::= { vmsLimited 1 }
	smsControl		OBJECT IDENTIFIER ::= { vmsLimited 2 }
	nmcs2Control	OBJECT IDENTIFIER ::= { vmsLimited 3 }
	deviceConfiguration	OBJECT IDENTIFIER ::= { vmsLimited 4 }
	powerStats		OBJECT IDENTIFIER ::= { vmsLimited 5 }	
	testSequence	OBJECT IDENTIFIER ::= { vmsLimited 6 }
	signFaults		OBJECT IDENTIFIER ::= { vmsLimited 7 }
	signDisplay		OBJECT IDENTIFIER ::= { vmsLimited 8 }
	signFonts		OBJECT IDENTIFIER ::= { vmsLimited 9 }
	environmental	OBJECT IDENTIFIER ::= { vmsLimited 10 }
	addressPlug		OBJECT IDENTIFIER ::= { vmsLimited 11 }
	cpcHistory		OBJECT IDENTIFIER ::= { vmsLimited 12 }
	digitalIO		OBJECT IDENTIFIER ::= { vmsLimited 13 }
	timeTable		OBJECT IDENTIFIER ::= { vmsLimited 14 }
	canDevice		OBJECT IDENTIFIER ::= { vmsLimited 15 }
	cpcGlobals		OBJECT IDENTIFIER ::= { vmsLimited 16 }
	vehicleClassification OBJECT IDENTIFIER ::= { vmsLimited 17 }
	
END	
